
/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , files = require('./routes/files')
  , login = require('./routes/login')
  , user = require('./routes/users')
  , memo = require('./routes/memos')
  , timesheet = require('./routes/timesheets')
  , http = require('http')
  , path = require('path');

var app = express();

var cronJobs = require('./utils/timesheetPeriods.js');


app.configure(function(){
  app.set('port', process.env.PORT || 3000);
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
app.use(express.cookieParser('secret'));
  app.use(express.session());
  app.use(app.router);
  app.use(express.static(path.join(__dirname, 'public')));
});

//Stuff for upload
var S3_KEY = 'AKIAIVWEGU4M57C3W75A';
var S3_SECRET = '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';
var S3_BUCKET = 'b1.test';
var knox = require('knox').createClient({
    key: S3_KEY,
    secret: S3_SECRET,
    bucket: S3_BUCKET
});

app.configure('development', function(){
  app.use(express.errorHandler());
});


app.get('/', routes.index);
app.post('/uploadFile', files.upload );
app.get('/getFiles', files.get);
app.get('/loginAdmin', login.admin );
app.get('/loginUser', login.user );
app.get('/logout', login.logout);
app.get('/createUser', user.create);
app.get('/resetUserPassword', user.resetPassword);
app.get('/getUsers', user.getAll);
app.get('/deleteUser', user.delete);
app.get('/postMemo', memo.post);
app.get('/getMemos', memo.getAll);
app.get('/getCurrentUser', user.getCurrentUser);
app.get('/getUserMemos', memo.getUserMemos);
app.get('/postTimesheet', timesheet.post);
app.get('/searchTimesheetsByName', timesheet.searchTimesheetsByName);
app.get('/searchTimesheetsByPayPeriod', timesheet.searchTimesheetsByPayPeriod);
app.get('/newPayPeriod', timesheet.newPayPeriod);
app.get('/getCurrentPayPeriod', timesheet.getCurrentPayPeriod);
app.get('/getAllPayPeriods', timesheet.getAllPayPeriods);

app.get('/deleteAllUsers', user.wipe);
app.get('/deleteAllTimesheets', timesheet.wipe);
app.get('/deleteAllMemos', memo.wipe);
app.get('/deleteAllFiles', files.wipe);
app.get('/deleteAllPayPeriods', timesheet.payperiodWipe);
app.get('/bootstrapDB', cronJobs.bootstrap);


http.createServer(app).listen(app.get('port'), function(){
  cronJobs.start();
  console.log("Express server listening on port " + app.get('port'));
});


