var eps_admindocumentsinit = function()
{
  getElementObject("uploadDocumentButton").innerHTML = "<a href='javascript:uploadDocumentClicked()'> <div class='glassFinish addNew'>Upload File</div></a>";
  getElementObject("uploadDocumentBox").innerHTML =
  	'<b>Name: </b>'+
	'<input type="text" id="file_name"><br>'+
    '<b>Description:</b> <br>'+
	'<textarea id="file_description" rows=10 cols=30>Enter a description for the file...</textarea><br>'+
    '<b>Select file:</b>'+
    '<input type="file" id="uploaded"><br>'+ 
    '<b>Categories:</b>'+
    '<select id="category" class="input">'+
      '<option value="Core Measures<">Core Measures</option>'+
      '<option value="Employee Requirements">Employee Requirements</option>'+
      '<option value="Benefits">Benefits</option>'+
      '<option value="Risk Management">Risk Management</option>'+
      '<option value="Newsletter">Newsletter</option>'+
      '<option value="Utilization/Practice Guidelines">Utilization/Practice Guidelines</option>'+
      '<option value="Computer Training">Computer Training</option>'+
      '<option value="Health Insurance Benefits">Health Insurance Benefits</option>'+
      '<option value="Long Term Disability Benefits">Long Term Disability Benefits</option>'+
      '<option value="Short Term Disability Benefits">Short Term Disability Benefits</option>'+
      '<option value="Profit Sharing/ Employee Savings Trust (401k)">Profit Sharing/ Employee Savings Trust (401k)</option>'+
      '<option value="Credentialing">Credentialing</option>'+
      '<option value="Payroll/Tax forms">Payroll/Tax forms</option>'+
    '</select><br>'+
    '<b>Access Choices:</b><br>'+
      '<input type="checkbox" id="document_access0" value="page">Page<br>'+ 
      '<input type="checkbox" id="document_access1" value="churchill">Churchill<br>'+ 
      '<input type="checkbox" id="document_access2" value="good_samaritan">Good Samaritan<br>'+ 
      '<input type="checkbox" id="document_access3" value="ironwood">Ironwood<br>'+ 
      '<input type="checkbox" id="document_access4" value="estrella">Estrella<br>'+ 
      '<input type="checkbox" id="document_access5" value="rapid_care">Rapid Care<br>'+ 
      '<input type="checkbox" id="document_access6" value="mid_level">Mid Level<br>'+ 
      '<input type="checkbox" id="document_access7" value="Phoenix">Phoenix<br>';
}


function uploadDocumentClicked(){
  var form = new FormData();
  var categorySelector = document.getElementById('category');
  var fileInput = document.getElementById('uploaded');
  var file = $('#uploaded').get(0).files[0];
  var name = getElementObject("file_name").value;
  var description = getElementObject("file_description").value;
  var action = '/uploadFile';
  var category = categorySelector.value;
  var locations=[];
  var accessPrivileges=[];

  var currentSubj;

  for(i=0; i<8; i++){
    currentSubj = document.getElementById("document_access"+i);
    if(currentSubj.checked){
      accessPrivileges = accessPrivileges + '1';
      locations.push(currentSubj.value)
    }else
      accessPrivileges = accessPrivileges + '0';
  }

  console.log("ACCESS PRIV:"+accessPrivileges+'\n'+"LOCATIONS:"+locations+'\n');
  form.append('uploaded', file);
  form.append('category', category);
  form.append('accessPrivileges', accessPrivileges);
  form.append('locations', locations);
  form.append('name', name);
  form.append('description', description);

  var xhr = new XMLHttpRequest();
  xhr.open('POST', action, true);
  xhr.send(form);
  alert("file submitted!!");
  return false;
}