console.log("main-memos entered");
var memosOpen = false;
var eps_adminmemosactive = function(){
    showAllMemos();
}

var eps_adminmemosinit = function()
{
  getElementObject("memoPostButton").innerHTML = "<a href='javascript:memoPostClicked()'> <div class='glassFinish' id='logInButton' >Post Memo</div></a>";
  getElementObject("showAllMemosButton").innerHTML = "<a href='javascript:showAllMemosClicked()'> <div class='glassFinish' id='logInButton' >Show All Memos</div></a>";
  getElementObject("memoInputBox").innerHTML = 
    'Memo Text:<br>'+
    '<textarea class="input" id="eps_memo" cols="50" rows="20"> Enter the memo text here...</textarea><br>'+
    '</select><br>'+
    'Memo Date:<br>'+
    '<input type="date" class="input" id="eps_memo_date">' +
    'Access Choices:<br>'+
      '<input type="checkbox" id="memo_access0" value="page">Page<br>'+ 
      '<input type="checkbox" id="memo_access1" value="churchill">Churchill<br>'+ 
      '<input type="checkbox" id="memo_access2" value="good_samaritan">Good Samaritan<br>'+ 
      '<input type="checkbox" id="memo_access3" value="ironwood">Ironwood<br>'+ 
      '<input type="checkbox" id="memo_access4" value="estrella">Estrella<br>'+ 
      '<input type="checkbox" id="memo_access5" value="rapid_care">Rapid Care<br>'+ 
      '<input type="checkbox" id="memo_access6" value="mid_level">Mid Level<br>'+ 
      '<input type="checkbox" id="memo_access7" value="Phoenix">Phoneix<br>';
}

function memoPostClicked(){
  var memo = getElementObject("eps_memo").value;
  var date = getElementObject("eps_memo_date").value;
  var permissions=[];
  for(i=0; i<8; i++){
    temp = document.getElementById("memo_access"+i).checked;
    console.log(temp);
    if(temp)
      permissions = permissions + '1';
    else
      permissions = permissions + '0';
  }
  
  var url = "../";
  url = url + "postMemo";
  url = url + "?memo=" + memo;
  url = url + "&date=" + date;
  url = url + "&accessPrivileges=" + permissions; 
  loadFile(url, memoPostCB);
}

function showAllMemosClicked(){
  var url = "../";
  url = url + "getMemos";
  loadFile(url, showAllMemosCB);
}

function hideAllMemosClicked(){
   getElementObject("allMemosBox").innerHTML = "";
   getElementObject("hideAllMemosButton").innerHTML = "";
   getElementObject("showAllMemosButton").innerHTML = "<a href='javascript:showAllMemosClicked()'> <div class='glassFinish' id='logInButton' >Show All Memos</div></a>";
   memosOpen = false;
}

function memoPostCB(){
  alert("memo posted");
  if(memosOpen == true)
  {
	showAllMemosClicked();
  }
}

function showAllMemosCB(data){
  try
  {
    var parsedData = JSON.parse(data);
  }
  catch(e)
  {
    var parsedData = {};
  }

  var allMemos = '<table border="1">'
                +'<tr>'
                +'<th>Memo Date</th>'
                +'<th>Memo</th>'
                +'<th>Access Privileges</th>'
                +'</tr>';
  for (i = 0; i< parsedData.length; i++){
    var object = parsedData[i];
    allMemos+='<tr>'
            + '<td>'+object["date"]+'</td>'
            + '<td>'+object["memo"]+'</td>'
            + '<td>'+object["accessPrivileges"]+'</td>'
            + '</tr>';
  }
  allMemos+='</table>';
  getElementObject("allMemosBox").innerHTML = allMemos;
  getElementObject("hideAllMemosButton").innerHTML = "<a href='javascript:hideAllMemosClicked()'> <div class='glassFinish' id='logInButton' >Hide Memos</div></a>";
  getElementObject("showAllMemosButton").innerHTML = "";
  memosOpen = true;

}