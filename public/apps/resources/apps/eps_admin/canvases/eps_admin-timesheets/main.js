var eps_admintimesheetsinit = function(){

  getElementObject("searchByNameButton").innerHTML = "<a href='javascript:selectSearchByNameClicked()'> <div class='glassFinish' id='logInButton' >Search by name </div></a>";
  getElementObject("searchByPayPeriodButton").innerHTML = "<a href='javascript:selectSearchByPayPeriodClicked()'> <div class='glassFinish' id='logInButton'>Search by pay period </div></a>";
  getElementObject("timesheetSearchBox").innerHTML = "";
  getElementObject("searchTimesheetsButton").innerHTML =  "";
  getElementObject("showTimesheetsBox").innerHTML =  "";
  getElementObject("resetSearchButton").innerHTML =  "";
}

function selectSearchByNameClicked(){
  loadFile("../getUsers", getAllUsersCB)
}
function getAllUsersCB(data){
    try
    {
      var parsedData = JSON.parse(data);
    }
    catch(e)
    {
      var parsedData = {};
    }
  
     var usersDropDown = '<select id="users_search">';
	 for (i = 0; i< parsedData.length; i++){
       var object = parsedData[i];
       usersDropDown += '<option value=' + object["username"] + '>' + object["lastName"] +', '+ object["firstName"] + '</option>';
     }
	 usersDropDown += '</select>';
	 getElementObject("timesheetSearchBox").innerHTML = usersDropDown;
	 
     getElementObject("searchByNameButton").innerHTML = "";
     getElementObject("searchByPayPeriodButton").innerHTML = "<a href='javascript:selectSearchByPayPeriodClicked()'> <div class='glassFinish' id='logInButton'>Search by pay period </div></a>";
     getElementObject("searchTimesheetsButton").innerHTML =  "<a href='javascript:searchByNameClicked()'> <div class='glassFinish' id='logInButton' >Search</div></a>";
     getElementObject("showTimesheetsBox").innerHTML =  "";
     getElementObject("resetSearchButton").innerHTML =  "";	 
  
}

function selectSearchByPayPeriodClicked(){
  loadFile("../getAllPayPeriods", getPayPeriodsCB)
 }
 
function  getPayPeriodsCB(data){
    try
    {
      var parsedData = JSON.parse(data);
    }
    catch(e)
    {
      var parsedData = {};
    }
  
   var payPeriodsDropDown = '<select id="timesheets">';
	 for (i = 0; i< parsedData.length; i++){
       var object = parsedData[i];
       payPeriodsDropDown += '<option value=' + object["timesheet"] + "|" + object["sheetType"] + '>' + object["sheetType"] + ": " + object["timesheet"] + '</option>';
     }
	 payPeriodsDropDown += '</select>'; 
	 getElementObject("timesheetSearchBox").innerHTML = payPeriodsDropDown; 

    getElementObject("searchByNameButton").innerHTML = "<a href='javascript:selectSearchByNameClicked()'> <div class='glassFinish' id='logInButton' >Search by name </div></a>";
    getElementObject("searchByPayPeriodButton").innerHTML = "";
    getElementObject("searchTimesheetsButton").innerHTML =  "<a href='javascript:searchByPayPeriodClicked()'> <div class='glassFinish' id='logInButton' >Search</div></a>";
	  getElementObject("showTimesheetsBox").innerHTML =  "";
	  getElementObject("resetSearchButton").innerHTML =  "";
}
 
function searchByNameClicked(){
  var name = getElementObject("users_search").value;
	
  var url = "../";  
  url = url + "searchTimesheetsByName";
  url = url + "?username=" + name;
  console.log(url)
  loadFile(url,searchByNameCB);
}
function searchByPayPeriodClicked(){
  var dropdownInfo = getElementObject("timesheets").value;
  var info = dropdownInfo.split("|");
  var timesheet = info[0];
  var type = info[1];
  var url = "../";  
  url = url + "searchTimesheetsByPayPeriod";
  url = url + "?timesheet=" + timesheet;
  url = url + "&sheetType=" + type;
  console.log(url);
  loadFile(url,searchByPayPeriodCB);
  
}

function searchByNameCB(data){
 try
  {
    var parsedData = JSON.parse(data);
  }
  catch(e)
  {
    var parsedData = {};
  }
  
  var timesheets = "";

  for (i = 0; i< parsedData.length; i++){
    var object = parsedData[i];
    var type = object.type;
	console.log("TYPE:" + type);
    switch(type)
    {
      case "PA":
		timesheets += '<br><b>'+ object.timesheet + '</b><br>'
				   + "<b>Name:</b> " + object.username + "<br>"
				   + "<b>Hours After Midnight:</b> " + object.hoursAfterMidnight + "<br>"
				   + "<b>Holiday Hours:</b> " + object.holidayHours + "<br>"
				   + "<b>Additional Hours:</b> " + object.additionalHours + "<br>"
				   + "<b>Special Requests:</b> " + object.specialRequests + "<br>";
		break;
	  case "pageProvider":
	    console.log("ENTERED PAGE PROVIDER");
		timesheets += '<br><b>'+ object.timesheet + '</b><br>'
				   + "<b>Name:</b> " + object.username + "<br>"
				   + "<b>Good Samaritan Hours: </b>" + object.goodSamaritanHours +"<br>"
				   + "<b>Ironwood Hours:</b>" + object.ironwoodHours +"<br>"
				   + "<b>Estrella Hours:</b>" + object.estrellaHours +"<br>"
				   + "<b>Additional Hours:</b> " + object.additionalHours + "<br>"
				   + "<b>Special Requests:</b> " + object.specialRequests + "<br>";
		break;
	  case "Phoenix":
		timesheets += '<br><b>'+ object.timesheet + '</b><br>'
				   + "<b>Name:</b> " + object.username + "<br>"
				   + "<b>Good Samaritan Hours: </b>" + object.goodSamaritanHours +"<br>"
				   + "<b>Ironwood Hours:</b>" + object.ironwoodHours +"<br>"
				   + "<b>Estrella Hours:</b>" + object.estrellaHours +"<br>"
				   + "<b>Monday-Thursday Night Shift Hours:</b>" + object.monThursNightShiftHours +"<br>"
				   + "<b>Friday-Sunday Night Shift Hours:</b>" + object.friSunNightShiftHours +"<br>"
				   + "<b>Call In Hours:</b>" + object.callInHours +"<br>"
				   + "<b>Extra Calculated Pay:</b>" + object.extraCalPay + "<br>"
				   + "<b>Hours After Midnight:</b> " + object.hoursAfterMidnight + "<br>"
				   + "<b>Holiday Hours:</b> " + object.holidayHours + "<br>"
				   + "<b>Additional Hours:</b> " + object.additionalHours + "<br>"
				   + "<b>Special Requests:</b> " + object.specialRequests + "<br>";
		break;
    case "Office":
    timesheets += '<br><b>'+ object.timesheet + '</b><br>'
           + "<b>Name:</b> " + object.username + "<br>"
           + "<b>Website Hours:</b> " + object.websiteHours + "<br>"
           + "<b>Sick</b> " + object.sick + "<br>"
           + "<b>Holiday:</b> " + object.holiday + "<br>"
           + "<b>Other:</b> " + object.other + "<br>";
    break;
    case "Churchill":
    timesheets += '<br><b>'+ object.timesheet + '</b><br>'
           + "<b>Name:</b> " + object.username + "<br>"
           + "<b>Call In Hours:</b> " + object.callInHours + "<br>"
           + "<b>Holiday Hours:</b> " + object.holidayHours + "<br>"
           + "<b>Additional Hours:</b> " + object.additionalHours + "<br>"
           + "<b>Special Requests:</b> " + object.specialRequests + "<br>";
    break;
	  default:
	    console.log("ERROR");
    }
  }
   getElementObject("showTimesheetsBox").innerHTML =  timesheets; 
}

function searchByPayPeriodCB(data){
 try
  {
    var parsedData = JSON.parse(data);
  }
  catch(e)
  {
    var parsedData = {};
  }
  
  var timesheets = "";

  for (i = 0; i< parsedData.length; i++){
    var object = parsedData[i];
    var type = object.type;

    switch(type)
    {
      case "PA":
		timesheets += '<br><b>'+ object.timesheet + '</b><br>'
				   + "<b>Name:</b> " + object.username + "<br>"
				   + "<b>Hours After Midnight:</b> " + object.hoursAfterMidnight + "<br>"
				   + "<b>Holiday Hours:</b> " + object.holidayHours + "<br>"
				   + "<b>Additional Hours:</b> " + object.additionalHours + "<br>"
				   + "<b>Special Requests:</b> " + object.specialRequests + "<br>";
		break;
	  case "pageProvider":
		timesheets += '<br><b>'+ object.timesheet + '</b><br>'
				   + "<b>Name:</b> " + object.username + "<br>"
				   + "<b>Good Samaritan Hours: </b>" + object.goodSamaritanHours +"<br>"
				   + "<b>Ironwood Hours:</b>" + object.ironwoodHours +"<br>"
				   + "<b>Estrella Hours:</b>" + object.estrellaHours +"<br>"
				   + "<b>Additional Hours:</b> " + object.additionalHours + "<br>"
				   + "<b>Special Requests:</b> " + object.specialRequests + "<br>";
		break;
	  case "Phoenix":
		timesheets += '<br><b>'+ object.timesheet + '</b><br>'
				   + "<b>Name:</b> " + object.username + "<br>"
				   + "<b>Good Samaritan Hours: </b>" + object.goodSamaritanHours +"<br>"
				   + "<b>Ironwood Hours:</b>" + object.ironwoodHours +"<br>"
				   + "<b>Estrella Hours:</b>" + object.estrellaHours +"<br>"
				   + "<b>Monday-Thursday Night Shift Hours:</b>" + object.monThursNightShiftHours +"<br>"
				   + "<b>Friday-Sunday Night Shift Hours:</b>" + object.friSunNightShiftHours +"<br>"
				   + "<b>Call In Hours:</b>" + object.callInHours +"<br>"
				   + "<b>Extra Calculated Pay:</b>" + object.extraCalPay + "<br>"
				   + "<b>Hours After Midnight:</b> " + object.hoursAfterMidnight + "<br>"
				   + "<b>Holiday Hours:</b> " + object.holidayHours + "<br>"
				   + "<b>Additional Hours:</b> " + object.additionalHours + "<br>"
				   + "<b>Special Requests:</b> " + object.specialRequests + "<br>";
		break;
    case "Office":
    timesheets += '<br><b>'+ object.timesheet + '</b><br>'
           + "<b>Name:</b> " + object.username + "<br>"
           + "<b>Website Hours:</b> " + object.websiteHours + "<br>"
           + "<b>Sick</b> " + object.sick + "<br>"
           + "<b>Holiday:</b> " + object.holiday + "<br>"
           + "<b>Other:</b> " + object.other + "<br>";
    break;
    case "Churchill":
    timesheets += '<br><b>'+ object.timesheet + '</b><br>'
           + "<b>Name:</b> " + object.username + "<br>"
           + "<b>Call In Hours:</b> " + object.callInHours + "<br>"
           + "<b>Holiday Hours:</b> " + object.holidayHours + "<br>"
           + "<b>Additional Hours:</b> " + object.additionalHours + "<br>"
           + "<b>Special Requests:</b> " + object.specialRequests + "<br>";
    break;
	  default:
	    console.log("ERROR");
    }
  }
   getElementObject("showTimesheetsBox").innerHTML =  timesheets; 
}
