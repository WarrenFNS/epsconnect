
var eps_adminusersActive = function(){
    showAllUsers();
}

var usersOpen = false;

var eps_adminusersinit = function(){
  getElementObject("userCreateButton").innerHTML = "<a href='javascript:userCreateClicked()'> <div class='glassFinish' id='logInButton' >Create </div></a>";
  getElementObject("showAllUsersButton").innerHTML = "<a href='javascript:showAllUsersClicked()'> <div class='glassFinish' id='logInButton' >Show All Users</div></a>";
  getElementObject("userInputBox").innerHTML =
  'User id:<br>'+
    '<input autocapitalize="off" autocorrect="off" class="input" id="username">'+ 
  'First name:<br>'+
    '<input autocapitalize="off" autocorrect="off" class="input" id="first_name">'+ 
  'Last name:<br>'+
    '<input autocapitalize="off" autocorrect="off" class="input" id="last_name">'+ 
  'Password:<br>'+
    '<input autocapitalize="off" autocorrect="off" type="password" class="input" id="user_password">'+ 
  'Verify Password :<br>'+
    '<input autocapitalize="off" autocorrect="off" type="password" class="input" id="verify_user_password">'+ 
  'Timesheet Choice:'+
  '<select id="timesheet" class="input">'+
    '<option value="Phoenix">Phoenix</option>'+
    '<option value="PA">PA</option>'+
    '<option value="Office">Office</option>'+
    '<option value="Churchill">Churchill</option>'+
    '<option value="pageProvider">Page Provider</option>'+
  '</select><br>'+
  'Access Choices:<br>'+
    '<input type="checkbox" id="eps_access0">Page<br>'+ 
    '<input type="checkbox" id="eps_access1">Churchill<br>'+ 
    '<input type="checkbox" id="eps_access2">Good Samaritan<br>'+ 
    '<input type="checkbox" id="eps_access3">Ironwood<br>'+ 
    '<input type="checkbox" id="eps_access4">Estrella<br>'+ 
    '<input type="checkbox" id="eps_access5">Rapid Care<br>'+ 
    '<input type="checkbox" id="eps_access6">Mid Level<br>'+ 
    '<input type="checkbox" id="eps_access7">Phoenix<br>';
}

function userCreateClicked(){
  var username = getElementObject("username").value;
  var firstName = getElementObject("first_name").value;
  var lastName = getElementObject("last_name").value;
  var pass = hex_md5(getElementObject("user_password").value);
  var passVerify = hex_md5(getElementObject("verify_user_password").value);
  var timesheet = getElementObject("timesheet").value;
  var permissions = [];
  var temp;

  if(pass != passVerify){
    messageHandler.flashNewMessage("Password error", "Passwords do not match");
    return;
  }

  for(i=0; i<8; i++){
    temp = document.getElementById("eps_access"+i).checked;
    console.log(temp);
    if(temp)
      permissions = permissions + '1';
    else
      permissions = permissions + '0';
  }


  var url = "../";
  url = url + "createUser";
  url = url + "?username=" + username;
  url = url + "&password=" + pass;
  url = url + "&accessPrivileges=" + permissions;
  url = url + "&firstName=" + firstName;
  url = url + "&lastName=" + lastName; 
  url = url + "&timesheet=" + timesheet; 
  loadFile(url, createUserCB);
}

function createUserCB(){
  alert("user created");
  if(usersOpen == true)
  {
	showAllUsers();
  }
  
}


function showAllUsers(){
  var url = "../";
  url = url + "getUsers";
  loadFile(url, showAllUsersCB);
}

function showAllUsersCB(data){
  try
  {
    var parsedData = JSON.parse(data);
  }
  catch(e)
  {
    var parsedData = {};
  }

  var allUsers = '<table border="1">'
                +'<tr>'
                +'<th>Username</th>'
                +'<th>Name (last, first)</th>'
                +'<th>Access Privileges</th>'
                +'<th>Timesheet</th>'
                +'</tr>';
  for (i = 0; i< parsedData.length; i++){
    var object = parsedData[i];
    var username = object["username"];
    var privs = object["accessPrivileges"];
    var privsString='';
    if(privs!=null){
      if (privs[0]==1)
        privsString+="Page";

      if (privs[1]==1){
        if(privsString != "")
          privsString+=", ";
        privsString+="Churchill";
      }

      if (privs[2]==1){
        if(privsString != "")
          privsString+=", ";
        privsString+="Good Samaritan";
      }

      if (privs[3]==1){
        if(privsString != "")
          privsString+=", ";
        privsString+="Ironwood";
      }

      if (privs[4]==1){
        if(privsString != "")
          privsString+=", ";
        privsString+="Estrella";
      }

      if (privs[5]==1){
        if(privsString != "")
          privsString+=", ";
        privsString+="Rapid Care";
      }

      if (privs[6]==1){
        if(privsString != "")
          privsString+=", ";
        privsString+="Mid Level";
      }

      if (privs[7]==1){
        if(privsString != "")
          privsString+=", ";
        privsString+="Phoenix";
      }
    }

    allUsers+='<tr>'
            + '<td>'+object["username"]+'</td>'
            + '<td>'+object["lastName"]+', '+ object["firstName"]+'</td>'
            + '<td>'+privsString+'</td>'
            + '<td>'+object["timesheet"]+'</td>'
            + '<td>'+"<a href='javascript:deleteUserClicked("+'"'+username+'"'+")'>Delete</a>"+'</td>'
            + '<td>'+"<a href='javascript:changeUserPasswordClicked("+'"'+username+'"'+")'>Change Password</a>"+'</td>'
            + '</tr>';
  }
  allUsers+='</table>';
  getElementObject("allUsersBox").innerHTML = allUsers;
  getElementObject("hideAllUsersButton").innerHTML = "<a href='javascript:hideAllUsersClicked()'> <div class='glassFinish' id='logInButton' >Hide Users</div></a>";
  getElementObject("showAllUsersButton").innerHTML = "";
  usersOpen = true;

}

function deleteUserClicked(username){
  var r=confirm("Are you sure you want to delete " + username + "?");
    if (r==true){
        var url = "../";
        url = url + "deleteUser?username="+username;
        console.log(url);
        loadFile(url, deleteUserCB);
      }
}

function deleteUserCB(){
  console.log("EXTERMINATED");
  showAllUsers();
}

function showAllUsersClicked(){
  var url = "../";
  url = url + "getUsers";
  loadFile(url, showAllUsersCB);
}

function hideAllUsersClicked(){
   getElementObject("allUsersBox").innerHTML = "";
   getElementObject("hideAllUsersButton").innerHTML = "";
   getElementObject("showAllUsersButton").innerHTML = "<a href='javascript:showAllUsersClicked()'> <div class='glassFinish' id='logInButton' >Show All Users</div></a>";
   usersOpen = false;
}

function changeUserPasswordClicked(username){
  var newPassword = prompt("Please enter the new password for " + username);
  var newPassEnc = hex_md5(newPassword);
  var url = "../";
  url = url + "resetUserPassword?username="+username+"&password="+newPassEnc;
  loadFile(url, changeUserPasswordCB);
}

function changeUserPasswordCB(){
  alert("Success");
}
