var eps_userdocumentsinit = function()
{
  getElementObject("showUserDocumentsButton").innerHTML = "<a href='javascript:showUserDocumentsClicked()'> <div class='glassFinish' id='logInButton' >Refresh Documents</div></a>";
}

var documentsActive = function(){
  showUserDocumentsClicked();
}
  
function showUserDocumentsClicked(){
  var url = "../";
  url = url + "getFiles";
  loadFile(url, showUserDocumentsCB);
}

function showUserDocumentsCB(data){
  try
  {
    var parsedData = JSON.parse(data);
  }
  catch(e)
  {
    var parsedData = {};
  }

  var userDocumentLinks ='<table border="1">'
                +'<tr>'
                +'<th>File</th>'
                +'<th>Category</th>'
                +'<th>Description</th>'
                +'</tr>';
  for (i = 0; i< parsedData.length; i++){
    var object = parsedData[i];
    var category;
    userDocumentLinks+= '<tr>'
					 + '<th>' + "<a href='" + object.url + "'>" + object.name + "</a></th>"
           + '<th>' + object.category + '</th>'
					 + '<th>' + object.description + '</th></tr>';
  }
  getElementObject("documentsBox").innerHTML = userDocumentLinks;
}