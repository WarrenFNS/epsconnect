var eps_usermanageAccountinit = function()
{
  getElementObject("changePasswordButton").innerHTML = "<a href='javascript:changePasswordClicked()'> <div class='glassFinish' id='logInButton' >Change Password</div></a>";
  getElementObject("changePasswordBox").innerHTML = '<p class="text"> New Password</p> <input autocapitalize="off" autocorrect="off" type="password" class="input" id="new_user_password"> <p class="text"> Confirm Password </p> <input autocapitalize="off" autocorrect="off" type="password" class="input" id="confirm_new_user_password">';
}
  
function changePasswordClicked(){
	var password = hex_md5(getElementObject("new_user_password").value); 
	var confirmPassword = hex_md5(getElementObject("confirm_new_user_password").value);
	if(password == null || confirmPassword == null){
		alert("Do not leave a field empty");
	}else{
		if(password == confirmPassword){
			  var url = "../";
			  url = url + "getCurrentUser";
			  loadFile(url, changePasswordCB);
		} else {
			alert("passwords do not match");
		}
	}
}

function changePasswordCB(data){
  try
  {
    var parsedData = JSON.parse(data);
  }
  catch(e)
  {
    var parsedData = {};
  }
  if(parsedData!=null){
	  var password = hex_md5(getElementObject("new_user_password").value);
	  var url = "../";
	  url = url + "resetUserPassword?username=" + parsedData.username + "&password="+ password;
	  loadFile(url, resetCB);	
	}
}

function resetCB(){
	alert("Success!");
}