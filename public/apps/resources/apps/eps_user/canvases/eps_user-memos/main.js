var eps_usermemosinit = function()
{
  getElementObject("showUserMemosButton").innerHTML = "<a href='javascript:showUserMemosClicked()'> <div class='glassFinish' id='logInButton' >Refresh Memos</div></a>";
}
  
var memosActive = function(){
  showUserMemosClicked();
}

function showUserMemosClicked(){
  var url = "../";
  url = url + "getUserMemos";
  loadFile(url, showUserMemosCB);
}

function userMemosActive()
{
	showUserMemosClicked();
}
function showUserMemosCB(data){
  try
  {
    var parsedData = JSON.parse(data);
  }
  catch(e)
  {
    var parsedData = {};
  }

  var userMemos = '<table border="1">'
                +'<tr>'
                +'<th>Memo Date</th>'
                +'<th>Memo</th>'
                +'</tr>';
  for (i = 0; i< parsedData.length; i++){
    var object = parsedData[i];
    userMemos+='<tr>'
            + '<td>'+object["date"]+'</td>'
            + '<td>'+object["memo"]+'</td>'
            + '</tr>';
  }
  userMemos+='</table>';
  getElementObject("userMemosBox").innerHTML = userMemos;
}

function getUserMemos(){
  var url = "../";
  url = url + "getUserMemos";
  loadFile(url, getUserMemosCB);
}


function showMemosClicked(){
  showUserMemos();
}