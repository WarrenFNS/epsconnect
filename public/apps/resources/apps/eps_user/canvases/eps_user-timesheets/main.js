var timesheetType;
var username;

var timesheet;

var eps_usertimesheetsinit = function(){
	getElementObject("timesheetCreateButton").innerHTML = "<a href='javascript:timesheetCreateClicked()'> <div class='glassFinish' id='logInButton' >Create a new timesheet</div></a>";
	getElementObject("timesheetPostButton").innerHTML = "";
}

var timesheetsActive = function(){
	timesheetCreateClicked();
}

function timesheetCreateClicked(){
  var url = "../";
  url = url + "getCurrentUser";
  loadFile(url, getCurrentUserCB);	
}

function timesheetPostClicked() {
	console.log(timesheetType);
	var baseURL;
	var type = timesheetType;
	if(type != "Office"){
		var additionalHours = getElementObject("additional_hours").value;
		var specialRequests = getElementObject("special_requests").value;
		//var totalHours = getElementObject("total_hours").value;
		var baseURL = "../";
				baseURL = baseURL + "postTimesheet";
				baseURL = baseURL + "?username=" + username;
				baseURL = baseURL + "&timesheet=" + timesheet;
				baseURL = baseURL + "&additionalHours=" + additionalHours; 
				baseURL = baseURL + "&specialRequests=" + specialRequests;
				baseURL = baseURL + "&type=" + type; 
				//baseURL = baseURL + "&totalHours=" + totalHours; 
	}
	var url;
	switch(timesheetType)
	{
		case "PA":
			var hoursAfterMidnight = getElementObject("after_midnight_hours").value;
			var holidayHours = getElementObject("holiday_hours").value;
			url = baseURL + '&hoursAfterMidnight=' + hoursAfterMidnight 
						  + '&holidayHours=' + holidayHours;
			console.log(url);
			loadFile(url, timesheetPostCB);
			break;
		case "pageProvider":
			var goodSamaritanHours = getElementObject("good_samaritan_hours").value;
			var estrellaHours = getElementObject("estrella_hours").value;
			var ironwoodHours = getElementObject("ironwood_hours").value;	
			url = baseURL + '&goodSamaritanHours=' + goodSamaritanHours  
						  + "&estrellaHours=" + estrellaHours 
						  + "&ironwoodHours=" + ironwoodHours;
						  
			console.log(url);
			loadFile(url, timesheetPostCB);
			break;
		case "Phoenix":
			var hoursAfterMidnight = getElementObject("after_midnight_hours").value;
			var holidayHours = getElementObject("holiday_hours").value;
			var goodSamaritanHours = getElementObject("good_samaritan_hours").value;
			var estrellaHours = getElementObject("estrella_hours").value;
			var ironwoodHours = getElementObject("ironwood_hours").value;
			var monThursNightShiftHours = getElementObject("mon_thurs_night_shift_hours").value;
			var friSunNightShiftHours = getElementObject("fri_sun_night_shift_hours").value;
			var callInHours = getElementObject("call_in_hours").value;
			var extraCalPay = getElementObject("extra_cal_pay").value;
			
			url = baseURL + '&hoursAfterMidnight=' + hoursAfterMidnight 
						  + '&holidayHours=' + holidayHours 
						  + '&goodSamaritanHours=' + goodSamaritanHours 
						  + "&estrellaHours=" + estrellaHours 
						  + "&ironwoodHours=" + ironwoodHours 
						  + "&monThursNightShiftHours=" + monThursNightShiftHours 
						  + "&friSunNightShiftHours=" + friSunNightShiftHours 
						  + "&callInHours=" + callInHours
						  + "&extraCalPay=" + extraCalPay;
			console.log(url);
			loadFile(url, timesheetPostCB);
			
			break;

		case "Office":
			var websiteHours = getElementObject("website_hours").value;
			var vacation = getElementObject("vacation").value;
			var sick = getElementObject("sick").value;
			var holiday = getElementObject("holiday").value;
			var other = getElementObject("other").value;
			url = "../"
				+ "postTimesheet"
				+ "?username=" + username
				+ "&timesheet=" + timesheet
				+ "&type=" + type
				+ "&websiteHours=" + websiteHours
				+ "&vaction=" + vacation
				+ "&sick=" + sick
				+ "&holiday="  + holiday
				+"&other=" + other; 
			console.log(url);
			loadFile(url, timesheetPostCB);
		break;

		case "Churchill":
			var callIn = getElementObject("call_in_hours").value;
			var holidayHours = getElementObject("holiday_hours").value;
			url = baseURL + '&callInHours=' + callIn 
						  + '&holidayHours=' + holidayHours;
			console.log(url);
			loadFile(url, timesheetPostCB);
			break;

		default:
			console.log("ERROR");
	}
}

function  timesheetPostCB() {
	alert("posted");
	getElementObject("timesheetCreateButton").innerHTML = "<a href='javascript:timesheetCreateClicked()'> <div class='glassFinish' id='logInButton' >Create a new timesheet</div></a>";
	getElementObject("timesheetPostButton").innerHTML = "";
	getElementObject("timesheetInputTitle").innerHTML = "";
	getElementObject("timesheetInputBox").innerHTML = "";

}
function getCurrentUserCB(data){

   try
  {
    var parsedData = JSON.parse(data);
  }
  catch(e)
  {
    var parsedData = {};
  }
  
  timesheetType = parsedData.timesheet;
  username = parsedData.username;
  var sheetType;
  if (timesheetType == "Phoenix"){
  	sheetType = "monthly";
  }else{
  	sheetType = "bi-monthly";
  }

  loadFile('/getCurrentPayPeriod?sheetType='+sheetType, getCurrentPayPeriodCB);
}

function getCurrentPayPeriodCB(data){
 try
  {
    var parsedData = JSON.parse(data);
  }
  catch(e)
  {
    var parsedData = {};
  }
  
  timesheet = parsedData.timesheet;
  var timesheetBegin = '<b>Name:</b> '+ username +"<br>" +
	'<b>Timesheet:</b> ' + timesheet + "<br>";
	'<input autocapitalize="off" autocorrect="off" class="input" id="pay_period"><br>';

  var sharedClinics = '<b>Good Samaritan Clinical Hours:</b><br>' +
    '<input type="number" class="hours_input" id="good_samaritan_hours"><br>'+
	'<b>Estrella Clinical Hours:</b><br>' +
    '<input type="number" class="hours_input" id="estrella_hours"><br>'+
	'<b>Ironwood Clinical Hours:</b><br>' +
    '<input type="number" class="hours_input" id="ironwood_hours"><br>';
	
  var timesheetEnd = '<b>Additional Hours:</b><br>'+
    '<input autocapitalize="off" autocorrect="off" class="input" id="additional_hours"><br>'+
	'<b>Special Requests</b><br>'+
	'<input autocapitalize="off" autocorrect="off" class="input" id="special_requests"><br>'+
	'Total Hours:<br>'+
	'<br>'+
	'<input type="checkbox" id="confirm">I agree that the above hours are correct<br>';

  var phoenixAndPA = '<b>Hours after midnight:</b><br>'+
	'<input type="number" class="hours_input" id="after_midnight_hours"><br>'+
	'<b>Holiday hours:</b><br>'+
	'<input type="number" class="hours_input" id="holiday_hours"><br>';
	
  var phoenixExclusive = '<b>Mon-Thurs Night shift hours</b> (7p-7a, 6p-6a, 8p-6a, 11p-7a, 6p-4a)<b>:</b><br>'+
	'<input type="number" class="hours_input" id="mon_thurs_night_shift_hours"><br>'+
	'<b>Fri-Sun Night shift hours</b> (7p-7a, 6p-6a, 8p-6a,6p-4a)<b>:</b><br>'+
	'<input type="number" class="hours_input" id="fri_sun_night_shift_hours"><br>'+
	'<b>Call-in:</b><br>'+
	'<input type="number" class="hours_input" id="call_in_hours"><br>'+
	'<b>Extra Cal Pay:</b><br>'+
	'<input type="number" class="hours_input" id="extra_cal_pay"><br>';

	var churchillExculsive = '<b>Call-in:</b><br>'+
	'<input type="number" class="hours_input" id="call_in_hours"><br>'+
	'<b>Holiday hours:</b><br>'+
	'<input type="number" class="hours_input" id="holiday_hours"><br>';

	var officeExclusive = '<b>Website Hours:</b><br>'+
	'<input type="number" class="hours_input" id="website_hours"><br>'+
	'<b>Vacation:</b><br>'+
	'<input type="number" class="hours_input" id="vacation"><br>'+
	'<b>Sick:</b><br>'+
	'<input type="number" class="hours_input" id="sick"><br>'+
	'<b>Holiday:</b><br>'+
	'<input type="number" class="hours_input" id="holiday"><br>'+
	'<b>Other:</b><br>'+
	'<input type="number" class="hours_input" id="other"><br>';
	
  var phoenixTimesheet = timesheetBegin + sharedClinics + phoenixExclusive + phoenixAndPA + timesheetEnd;

  var PATimesheet = timesheetBegin + phoenixAndPA + timesheetEnd;

  var pageProviderTimesheet = timesheetBegin + sharedClinics + timesheetEnd;

  var churchillTimesheet = timesheetBegin + churchillExculsive +timesheetEnd;

  var officeTimesheet = timesheetBegin + officeExclusive;
	
  console.log(timesheetType);
  switch(timesheetType)
  {
    case "PA":
		getElementObject("timesheetInputTitle").innerHTML = "New " + timesheetType + " Timesheet ";
		getElementObject("timesheetInputBox").innerHTML = PATimesheet;
		getElementObject("timesheetCreateButton").innerHTML = "";
		getElementObject("timesheetPostButton").innerHTML = "<a href='javascript:timesheetPostClicked()'> <div class='glassFinish' id='logInButton' >Submit timesheet</div></a>";
		break;
	case "pageProvider":
		getElementObject("timesheetInputTitle").innerHTML = "New " + timesheetType + " Timesheet";
		getElementObject("timesheetInputBox").innerHTML = pageProviderTimesheet;
		getElementObject("timesheetCreateButton").innerHTML = "";
		getElementObject("timesheetPostButton").innerHTML = "<a href='javascript:timesheetPostClicked()'> <div class='glassFinish' id='logInButton' >Submit timesheet</div></a>";
		break;
	case "Phoenix":
		getElementObject("timesheetInputTitle").innerHTML = "New " + timesheetType + " Timesheet";
		getElementObject("timesheetInputBox").innerHTML = phoenixTimesheet;
		getElementObject("timesheetCreateButton").innerHTML = "";
		getElementObject("timesheetPostButton").innerHTML = "<a href='javascript:timesheetPostClicked()'> <div class='glassFinish' id='logInButton' >Submit timesheet</div></a>";
		break;
	case "Office":
		getElementObject("timesheetInputTitle").innerHTML = "New " + timesheetType + " Timesheet";
		getElementObject("timesheetInputBox").innerHTML = officeTimesheet;
		getElementObject("timesheetCreateButton").innerHTML = "";
		getElementObject("timesheetPostButton").innerHTML = "<a href='javascript:timesheetPostClicked()'> <div class='glassFinish' id='logInButton' >Submit timesheet</div></a>";
	break;
	case "Churchill":
	getElementObject("timesheetInputTitle").innerHTML = "New " + timesheetType + " Timesheet ";
		getElementObject("timesheetInputBox").innerHTML = churchillTimesheet;
		getElementObject("timesheetCreateButton").innerHTML = "";
		getElementObject("timesheetPostButton").innerHTML = "<a href='javascript:timesheetPostClicked()'> <div class='glassFinish' id='logInButton' >Submit timesheet</div></a>";
	break;
	default:
	console.log("ERROR");
  }
}

