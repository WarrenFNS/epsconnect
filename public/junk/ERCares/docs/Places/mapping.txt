APIPA : TAPIPA : Transportation Services
ASU Community Health Services : hcASU :  Mental Health Services, Low-cost clinic,  Scottsdale
Adelante Health Care : hcSunCIty : Low-cost clinic,  Mirage, Surprise, Sun City
Ak Chin Health Clinic : hcAkChin : Low-cost clinic,  Maricopa
Arizona School of Dentistry & Oral Health : DentATstill : Dental clinic,  Low-cost clinic, Glendale, Peoria, Mesa
Avondale Family Health Center- MIHS : HCAvon : Low-cost clinic,  Avondale
Avondale Primary Care Center : PCCAvon :  STD clinic, Low-cost clinic, Avondale
Banner Good Samaritan Residency Clinic : BGSRC : Mental Health Services, Phoenix
Banner Hospitals and Medical Centers : banner1 : Hospitals, Mesa, Phoenix, Sun City, Scottsdale, Glendale, Valleywide
Banner Outpatient and Other Services : banner3 : Hospitals, Phoenix, Glendale, Mesa, Peoria, Sun City, Valleywide
Banner Research Locations : banner2 : Hospitals, Phoenix, Sun City
Bridgeway AHCCCS : TBridge : Transportation Services
Buckeye Dental Office : DentBuck : Dental clinic,  Avondale, Goodyear, Buckeye
Buckeye Family Care Center : hcBuck : Low-cost clinic,  Avondale, Goodyear, Buckeye
CVS 24-hr Pharmacy Camelback : CVSCamel : 24-hr Pharmacy, Phoenix
CVS 24-hr Pharmacy Glendale : CVSGlendale : 24-hr Pharmacy, Glendale
CVS 24-hr Pharmacy North Phoenix : CVSNPHX : 24-hr Pharmacy, Phoenix
CVS 24-hr Pharmacy Peoria : CVSPeoria : 24-hr Pharmacy, Peoria
CVS 24-hr Pharmacy Phoenix : CVSPHX : 24-hr Pharmacy, Phoenix
Care 1st : Tcare1st : Transportation Services
Care Partnership Pediatric Clinic : hcCarePart : Dental clinic, Low-cost clinic, Mesa
Catholic Social Services : CSS : Mental Health Services
Central AZ Shelter Services : HCshelter : Dental clinic, Low-cost clinic, Phoenix
Central Arizona Dental Society : DentCADS : Dental clinic, Valleywide
Chandler Family Health Center- MIHS : HCchandler : Mental Health Services, Dental clinic, Low-cost clinic,  Chandler
Chinese Senior Center : HCchinese : Low-cost clinic, Phoenix
Clinica Adelante- Administrative Office : hcClinicAda : Low-cost clinic,  Mirage, Surprise, Sun City
Community Information and Referral : 211 :  Valleywide
Comprehensive Health Center- MIHS : HCcompr : Low-cost clinic, Phoenix
Coronado Dental Services for Homebound Patients : DentCo : Dental clinic,  Valleywide
DMTS (Dependable Medical Transportation Service) : TDMTS : Transportation Services
Desert Senita Community Center : HCCDSCC : Mental Health Services, Low-cost clinic, Dental clinic, Ajo
East Valley Family Care : hcmesaFam : Low-cost clinic,  Mesa
El Mirage Family Health Center- MIHS : HCmirage : Low-cost clinic,  Mirage, Surprise, Sun City
El Mirage Primary Care Center : PCCelM :  STD clinic, Low-cost clinic,  Mirage, Surprise, Sun City
Escalante Health/ Tempe Community Action Agency : hcTempe : Low-cost clinic,  Tempe
Evercare : Tevercare : Transportation Services
Family Planning Clinic/ CARE Partnership, Inc. : hcFamPlan : Low-cost clinic,  Mesa
Family Service Agency : FSA : Mental Health Services, Phoenix
Gila Bend Primary Care Center  : PCCGilaBend :  Low-cost clinic, Gila Bend
Gila Crossing Health Clinic : HCGC : Dental clinic, Low-cost clinic,  Laveen
Glendale Dental Group : Dentglen : Dental clinic, Glendale, 
Glendale Family Health Care : hcClinAdaGlen : Low-cost clinic, Glendale
Glendale Family Health Center- MIHS : HCGlen : Low-cost clinic,  Glendale
Glendale Primary Care Center : PCCglen :  STD clinic, Low-cost clinic,  Glendale
Good Samaritan Clinic : HCgoodsam : Low-cost clinic
Guadalupe Family Health Center- MIHS : HCGuad : Low-cost clinic,  Guadalupe
HEALTHNET : THnet : Transportation Services
HUMANA : Thumana : Transportation Services
Health Care for the Homeless : HCcarehome : Mental Health Services, Low-cost clinic
Health Choice of Arizona : THCA : Transportation Services
HealthCare Connect : HCconnect : Low-cost clinic, Phoenix, Valleywide 
HomeBase Youth Services : HCyouth : Low-cost clinic, Phoenix
Indian Health Services : TIHS : Transportation Services
Jewish Family and Children : JFC : Mental Health Services
John C Lincoln Hospital North Mountain : DentJCL : Dental clinic, Phoenix
John C. Lincoln Hospital- North Mountain : HCjohnC : Dental clinic, Low-cost clinic
KidsCare : HCkids : Low-cost clinic, Phoenix
Longview Community Church : HCchurch : Low-cost clinic, Phoenix
Luke Air Force Base : PCCLAFB :  STD clinic, Low-cost clinic,  Glendale
Magellan Behavioral Health : MBH : Mental Health Services
Maricopa County Dept. of Public Health : HCpublicH : Low-cost clinic, Valleywide, Phoenix
Maricopa County Health Care Adult Immunizations : HCmaricopa : Low-cost clinic, Phoenix
Maricopa County Health Care Child Immunizations : HCmaricopaCh : Low-cost clinic, Phoenix
Maricopa County Health Care Homeless Shelter : HCHS : Low-cost clinic, Phoenix
Maricopa County Health Care Scottsdale : hcMariScot : Low-cost clinic,  Scottsdale
Maricopa County Health Clinic : HCMCHcl : Low-cost clinic, Phoenix
Maricopa County STD Clinic : HCSTD : Low-cost clinic, Phoenix
Maricopa Health-University Physicians Health Plan : TMHUplan : Transportation Services
Maricopa Integrated Health Services : DentMIHS : Dental clinic, Phoenix
Maricopa Integrated Health System (MIHS) : HCMCHC : Low-cost clinic, Phoenix, Valleywide
Maricopa Medical Center : PCCmaricopa :  STD clinic, Low-cost clinic, Phoenix
Maryvale Family Health Center- MIHS : HCmaryV : Low-cost clinic, Phoenix
Mayo Clinic : hcMayoScotts : Low-cost clinic, Scottsdale
McDowell Healthcare Center- MIHS : HCMIHS : Mental Health Services, Dental clinic, Low-cost clinic, Phoenix
Mercy Care Transport : TMCT : Transportation Services
Mercy Health Care Center : HCmercy : Dental clinic, Low-cost clinic, Phoenix
Mesa Family Health Center- MIHS : HCmesa : Low-cost clinic, Mesa
Mesa Senior Services : hCmesaSr : Low-cost clinic, Dental clinic,  Mesa
Mission Family Medical Center : hcMesaMission : Low-cost clinic,  Mesa
Mission of Mercy : HCmission :  Low-cost clinic, Phoenix, Avondale, Mesa
Mission of Mercy Care Partnership Clinics : HCmissMary : Low-cost clinic, Phoenix
Monroe Place : HCMonroe : Low-cost clinic
Mountain Park Health Center- Maryvale : hcParkMary : Low-cost clinic, Phoenix
Mountain Park Health Center- Tolleson : hcParkToll :  Dental clinic, Low-cost clinic, Tolleson
Mountain Park Health Center-Baseline : DENTmount :  Dental clinic, Low-cost clinic, Phoenix
Mountain Park Health Center-East Phoenix : HCPark : Low-cost clinic, Phoenix
Mountain Park Health Center-Tempe : hcMtnPark :  Mental Health Services, Dental clinic, Low-cost clinic, Tempe 
Native American Community Health Center : DentNACHC : Mental Health Services, Low-cost clinic, Dental clinic, Phoenix
Native American Community Health Center-West : hcNativeAmerican : Low-cost clinic, Phoenix
Neighborhood Christian Clinic : HCneighChrist : Low-cost clinic, Phoenix
North Tempe Community Center :  hcNorTem : Low-cost clinic, Tempe
Ortho Clinic- MIHS : HCORTHO : Low-cost clinic, Phoenix
PACFICARE : Tpacifi : Transportation Services
Pascua Yaqui Health Services : hcYaqui : Low-cost clinic,  Guadalupe
Phoenix College Dental Clinic : DentPCDC : Dental clinic, Low-cost clinic, Phoenix
Phoenix Foundation for Homeless Children : DentPFHC : Dental clinic, Low-cost clinic, Phoenix
Phoenix Foundation for Homeless Children : hcHmlsChld : Dental clinic, Low-cost clinic, Phoenix
Phoenix Health Plan : TPHP : Transportation Services
Phoenix Indian Medical Center : PCCindian :  STD clinic, Low-cost clinic, Phoenix
Phoenix Indian Medical Center Dental Clinic : DentPIMC : Dental clinic, Low-cost clinic, Phoenix
Public Health Clinic : PCCpublic :  STD clinic, Low-cost clinic, Phoenix
Recovery Innovations : RI : Mental Health Services, Peoria
Roosevelt Primary Care Center : PCCRoos :  STD clinic, Low-cost clinic, Phoenix
Rural Health Team : HcRural : Low-cost clinic,  Buckeye
SCAN ALTCS : Tscan : Transportation Services
Seventh Ave Walk-in Clinic- MIHS : HCMC7th: Low-cost clinic, Phoenix
Seventh Avenue Family Health Center- MIHS : HC7th : Low-cost clinic, Phoenix
Seventh Avenue Primary Care Center : PCC7th :   STD clinic, Low-cost clinic, Phoenix
Society of Saint Vincent de Paul Diocese of Phoenix : DentStVinc : Dental clinic, Low-cost clinic, Phoenix
South Central Family Health Center- MIHS  : HCsouth : Low-cost clinic, Phoenix
South Phoenix Primary Care Center : PCCsPHX :  STD clinic, Low-cost clinic, Phoenix
St Joesph's Womens Wellness Clinic : HCstJOE : Low-cost clinic
St John Vianney Church : HCstJohn : Low-cost clinic,  Goodyear
Sun City West Adelante Women's Health Center : hcAdelanWomenSun : Low-cost clinic,  Sun City
Sunnyslope Family Health Center- MIHS : HCsunny : Low-cost clinic, Phoenix
Sunnyslope Family Service Center : hcSunnyFam : Low-cost clinic, Phoenix
Sunnyslope Primary Care Center : PCCsunny :  STD clinic, Low-cost clinic, Phoenix
Sunnyslope Senior Services : hcSunnySr : Low-cost clinic, Phoenix
The Neighborhood Clinic : hcNeighCl : Low-cost clinic, Phoenix
Tidwell Family Care Center : hcTidwell : Low-cost clinic,  Surprise
Tooth Doctor for Kids : DentToothDR : Dental clinic, Low-cost clinic, Phoenix
UNITED HEALTHCARE : TunitedHC : Transportation Services
Urgent Psychiatric Center : UPC : Mental Health Services, Phoenix
Veteran's Administration Medical Center : VA : Dental clinic, Low-cost clinic, Phoenix
Walgreens 24-hr Pharmacy Glendale : WALGLEN : 24-hr Pharmacy, Glendale
Walgreens 24-hr Pharmacy Goodyear : WALGOOD : 24-hr Pharmacy, Goodyear
Walgreens 24-hr Pharmacy Metrocenter : WALMETRO : 24-hr Pharmacy, Phoenix
Walgreens 24-hr Pharmacy North Glendale : WALGLENN : 24-hr Pharmacy, Glendale
Walgreens 24-hr Pharmacy North Phoenix : WALPHXN : 24-hr Pharmacy, Phoenix
Walgreens 24-hr Pharmacy Peoria : WALPEORIA : 24-hr Pharmacy, Peoria
Walgreens 24-hr Pharmacy Phoenix : WALPHX : 24-hr Pharmacy, Phoenix
Walgreens 24-hr Pharmacy West Phoenix : WALPHXW : 24-hr Pharmacy, Phoenix, Glendale
Wesley Community Center/ Centro del Salud  : hcSalud : Low-cost clinic, Phoenix
Western Dental Services : DentWDS : Dental clinic, Low-cost clinic, Phoenix
Wickenburg Family Care Center : hcWicken : Wickenburg, Low-cost clinic
Women's Health : hcWomenHealth : Low-cost clinic, Sun City