ERNavLabel: ER Navigator : ER Navegador
HRLabel: Health Records : Archivos de Salud
mhrText: Health Records : Archivos de Salud
HILabel: Health Info : Informaci&oacute;n de Salud
hiText: Health Information : Informaci&oacute;n de Salud
gamesLabel: Games : Juegos
AERabel: After ER : Despu&eacute;s de ER
NotLabel: Notifications : Notificaciones
prefTitle: Preferences : Ajustes
textLabel2: Last Name : Apellido
textLabel1: First Name : Nombre 
filterSearchButton: Search : Buscar
filterSearchGoButton: Search : Buscar
iFrameTitle: Details : Detalles
mpText: Medical Problems : Problemas m&iacute;dicos
surgsText: Surgery/Procedures : Cirug&iacute;a/procedimientos
filterAddNewButton: Add New : A&ntilde;adir nuevos