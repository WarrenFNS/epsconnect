var ERSteps = ["intake", "quickLook", "evaluation", "continuingCare", "observationDischarge"];


var ERStatus = [];
var ERStatusDefault = "1000000000";
var allAfterER = new Array();
var allAfterERDirDict = new Array();
var allAfterERTags = new Array();

var mpSelAlphabet = "none";
var hiSelAlphabet = "none";

var theKeyBoard;
var currentSelectedERScreen = "";
var currentLang = "English"
var currentStartupPref = true;
var currentHospital = "Estrella";
             
var langID = new Array();
var langEN = new Array();
var langES = new Array();

var allSurgsEN = new Array();
var allSurgsES = new Array();


var allDiseasesEN = new Array();
var allDiseasesES = new Array();


var medicationKeys = [
"ADHD",
"Angina",
"Antianxiety",
"Antibiotics",
"Antidepressants",
"Antihistamine",
"Decongestant",
"Antihypertensive Agents",
"Antiemetic",
"Antipsychotic & Combinations",
"Antivirals",
"Arthritis (Osteo or Rheumatoid)",
"Asthma/COPD",
"Benign Prostate Hypertrophy",
"Cholesterol Lowering Agents",
"Crohn’s Disease/Ulcerative Colitis",
"Diabetes",
"Diuretics",
"Genito-Urinary Agents",
"GERD/ulcers",
"Incontinence",
"Migraines",
"Muscle Relaxants",
"Pain Relievers",
"Parkinson’s Disease Primary",
"Vitamins/Supplements",
"Thyroid Agents"
];


var medication = new Array();

medication["ADHD"] = [
"Concerta", 
"Cylert", 
"Dexedrine",
"Focalin",
"Ritalin LA"
];

medication["Angina"] = [
"Imdur",
"Minitran",
"Nitrik",
"Nitro-Dur",
"Nitrogard",
"Nitrolingual Spray"
];

medication["Antianxiety"] = [
"Buspar",
"Dividose",
"Diastat",
"Klonopin",
"Valium",
"Vistaril"
];

medication["Antibiotics"] = [
"Amoxicillin",
"Augmentin",
"Avelox",
"Bactroban",
"Bactrim DS",
"Biaxin",
"Ceclor",
"Ceftin",
"Cefzil",
"Cephalexin",
"Ceptaz",
"Ciloxan",
"Cipro",
"Cleocin",
"Clindamycin",
"Dynacin",
"Levaquin",
"Macrobid",
"Macrodantin",
"MetroCream",
"MetroGel",
"Minocin",
"Omnicef",
"TEQUIN",
"VIBRAMYCIN",
"Zithromycin",
"Zyvox"
];

medication["Antidepressants"] = [
"Celexa",
"Desyrel Dividose",
"Effexor",
"Lexapro",
"Paxil",
"Prozac",
"Remeron",
"Serzone",
"Sinequan",
"Wellbutrin",
"Zoloft",
"Zyban"
];

medication["Antihistamine"] = [
"Benadryl",
"Allegra",
"Claritin",
"Vistaril",
"Zyrtec"
];

medication["Decongestant"] = [
"Allegra-D",
"Claritin-D",
"Zyrtec-D"
];

medication["Antihypertensive Agents"] = [
"Accupril",
"Accuretic",
"Aceon",
"Adalat CC",
"Altace",
"Atacand",
"Avalide",
"Benicar",
"Cardene",
"Cardene",
"Cardura",
"Catapres",
"Coreg",
"Corgard",
"Covera HS",
"Diovan",
"Diovan HCT",
"Dynacirc CR",
"Dyazide",
"Enalopril",
"Hytrin",
"Hyzaar",
"Inderal LA",
"Inderal Tablets",
"Inderide",
"Isoptin SR",
"Lexxel",
"Lisinopril",
"Loressor",
"Lotensin",
"Lotensin HCT",
"Lotrel",
"Mavik",
"Maxzide",
"Maxzide-25",
"Micardis",
"Minipress",
"Minizide",
"Moduretic",
"Monopril",
"Monopril-HCTZ",
"Nimotop",
"Norvasc",
"Plendi",
"Prinivil",
"Prinizide",
"Proamatine",
"Procardia",
"Procardia XL",
"Sular",
"Tarka",
"Teveten",
"Tiazac",
"Toprol XL",
"Vascor",
"Vasotec"
];

medication["Antiemetic"] = [
"Compazine",
"Phenergan",
"Reglan",
"Zofran"
];

medication["Antipsychotic & Combinations"] = [
"Abilify",
"Clozaril",
"Eskalith CR",
"Etrafon",
"Geodon",
"Haldol",
"Moban",
"Navane",
"Prolixin",
"Risperdal (separate program)",
"Seroquel",
"Stelazine",
"Zyprexa"
];

medication["Antivirals"] = [
"Acyclovir",
"Cytovene",
"Famvir",
"Foscavir",
"Relenza",
"Tamiflu",
"Valcyte",
"Valtrex",
"Viroptic",
"Vistide",
"Zovirax"
];

medication["Arthritis (Osteo or Rheumatoid)"] = [
"Arava",
"Arthrotec",
"Enbrel",
"Plaquenil"
];

medication["Asthma/COPD"] = [
"Accolate",
"Advair/Diskus",
"Aerobid",
"Aerobid-M",
"Aerochamber",
"Aerochamber w/Mask",
"Alupent",
"Atrovent MDI",
"Atrovent Nasal",
"Azmacort Inhalation Aerosol",
"Beconase",
"Beconase AQ",
"Combivent MDI",
"Ellipse spacer",
"Flovent",
"Flovent Rotadisk",
"Foradil",
"Maxair Autohaler",
"Maxalt",
"Nasacort Nasal Inhaler",
"Nasonex",
"Proventil",
"Pulmicort Respules",
"Pulmicort Inhaler",
"Quibron",
"Quibron-T",
"Quibron-T/SR",
"Rhinocort AQ",
"Serevent Diskus",
"Singulair",
"Theochron",
"Theolair",
"Tilade inhaler",
"Ventolin",
"Zyflo" 
];

medication["Benign Prostate Hypertrophy"] = [
"Flomax",
"Propecia",
"Proscar"
];

medication["Cholesterol Lowering Agents"] = [
"Advicor",
"Crestor",
"Lescol/XL",
"Lipitor",
"Mevacor",
"Pravachol",
"Tricor tablets",
"Zetia",
"Zocor"
];

medication["Crohn’s Disease/Ulcerative Colitis"] = [
"Asacol",
"Pentasa",
"Remicade",
"Rowasa"
];

medication["Diabetes"] = [
"Actos",
"Amaryl",
"Avandamet",
"Avandia",
"Avapro",
"Diabinese",
"Rhinal",
"Glucagon",
"Glucophage",
"Glucophage XR",
"Glucotrol",
"Glucotrol XL",
"Glucovance",
"Glyset",
"Humalog",
"Humulin",
"Humulin 50/50",
"Humulin 70/30",
"Humulin L,N,N/NPH,R,U",
"Humulin Ultralente",
"Iletin II",
"Lantus",
"Novofine (needles)",
"Novolin (all)",
"Novolog",
"Novopen (complete set up includes pen, vials, insulin, needles)",
"Prandin",
"Precose",
"Starlix",
"Velosulin BR"
];

medication["Diuretics"] = [
"Bumex",
"Clorpres",
"Demadex",
"Mykrox Tab",
"Naqua",
"Naturetin",
"Thalitone",
"Zaroxolyn"
];

medication["Genito-Urinary Agents"] = [
"Aci-Jel",
"Aldara",
"Calcibind",
"Hiprex",
"Lithostat",
"Monistat-Derm",
"Thiola",
"Urispas",
"Viagra"
];

medication["GERD/ulcers"] = [
"Aciphex",
"Carafate suspension",
"Carafate tablets",
"Cytotec",
"Nexium",
"Pepcid",
"Prevacid",
"Protonix",
"Tagamet",
"Zantac"
];

medication["Incontinence"] = [
"Detrol LA",
"Ditropan XL"
];

medication["Migraines"] = [
"Amerge",
"Avonex",
"DHE 45",
"Imitrex",
"Migranal",
"Phrenilin",
"Phrenilin Forte",
"Relpax"
];

medication["Muscle Relaxants"] = [
"Dantrium",
"Flexeril",
"Norgesic",
"Norgesic Forte",
"Parafon Forte DSC"
];

medication["Pain Relievers"] = [
"Actiq",
"Anaprox",
"Anaprox DS",
"Bextra",
"Bupap",
"Celebrex",
"Clinoril",
"Darvon",
"Darvon-N",
"Darvocet",
"Darvocet-N",
"Disalcid capsules",
"Dolobid",
"Duragesic",
"EC – Naprosyn",
"Elmiron (urinary analgesic)",
"Esgic",
"Feldene",
"Hydrocet",
"Indocin",
"Lodine Caps",
"Lodine Tabs",
"Lodine XL",
"Mobic",
"MS Contin",
"MS Oral Solution",
"MSIR",
"Naprosyn",
"Oruvail",
"Oruvail Caps ER",
"OxyFast",
"Oxy IR",
"Oxycontin",
"Ponstel Caps",
"Relafen",
"Tolectin",
"Tolectin DS",
"Ultram",
"Vioxx",
"Voltaren-XR",
"Ultracet Ultram"
];

medication["Parkinson’s Disease Primary"] = [
"Comtan",
"Dostinex",
"Eldepryl",
"Kemadrin",
"Lodosyn",
"Mirapex",
"Norflex",
"Parlodel",
"Permax",
"Requip",
"Sinemet",
"Symmetrel",
"Tasmar"
];

medication["Vitamins/Supplements"] = [
"AquaSol A",
"AquaSol E Drops",
"Calcium carbonate",
"Drisdol",
"Ferrelicit",
"Hectoral",
"Hytakerol",
"Iberet-Folic 500",
"Infed",
"KCl Powder Packets",
"K-Dur (20 meq only)",
"K-Lor Klotrix",
"K-Lyte DS",
"K-Lyte/CL",
"K-Lyte Tab",
"K-Tab",
"Materna",
"Mephyton",
"MVI Pediatric",
"MVI 12",
"Neutra-Phos",
"Neutra-Phos K",
"Rocaltrol",
"Sodium bicarbonate",
"Theragram Hematinic"
];

medication["Thyroid Agents"] = [
"Armour Thyroid",
"Cytomel",
"Levothroid",
"Levoxyl",
"Synthroid",
"Tapazole",
"Thyrolar"
];

var allHealthIssues = new Array();
var allHealthIssuesES = new Array();
var allHealthIssuesDirDict = new Array();

var allInsurance = new Array();
var allInsuranceNos = new Array();

function load()
{
	arrangeScreens();
    setTimeout('getElementById("splashScreen").style.display = "none"', 2500);
	setTimeout('getElementById("splashScreen").style.opacity = "0";', 2000);
    PersistantValue.refreshGlobals = initializePersistantValues;
	PersistantValue.refreshGlobals();
    startUp();
}

var PV={
	dob: "",
	passwd: "",
	myFirstName: "",
	myLastName: "",
	myDiseases: ",",
	myMeds: ",",
	myAllergies: ",",
	mySurgeries: ",",
	myInsurances: ",",
    myNots: ",",
    customDiseases: "",
    customMeds: "",
    customMedAlgs: "",
    customSurgs: "",
    customIns: "",
	passwdSet: "no",
	dobSet: "no",
	nameSet: "no",
	myRandom: null,
	authToken: ""
}

var randomsalt = 1;
var patientID;

             
function getRandomDecString()
{
	return Math.floor(Math.random() * 1000000000000000000).toString();
}   

function initializePersistantValues()
{
    if (PersistantValue.check("myNots")){
        PV.myNots = PersistantValue.get("myNots");      
    }
    else{   
        PersistantValue.set("myNots", ",");       
        PV.myNots = ",";
    }  
    
    if (PersistantValue.check("customDiseases")){
        PV.customDiseases = PersistantValue.get("customDiseases");      
    }
    else{   
        PersistantValue.set("customDiseases", "");       
        PV.customDiseases = "";
    }  
    
    
    if (PersistantValue.check("customMeds")){
        PV.customMeds = PersistantValue.get("customMeds");      
    }
    else{   
        PersistantValue.set("customMeds", "");       
        PV.customMeds = "";
    }  
    
    
    if (PersistantValue.check("customMedAlgs")){
        PV.customMedAlgs = PersistantValue.get("customMedAlgs");      
    }
    else{   
        PersistantValue.set("customMedAlgs", "");       
        PV.customMedAlgs = "";
    }  
    
    
    if (PersistantValue.check("customSurgs")){
        PV.customSurgs = PersistantValue.get("customSurgs");      
    }
    else{   
        PersistantValue.set("customSurgs", "");       
        PV.customSurgs = "";
    }  
    
    
    if (PersistantValue.check("customIns")){
        PV.customIns = PersistantValue.get("customIns");      
    }
    else{   
        PersistantValue.set("customIns", "");       
        PV.customIns = "";
    }  
    
    
    
    
    
    
    
    
    
    if (PersistantValue.check("currentLang")){
        currentLang = PersistantValue.get("currentLang");      
    }
    else{   
        PersistantValue.set("currentLang", "English");       
        currentLang = "English";
    }                 

    if (PersistantValue.check("authToken")){
        PV.authToken = PersistantValue.get("authToken");      
    }
    else{   
        PersistantValue.set("authToken", "");       
        PV.authToken = "";
    }

    if (PersistantValue.check("myRandom")){
        PV.myRandom = PersistantValue.get("myRandom");      
    }
    else{   
		var temp = getRandomDecString();
        PersistantValue.set("myRandom", temp);       
        PV.myRandom = temp;
    }
            
    
	if (PersistantValue.check("passwd")){
        PV.passwd = PersistantValue.get("passwd");      
    }
    else{
        PersistantValue.set("passwd", "none");       
        PV.passwd = "none";
    }



    if (PersistantValue.check("dobSet")){
        PV.dobSet = PersistantValue.get("dobSet");      
    }
    else{
        PersistantValue.set("dobSet", "no");       
        PV.dobSet = "no";
    }


	
    if (PersistantValue.check("dob")){
        PV.dob = PersistantValue.get("dob");      
    }
    else{
        PersistantValue.set("dob", "");       
        PV.dob = "";
    }

    if (PersistantValue.check("myDiseases")){
		PV.myDiseases = PersistantValue.get("myDiseases");
    }
    else{
        PersistantValue.set("myDiseases", ",");       
        PV.myDiseases = ",";
    }
	  
	if (PersistantValue.check("myMeds")){
			PV.myMeds = PersistantValue.get("myMeds");
	}
    else{
        PersistantValue.set("myMeds", ",");       
        PV.myMeds = ",";
    }  

	if (PersistantValue.check("myAllergies")){
			PV.myAllergies = PersistantValue.get("myAllergies");
	}
    else{
        PersistantValue.set("myAllergies", ",");       
        PV.myAllergies = ",";
    }

	if (PersistantValue.check("mySurgeries")){
			PV.mySurgeries = PersistantValue.get("mySurgeries");
	}
    else{
        PersistantValue.set("mySurgeries", ",");       
        PV.mySurgeries = ",";
    }            

	if (PersistantValue.check("myInsurances")){
			PV.myInsurances = PersistantValue.get("myInsurances");
	}
    else{
        PersistantValue.set("myInsurances", ",");       
        PV.myInsurances = ",";
    }


                    
	//PersistantValue.set("passwdSet", "no");       
    
	if (PersistantValue.check("passwdSet")){
		PV.passwdSet = PersistantValue.get("passwdSet");
    }
    else{
        PersistantValue.set("passwdSet", "no");       
        PV.passwdSet = "no";
    }

	if (PersistantValue.check("nameSet")){
		PV.nameSet = PersistantValue.get("nameSet");
    }
    else{
        PersistantValue.set("nameSet", "no");       
        PV.nameSet = "no";
    }


	if (PersistantValue.check("myFirstName")){
		PV.myFirstName = PersistantValue.get("myFirstName");
    }
    else{
        PersistantValue.set("myFirstName", "");       
        PV.myFirstName = "";
    }

	if (PersistantValue.check("myLastName")){
		PV.myLastName = PersistantValue.get("myLastName");
    }
    else{
        PersistantValue.set("myLastName", "");       
        PV.myLastName = "";
    }

}

function not10Clicked()
{
	new Audio('not10.m4a').play();
	return;
}

function cancelPasswdChange()
{
	getElementStyleObject("passwdWrapper").display = "none";
	getElementObject("currentPasswd").blur();
	                      
	getElementObject("currentPasswd").value = "";
	
	return;
}

function continuePasswordChange(){
	getElementObject("currentPasswd").blur();
	
	var pass = getElementObject("currentPasswd").value;
	                     	
	if (hex_md5(pass) === PV.passwd){
		setTimeout('getElementStyleObject("setPassword").display = "block"; getElementStyleObject("currentPasswd").focus();', 00);
		getElementStyleObject("passwdWrapper").display = "none";
		
	}
	
	else{
		messageHandler.flashNewMessage("Incorrect Password", "try again");
	}             
	
	getElementObject("currentPasswd").value = "";
	
	return;
}
              
function passwdContinue()
{
	var passt = getElementObject("passwdTextField").value;
	getElementObject("passwdTextField").blur();
               
	if(hex_md5(passt) == PV.passwd){                                           
		setTimeout('getElementStyleObject("passwordBlock").display = "none"', 500);
   		runKron();
	}                                                            
	
	else{
		messageHandler.flashNewMessage("Incorrect Password", "Try again!");   
		getElementObject("passwdTextField").value = "";
	}
}

function dontReset()
{            	 
	FNS.goDown("confMessage");
}
           
function doReset()
{      	      	
	FNS.goDown("confMessage");
	getElementStyleObject("passwordBlock").display = "none";    
	localStorage.clear();
	           
	PersistantValue.refreshGlobals();
	setDateinHR();
	updateNameDisplay();  
	settingsClicked();
  	
   	messageHandler.flashNewMessage("App Reset!","");
    currentLang = "English"
    applyChangeLanguage();
    
    return;
}   

function appReset()
{      	      
	getElementObject("passwdTextField").blur();
	confirmation.alert("Are you sure? This action cannot be undone.");

    PersistantValue.set("nameSet", "no");       
    PV.nameSet = "no";
    PersistantValue.set("dobSet", "no");       
    PV.dobSet = "no";
    PersistantValue.set("dobSet", "no");       
    PV.passwdSet = "no";
    setReqFields();
	return;
}         

function erRefresh()
{
    var url = 'https://www.iercares.com/service/service.php?function=is-session&parameters={"userId":"' + patientID +  '",' + '"superAuthToken":"3qwdEPv1Itxvx6Db6OWFFcAC1gvrR7Oq"' + '}';
        
	var callback = erRefresh_Aux;
    // console.log(url);
	loadFile(url, callback);
    
}   
var data5;

function erRefresh_Aux(status)
{
    var x = parseInt(status);
    data5 = x;
    
    if(x == 0){
        inSession = false;
        getLabelObject("sessionPref").innerHTML = "Not at the ER";
        FNS.applyStyle.unselected("sessionPref");
        FNS.applyStyle.unselected("welcomeListItem");
        FNS.applyStyle.unselected("intakeListItem");
        FNS.applyStyle.unselected("quickLookListItem");
        FNS.applyStyle.unselected("evaluationListItem");
        FNS.applyStyle.unselected("continuingCareListItem");
        FNS.applyStyle.unselected("observationDischargeListItem");
        FNS.applyStyle.unselected("nextStepsListItem");
    }
    else{
        getLabelObject("sessionPref").innerHTML = "Checked in";
        FNS.applyStyle.selected("sessionPref");
        loadERStatus();
        inSession = true;
    }
    
}

function notRefresh()
{
    populateNots();
}
                       

                  
function agreementNo()
{               
    FNS.goDown("AgreementScreen")
}
   
function agreementNo1()
{
	   FNS.goDown("AgreementScreen1")
}

function addButtonEventListeners()                
{                      
	FNS.addButtonElement("agreementNo", agreementNo);
	FNS.addButtonElement("agreementYes", startSession);
                                                        
	FNS.addButtonElement("agreementNo1", agreementNo1);
	FNS.addButtonElement("agreementYes1", endSession);
	          
	FNS.addButtonElement("erRefresh", erRefresh);
	FNS.addButtonElement("notRefresh", notRefresh);
	
	FNS.addButtonElement("confMessageYes", doReset);
	FNS.addButtonElement("confMessageNo", dontReset);
	                
	FNS.addButtonElement("appReset", appReset);
	FNS.addButtonElement("passwdContinue", passwdContinue);
	FNS.addButtonElement("sessionPref", startSessionEULA);
	FNS.addTapElement("donesetPassword_Main", cancelPasswdChange);
	FNS.addTapElement("goToPasswd", continuePasswordChange);

	FNS.addButtonElement("notificationsButton", notificationsClicked);
	FNS.addButtonElement("gamesButton", gamesButtonWasClicked);
	FNS.addButtonElement("followUpButton", placesClicked);
	FNS.addButtonElement("healthInfoButton", healthInfoClicked);
	FNS.addButtonElement("aboutButton", infoButtonWasClicked);
	FNS.addButtonElement("ERNavButton", ERNavButtonWasClicked);
	FNS.addButtonElement("myHealthRecordsButton", hrButtonWasClicked);
	FNS.addButtonElement("settingsButton", settingsClicked);

	FNS.addTapElement("donePlaces", doneWithPlaces);
	FNS.addTapElement("doneNot", doneWithNot);
	FNS.addTapElement("doneAbout", doneWithAbout);
	FNS.addTapElement("doneERNav", doneWithERNav);
	FNS.addTapElement("doneHR", doneHR);
	FNS.addTapElement("doneSettings", doneSettings);
	FNS.addTapElement("doneHealthInfo", doneWithHealthInfo);
	FNS.addTapElement("doneGames", doneGames);
	
	
	FNS.addTapElement("doneERNavDetails", doneERNavDetails);

	FNS.addButtonElement("welcomeListItem", welcomeInfoClicked);
	FNS.addButtonElement("intakeListItem", intakeInfoClicked);
	FNS.addButtonElement("quickLookListItem", quickLookInfoClicked);
	FNS.addButtonElement("evaluationListItem", evaluationInfoClicked);
	FNS.addButtonElement("continuingCareListItem", continuingCareInfoClicked);
	FNS.addButtonElement("observationDischargeListItem", observationDischargeInfoClicked);   
	FNS.addButtonElement("nextStepsListItem", nextStepsInfoClicked);   
	

	FNS.addButtonElement("hrName", hrNameClicked);
	FNS.addButtonElement("namePref", hrNameClicked);
	FNS.addButtonElement("passwdPref", setPasswordClicked_Main);
	
	FNS.addButtonElement("settingsLang", changeLanguage);
	FNS.addButtonElement("settingsPref", changeStartupPref);
	FNS.addButtonElement("locationPref", changeHospital);

	FNS.addButtonElement("hrDiseases", MPWasClicked);
	FNS.addTapElement("doneMP", doneMP);
	FNS.addTapElement("addMP", addCustomInput);


	FNS.addButtonElement("hrMeds", MedsClicked);
	FNS.addTapElement("doneMeds", doneMeds);
    FNS.addTapElement("addMeds", addCustomInput);
	
	FNS.addButtonElement("hrAllergies", MedAlgsClicked);
	FNS.addTapElement("doneMedAlgs", doneMedAlgs);
    FNS.addTapElement("addMedAlgs", addCustomInput);


	
	FNS.addButtonElement("hrSurgs", surgsClicked);
	FNS.addTapElement("doneSurgs", doneSurgs);
    FNS.addTapElement("addSurgs", addCustomInput);

	
	FNS.addButtonElement("hrIns", insClicked);
	FNS.addTapElement("doneIns", doneIns);
	FNS.addTapElement("addIns", addCustomInput);
	
	FNS.addTapElement("doneTextInput", doneWithTextInput);
	FNS.addTapElement("doneTextInput_Aux", cancelTextInput);
    
    FNS.addTapElement("doneCustomInput", doneWithCustomInput);
	FNS.addTapElement("doneCustomInput_Aux", cancelCustomInput);

	FNS.addTapElement("donesetPassword", doneSetPassword);
	FNS.addTapElement("donesetPassword_Aux", cancelSetPassword);

	FNS.addButtonElement("hrDOB", goToDateScreen);
	FNS.addButtonElement("hrDOB1", goToDateScreen);

	FNS.addTapElement("doneDateInput", doneWithDateInput);
	FNS.addTapElement("doneDateInput_Aux", cancelDateInput);
	
	FNS.addTapElement("doneiFrame", hideiFrameDiv);
	
	FNS.addTapElement("mpText", goToTopMPList);
	FNS.addTapElement("iFrameTitle", goToTopiFrameHTML);
	FNS.addTapElement("mhrText", goToTopHRScreen);
	FNS.addTapElement("aboutHeaderText", goToTopAboutScreen);
	FNS.addTapElement("hiText", goToTopHIScreen);

	return;
}

function goToDateScreen()
{
	getElementStyleObject("getDateInput").display = "block";
	getElementObject("theDate").focus();
	return;
}


function changeLanguage()
{     
	var i;
	var tempHI;
	var tempSP;
	var tempMP;
    
    if (currentLang === "English"){
		currentLang = "Spanish";
	}
	else{
		currentLang = "English";
    }
    
	applyChangeLanguage();
	return;
}


function applyChangeLanguage()
{     
	var i;
	var tempHI;
	var tempSP;
	var tempMP;
    
    if (currentLang === "Spanish"){
        $("#aboutHTML").load("docs/about/indexes.html");
        tempHI = allHealthIssuesES;
        tempSP = allSurgsES;
        tempMP = allDiseasesES;
        
        getLabelObject('welcomeListItem').innerHTML = "Bienvenido";
        getLabelObject('intakeListItem').innerHTML = "Llegada";
        getLabelObject('quickLookListItem').innerHTML = "Consulta R&aacute;pida";
        getLabelObject('evaluationListItem').innerHTML = "Admisi&oacute;n ";
        getLabelObject('continuingCareListItem').innerHTML = "Cuidado Continuo";
        getLabelObject('observationDischargeListItem').innerHTML = "Pasos finales";
        getLabelObject('nextStepsListItem').innerHTML = "Los pr&oacute;ximos pasos";
	}
	else{
        $("#aboutHTML").load("docs/about/index.html");
        tempHI = allHealthIssues;
        tempSP = allSurgsEN;
        tempMP = allDiseasesEN;
        
        getLabelObject('welcomeListItem').innerHTML = "Welcome";
        getLabelObject('intakeListItem').innerHTML = "Arrival";
        getLabelObject('quickLookListItem').innerHTML = "Quick Look";
        getLabelObject('evaluationListItem').innerHTML = "Intake";
        getLabelObject('continuingCareListItem').innerHTML = "Continuing Care";
        getLabelObject('observationDischargeListItem').innerHTML = "Final Steps";
        getLabelObject('nextStepsListItem').innerHTML = "Next Steps";
	}
	
	getLabelObject('settingsLang').innerHTML = "LANGUAGE : " + currentLang;  
	         
    
	for (i=0; i < tempSP.length; i++){		
        getLabelObject("surg_" + i.toString()).innerHTML =    tempSP[i]; 		
   	}     

    for (i=0; i < tempMP.length; i++){		
        getLabelObject("medProbs_" + i.toString()).innerHTML =    tempMP[i]; 
		
   	}   
    
    for (i=0; i < tempHI.length; i++){		
        getLabelObject("healthInfo_" + i.toString()).innerHTML =    tempHI[i]; 
   	}   

	for (i=0; i < langID.length; i++){	  
		if (currentLang === "English"){
			getElementObject(langID[i]).innerHTML = langEN[i]; 
		}
		else{
		   	getElementObject(langID[i]).innerHTML = langES[i]; 
		}
	 
	} 
		
	return;
}



function changeHospital()
{
	messageHandler.flashNewMessage("Only Estrella location supported in this version..", "new hospitals coming soon");
	return;
		
	if (currentHospital === "Estrella"){
		currentHospital = "Good Samaritan";
	}
	else if(currentHospital === "Good Samaritan"){
		currentHospital = "Ironwood";
	}
	else if(currentHospital === "Ironwood"){
		currentHospital = "Thunderbird";
	}
	else if(currentHospital === "Thunderbird"){
		currentHospital = "Cardon Children's";
	}
	else if(currentHospital === "Cardon Children's"){
		currentHospital = "Estrella";
	}
	
	getElementById('locationPref').innerHTML = "HOSPITAL : " + currentHospital;
		
	return;
}



function changeStartupPref()
{
	if (currentStartupPref){
		currentStartupPref = false;
	}
	else{
		currentStartupPref = true;
	}
		
	if (currentStartupPref){
		getElementById('settingsPref').innerHTML = "&check; Yes";
	}
	else{
		getElementById('settingsPref').innerHTML = "&times; No";	
	}
		
	return;
}



function arrangeScreens()
{	           
	       
	FNS.goDown("AgreementScreen");
	FNS.goDown("AgreementScreen1");
	
	FNS.goDown("placesScreen");
	FNS.goUp("placesHeader");
	
	FNS.goDown("notificationsScreen");
	FNS.goUp("headerNot");
	
	FNS.goDown("aboutScreen");
	FNS.goUp("headerAbout");
	
	FNS.goDown("ERNavScreen");
	FNS.goUp("headerERNav");
	
	FNS.goDown("healthRecordsScreen");
	FNS.goUp("headerHR");
	
	FNS.goDown("settingsScreen");
	FNS.goUp("headerSettings");
	
	FNS.goDown("refreshPopup");

	getElementStyleObject("getTextInput").display = "none";
	getElementStyleObject("addCustomInput").display = "none";
	getElementStyleObject("getDateInput").display = "none";
	getElementStyleObject("setPassword").display = "none";
	getElementStyleObject("passwdWrapper").display = "none";

	FNS.goUp('healthInfoHeader');
	FNS.goDown('healthInfoScreen');
	FNS.goDown('iFrameScreen');
	
	FNS.goUp('headerGames');
	fadeOut("gamesScreen");
	
	FNS.goUp("headerMP");
	FNS.goUp("headerMeds");
	FNS.goUp("headerMedAlgs");
	FNS.goUp("headerSurgs");
	FNS.goUp("headerIns");	
	
	FNS.goDown("getMedProbInput");
	FNS.goDown("getMedicationInput");
	FNS.goDown("getMedAlgsInput");
	FNS.goDown("getSurgsInput");
	FNS.goDown("getInsInput");
    
    FNS.goCenter("mainMenuScreen");

}

var aboutHTMLiScroll;
var iFrameHTMLiScroll;
var ERNaviScroll;
var notificationsiScroll;
var hriScroll;
var mpiScroll;
var medsiScroll;
var medAlgsiScroll;
var surgiScroll;
var insiScroll;
var fuciScroll;
var healthInfoiScroll;
var settingsiScroll;

function startUp()
{	      
        
	FNS.applyStyle.selected("locationPref");
	
	FNS.identifyDevice();
	FNS.initialize(); 
	
	messageHandler.initialize();
	confirmation.initialize();
	listFilter.initialize();
	
	initializeButtonClicks();
	addButtonEventListeners();	
	
		                  
	populateLangList();
	
	getElementObject("theDate").value = PV.dob;

	
	setDateinHR();
	updateNameDisplay();	

	aboutHTMLiScroll =  new iScroll('aboutWrapper', {zoom:false});
	iFrameHTMLiScroll =  new iScroll('iFrameWrapper', {zoom:true});
	ERNaviScroll =  new iScroll('ERNavWrapper', {zoom:false});
	notificationsiScroll =  new iScroll('notWrapper', {zoom:false});
	hriScroll =  new iScroll('healthRecordsWrapper', {zoom:false});
	settingsiScroll =  new iScroll('settingsWrapper', {zoom:false});

	fuciScroll =  new iScroll('diseaseContainerWrapper', {zoom:false});
	healthInfoiScroll = new iScroll('healthInfoListWrapper', {zoom:false})
	mpiScroll =  new iScroll('medProbListWrapper', {zoom:false});
	medsiScroll =  new iScroll('medicationListWrapper', {zoom:false});
	medAlgsiScroll =  new iScroll('medAlgListWrapper', {zoom:false});
	surgiScroll =  new iScroll('surgListWrapper', {zoom:false});
	insiScroll =  new iScroll('insListWrapper', {zoom:false});

	setReqFields(false);
	     
	if(PV.passwdSet.toLowerCase() === "no" ){
		getElementObject("passwdTextField").blur();
		getElementStyleObject("passwordBlock").display = "none"; 
		setTimeout('settingsClicked()', 2300);  
	}           
	else{  
        setTimeout('getElementStyleObject("passwordBlock").display = "block"', 2000);
	} 
	
           	
	setTimeout("applyChangeLanguage();", 1000);
    
    populateHealthInfoList();
	
}
               

function setReqFields(t)
{
	var f1 = false;
	var f2 = false;
	var f3 = false;
	var retVal = true;
    
	if(PV.passwdSet.toLowerCase() === "no" ){
		f1 = true;
		FNS.applyStyle.reqisNotSet("passwdPref");
	}     
	
	else{
		FNS.applyStyle.reqisSet("passwdPref");
	}            
	
	if(PV.nameSet.toLowerCase() === "no" ){
		f2 = true;
		FNS.applyStyle.reqisNotSet("namePref");
	}  
	
	else{
		FNS.applyStyle.reqisSet("namePref");
	}
	
	if(PV.dobSet.toLowerCase() === "no" ){
		f3 = true;
		FNS.applyStyle.reqisNotSet("hrDOB1");
	}  
	
	else{
		FNS.applyStyle.reqisSet("hrDOB1");
	}
	
	var outS = "Set "

	if(f1){
		outS = outS + "password";
	}
	if(f2){
		outS = outS + ", full name";		
	}
	if(f3){
		outS = outS + ", date-of-birth ";
	}
	
	outS = outS + " to continue..."
	
	if(f1 || f2 || f3){
		messageHandler.flashNewMessage(outS, ""); 
        retVal = false;
		if(t){
			settingsClicked();  
		}
	}
	
	if(!f1){
		getLabelObject("passwdPref").innerHTML = "Change password";
	}	
    
    return retVal;
}   
                
function focusPasswordField()
{
    getElementObject("passwdTextField").focus();   
}                                               

function notificationsClicked(){
	notificationsiScroll.refresh();	
	notificationsiScroll.scrollTo(0,0,0);	
	
	FNS.goUp("headerMain");
	FNS.goCenter("headerNot");
	
	FNS.goDown("mainMenuScreen");
	FNS.goCenter("notificationsScreen");
	
	populateNots(); 
    
    kronSemaphore = true;
		
	return;
}
    
var isAERLoaded = false;


function placesClicked()
{	    	
	FNS.goUp("headerMain");
	FNS.goCenter("placesHeader");
	
	FNS.goDown("mainMenuScreen");
	FNS.goCenter("placesScreen");
	 
	if (!isAERLoaded){ 
		isAERLoaded = true;  
		populateAfterERList();
		messageHandler.msgSemaphore = true;  
		listFilter.azClicked();
	}
	

	fuciScroll.refresh();
	fuciScroll.scrollTo(0,0,0);	
	     
	listFilter.setCurrentList("afterER");
	listFilter.show(); 	  	
	
}
     
var isHealthInfoLoaded = true;

function healthInfoClicked()
{ 
    FNS.goUp("headerMain");
	FNS.goCenter("healthInfoHeader");
	
	FNS.goDown("mainMenuScreen");
	FNS.goCenter("healthInfoScreen");
    
    activityIndicator.show();
    setTimeout('activityIndicator.hide();', 700);

	if (!isHealthInfoLoaded){
		isHealthInfoLoaded = true;  
		populateHealthInfoList();
	}  
	                            

	healthInfoiScroll.refresh();
	healthInfoiScroll.scrollTo(0,0,0); 
	             
	listFilter.setCurrentList("healthInfo");
	listFilter.show();
                 

	return;
}


function doneWithPlaces()
{                   
	FNS.goCenter("headerMain");
	FNS.goUp("placesHeader");
	
	FNS.goCenter("mainMenuScreen");
	FNS.goDown("placesScreen");
	
	hideiFrameDiv(); 
	listFilter.hide();   
	
}

function doneWithHealthInfo()
{                 	
	
	FNS.goCenter("headerMain");
	FNS.goUp("healthInfoHeader");
	
	FNS.goCenter("mainMenuScreen");
	FNS.goDown("healthInfoScreen");
	
	hideiFrameDiv();
	listFilter.hide();
	
}




var isERNavLoaded = true;

function ERNavButtonWasClicked(){   
	
	ERNaviScroll.scrollTo(0,0,0);
	
	FNS.goUp("headerMain");
	FNS.goCenter("headerERNav");
	
	FNS.goDown("mainMenuScreen");
	FNS.goCenter("ERNavScreen");
	                     
	refreshPatientID();

	setAuthToken();
    erRefresh();
}


function doneWithERNav(){
	
	FNS.goCenter("headerMain");
	FNS.goUp("headerERNav");
	
	FNS.goCenter("mainMenuScreen");
	FNS.goDown("ERNavScreen");

	doneERNavDetails();
	
	getElementObject("theVideo").setAttribute("src","");	
	
}





function doneWithNot(){
    activityIndicator.show();
    setTimeout('activityIndicator.hide()', 1000);

    
    var count = totalNots - (countCommas(PV.myNots) - 1);

    if(count > 0){
        getElementObject('systemNotN').innerHTML = count.toString();
        getElementStyleObject('systemNotN').display = "block";
    }
    else{
        getElementObject('systemNotN').innerHTML = "";
        getElementStyleObject('systemNotN').display = "none";
    }  
    
	FNS.goCenter("headerMain");
	FNS.goUp("headerNot");
	
	FNS.goCenter("mainMenuScreen");
	FNS.goDown("notificationsScreen");
    
    kronSemaphore = false;
  
}


function doneWithAbout(){
	FNS.goCenter("headerMain");
	FNS.goUp("headerAbout");
	
	FNS.goCenter("mainMenuScreen");
	FNS.goDown("aboutScreen");
	
}


function settingsClicked()
{	
    activityIndicator.show();
    setTimeout('activityIndicator.hide();', 800);
    
	FNS.goUp("headerMain");
	FNS.goCenter("headerSettings");
	
	FNS.goDown("mainMenuScreen");
	FNS.goCenter("settingsScreen");  
	
}


function doneSettings()
{	   
    activityIndicator.show();
    setTimeout('activityIndicator.hide();', 800);
   
    if(setReqFields(true)){
        FNS.goCenter("headerMain");
        FNS.goUp("headerSettings");
	
        FNS.goCenter("mainMenuScreen"); 
        FNS.goDown("settingsScreen");
    }        
         
    runKron();
    
	return;  
}

var isKron = false;
var kronSemaphore = false;
function runKron()
{

    if(!isKron){
        setInterval("runKron()", 60000);  
        isKron = true;
    }
    
    refreshPatientID();	  
	setAuthToken(); 
    erRefresh();
    if (kronSemaphore){
        return;
    }
    notRefresh();
}

function setAuthToken()
{
    return;
	var temp = hex_md5(patientID + PV.passwd)
    PersistantValue.set("authToken", temp);       
    PV.authToken = temp;
}
       
var washrClicked = false;
function hrButtonWasClicked()
{	  
	    	
	FNS.goUp("headerMain");
	FNS.goCenter("headerHR");
	
	FNS.goDown("mainMenuScreen");
	FNS.goCenter("healthRecordsScreen");
	   
	if (!washrClicked){   
		activityIndicator.show();
	 	setTimeout('activityIndicator.hide();', 1000);
	
		washrClicked = true;   
		
		isInsLoaded = true;  
		populateIns();   
		
		isSurgsLoaded = true;  
		populateSurg(); 
		
		isMPLoaded = true;  
		populateMedProbs();
	}

	updateMeds();
	updateMedAlgs(); 
	
	hriScroll.scrollTo(0,0,0); 	 
		
}


function doneHR()
{	     

	FNS.goCenter("headerMain");
	FNS.goUp("headerHR");
	
	FNS.goCenter("mainMenuScreen");
	FNS.goDown("healthRecordsScreen");	
 
}


     
var isMPLoaded = false;

function MPWasClicked()
{      
	FNS.goUp("headerHR");
	FNS.goCenter("headerMP");
	
	FNS.goDown("healthRecordsScreen");
	FNS.goCenter("getMedProbInput");
	
	if (!isMPLoaded){ 
		activityIndicator.show();
	 	setTimeout('activityIndicator.hide();', 500);
		isMPLoaded = true;  
		populateMedProbs(); 
		listFilter.azClicked();
	}
	
	mpiScroll.scrollTo(0,0,0);   
	
	listFilter.setCurrentList("medProbs");
	listFilter.show();  
	
}


function doneMP()
{             
	
	FNS.goCenter("headerHR");
	FNS.goUp("headerMP");
	
	FNS.goCenter("healthRecordsScreen");
	FNS.goDown("getMedProbInput");	

	updateMPs();
	listFilter.hide();	 
   
}

       
var isMedsLoaded = false;

function MedsClicked()
{            
	
	FNS.goUp("headerHR");
	FNS.goCenter("headerMeds");
	
	FNS.goDown("healthRecordsScreen");
	FNS.goCenter("getMedicationInput");
	
	if (!isMedsLoaded){  
		activityIndicator.show();
	 	setTimeout('activityIndicator.hide();', 500);
		isMedsLoaded = true;  
		populateMeds();  
		listFilter.azClicked();
	}
	
	medsiScroll.scrollTo(0,0,0); 
	
	listFilter.setCurrentList("meds");
	listFilter.show(); 
		
}


function doneMeds()
{
	FNS.goCenter("headerHR");
	FNS.goUp("headerMeds");
	
	FNS.goCenter("healthRecordsScreen");
	FNS.goDown("getMedicationInput");	

	updateMeds();	
   	listFilter.hide(); 


}
        
var isMedAlgsLoaded = false;

function MedAlgsClicked()
{              
	FNS.goUp("headerHR");
	FNS.goCenter("headerMedAlgs");
	
	FNS.goDown("healthRecordsScreen");
	FNS.goCenter("getMedAlgsInput");
	
	if (!isMedAlgsLoaded){   
		activityIndicator.show();
	 	setTimeout('activityIndicator.hide();', 500);
	
		isMedAlgsLoaded = true;  
		populateMedAlgs();
		listFilter.azClicked();
	}
	
	
	medAlgsiScroll.scrollTo(0,0,0);
	
	listFilter.setCurrentList("medAlgs");
	listFilter.show();  
		
}


function doneMedAlgs()
{
	FNS.goCenter("headerHR");
	FNS.goUp("headerMedAlgs");
	
	FNS.goCenter("healthRecordsScreen");
	FNS.goDown("getMedAlgsInput");	

	updateMedAlgs();	
	listFilter.hide();  
	
	hriScroll.refresh();
}

        
var isSurgsLoaded = false;

function surgsClicked()
{    
	FNS.goUp("headerHR");
	FNS.goCenter("headerSurgs");
	
	FNS.goDown("healthRecordsScreen");
	FNS.goCenter("getSurgsInput");
	   
	if (!isSurgsLoaded){ 
		isSurgsLoaded = true;  
		populateSurg(); 
		listFilter.azClicked();
	}    
	
	listFilter.setCurrentList("surgs");
	
	surgiScroll.scrollTo(0,0,0);
	
	listFilter.setCurrentList("surgs");
	listFilter.show(); 
	
	
}


function doneSurgs()
{
	FNS.goCenter("headerHR");
	FNS.goUp("headerSurgs");
	
	FNS.goCenter("healthRecordsScreen");
	FNS.goDown("getSurgsInput");	

	updateSurgs();	  
  	listFilter.hide(); 

}

var isInsLoaded = false;

function insClicked()
{	     
	FNS.goUp("headerHR");
	FNS.goCenter("headerIns");
	
	FNS.goDown("healthRecordsScreen");
	FNS.goCenter("getInsInput");
	  
	if (!isInsLoaded){ 
		isInsLoaded = true;  
		populateIns(); 
		listFilter.azClicked();
	}
	
	insiScroll.scrollTo(0,0,0); 
	
	listFilter.setCurrentList("ins");
	listFilter.show();  	
}


function doneIns()
{
	FNS.goCenter("headerHR");
	FNS.goUp("headerIns");
	
	FNS.goCenter("healthRecordsScreen");
	FNS.goDown("getInsInput");	

	updateIns(); 
	listFilter.hide();          

}





var isGameLoaded = false;

function gamesButtonWasClicked()
{	
    if(!FNS.device.touchSupport){
        return;
    }
    
    if(FNS.device.android){
        messageHandler.flashNewMessage("Coming soon", "currently only avialable on iOS");
        return;
    }

    if(!isGameLoaded){
        isGameLoaded = true;
        loadChildBrowser(true, "/games/index.html");
    }
    else{
        loadChildBrowser(false, "");
    }
    return;
}


function doneGames()
{
	fadeOut("gamesScreen");
}


function infoButtonWasClicked()
{
	FNS.goUp("headerMain");
	FNS.goCenter("headerAbout");
	
	FNS.goDown("mainMenuScreen");
	FNS.goCenter("aboutScreen");
    
    aboutHTMLiScroll.refresh();	
    aboutHTMLiScroll.scrollTo(0,0,0);	

}


function welcomeInfoClicked()
{
	currentSelectedERScreen = "welcome";
	showERNavDetails();
}
       
function nextStepsInfoClicked()
{
	currentSelectedERScreen = "nextSteps";
	showERNavDetails();
}



function intakeInfoClicked()
{
	currentSelectedERScreen = "arrival";
	showERNavDetails();	
}


function quickLookInfoClicked()
{
	currentSelectedERScreen = "quickLook";
	showERNavDetails();	
}


function evaluationInfoClicked()
{
	currentSelectedERScreen = "intake";
	showERNavDetails();	
}


function continuingCareInfoClicked()
{
	currentSelectedERScreen = "continuingCare";
	showERNavDetails();	
}


function observationDischargeInfoClicked()
{
	currentSelectedERScreen = "admitOrDischarge";	
	showERNavDetails();
	
}

function getERVidTitles(){
    var retS;
    if (currentLang === "Spanish"){
        if (currentSelectedERScreen === 'welcome'){
            retS = "Bienvenido";
        }
        if (currentSelectedERScreen === 'nextSteps'){
            retS = "Los pr&oacute;ximos pasos";
        }
        if (currentSelectedERScreen === 'arrival'){
            retS = "Llegada";
        }
        if (currentSelectedERScreen === 'quickLook'){
            retS = "Consulta R&aacute;pida";
        }
        if (currentSelectedERScreen === 'intake'){
            retS = "Admisi&oacute;n";
        }
        if (currentSelectedERScreen === 'continuingCare'){
            retS = "Cuidado Continuo";
        }
        if (currentSelectedERScreen === 'admitOrDischarge'){
            retS = "Pasos finales";  
        }
        
    }
    else{
        if (currentSelectedERScreen === 'welcome'){
            retS = "Welcome";
        }
        if (currentSelectedERScreen === 'nextSteps'){
            retS = "Next Steps";
        }
        if (currentSelectedERScreen === 'arrival'){
            retS = "Arrival";
        }
        if (currentSelectedERScreen === 'quickLook'){
            retS = "Quick Look";
        }
        if (currentSelectedERScreen === 'intake'){
            retS = "Intake";
        }
        if (currentSelectedERScreen === 'continuingCare'){
            retS = "Continuing Care";
        }
        if (currentSelectedERScreen === 'admitOrDischarge'){
            retS = "Final Steps";
        }
        
    }
    
    return retS;
    
    
}




function getERVidDetails(){
    var retS;
    if (currentLang === "English"){
        if (currentSelectedERScreen === 'welcome'){
            retS = "Welcome to the Emergency Department.  We have created the split-flow system to decrease the amount of time patients wait to see a medical provider.  We will guide you to different areas of the ER, so that we have more space available to see patients and you spend less time waiting and can start feeling better sooner.  We encourage you to look through the ER Navigator and review the steps you will take today on your visit: Arrival, Quick-Look, Intake, Continuing Care and Final Steps";
        }
        if (currentSelectedERScreen === 'nextSteps'){
            retS = "If you have questions about your discharge instructions please ask            your nurse or provider before you leave.  It is important that you follow these instructions.  If you have further questions, call the ER at (623) 327-4040.  A follow-up appointment with your doctor, a specialist or the Banner Rapid Care Clinic may have been scheduled. It is important to follow through with these appointments.  If you are uninsured, try browsing through the 'After ER' portion of this app to look for  services offered at low cost";
        }
        if (currentSelectedERScreen === 'arrival'){
            retS = "A receptionist gathers basic information.";
        }
        if (currentSelectedERScreen === 'quickLook'){
            retS = "A quick look nurse gathers initial medical information and determines the area in which you will be seen.";
        }
        if (currentSelectedERScreen === 'intake'){
            retS = "A provider performs a medical exam. Your provider may order further tests and will direct your care.";
        }
        if (currentSelectedERScreen === 'continuingCare'){
            retS = "Depending on your plan of care you may have an IV placed, blood drawn or medications given here. Your health care team will be continually monitoring you for any changes in your condition as well as your test results. Upon completion, we will review and discuss your results with you.";
        }
        if (currentSelectedERScreen === 'admitOrDischarge'){
            retS = "Your medical provider will review your  results with you and discuss your diagnosis and the next step in your care.";  
        }
        
    }
    else{
        if (currentSelectedERScreen === 'welcome'){
            retS = "Bienvenido a la sala de emergencias. Hemos creado el sistema split-flow system para reducir la cantidad de tiempo que los pacientes esperan a ver a un m&eacute;dico. Le guiaremos a diferentes &aacute;reas de la sala de emergencia, para hacer m&aacute;s espacio disponible para los pacientes y dedicar menos tiempo de espera para usted y a la vez se comience sentirse mejor m&aacute;s pronto. Le invitamos a mirar a trav&eacute;s del Navegador de ER a revisar los pasos que tomar&aacute; hoy en su visita: Llegada, consulta r&aacute;pida, admisi&oacute;n, atenci&oacute;n continua y los pr&oacute;ximos pasos";
        }
        if (currentSelectedERScreen === 'nextSteps'){
            retS = "Si usted tiene preguntas acerca de las instrucciones de descarga por favor pregunte a su enfermera o médico antes de salir. Es importante que siga estas instrucciones. Si usted tiene preguntas adicionales, llame a la sala de emergencias al (623) 327-4040. Una cita de seguimiento con su médico, un especialista de la Clínica o Banner atención rápida puede haber sido programado. lo Es importante seguir adelante con estos nombramientos. Si usted es seguro, intente navegar a través de la 'Después de ER' parte de esta aplicación para buscar los servicios que se ofrecen a bajo costo";
        }
        if (currentSelectedERScreen === 'arrival'){
            retS = " Una recepcionista re&uacute;ne informaci&oacute;n b&aacute;sica";
        }
        if (currentSelectedERScreen === 'quickLook'){
            retS = "Una enfermera re&uacute;ne informaci&oacute;n m&eacute;dica necesaria y determina la area en la que se ver&aacute;";
        }
        if (currentSelectedERScreen === 'intake'){
            retS = "Un m&eacute;dico/proveedor realiza un examen m&eacute;dico. Su m&eacute;dico/proveedor le puede ordenar pruebas adicionales y se encargar&aacute; de su cuidado";
        }
        if (currentSelectedERScreen === 'continuingCare'){
            retS = "Dependiendo de su plan de cuidado es posible que se le administre suero, se le tome  una muestra de sangre o darle medicamentos en este momento. Su equipo de atenci&oacute;n m&eacute;dica ser&aacute; responsable en observar cualquier cambio en su condici&oacute;n, revisar sus resultados de pruebas y al terminar, vamos a revisar y discutir los resultados con usted";
        }
        if (currentSelectedERScreen === 'admitOrDischarge'){
            retS = "El m&eacute;dico va a revisar los resultados con usted y discutir su diagn&oacute;stico y los  siguiente pasos en de su cuidado";
        }
        
    }
    
    return retS;
}



function showERNavDetails()
{
	fadeOut('ERNavWrapper');
	fadeIn('ERNavDetails');
	getElementObject("theVideo").setAttribute("src","docs/steps/" + currentSelectedERScreen +".m4v");
                                      

	getElementById('videoDetails').innerHTML = getERVidDetails();
    getElementById('ERDetailsTitle').innerHTML = getERVidTitles();
    getElementStyleObject("erRefresh").display = "none";


}

function doneERNavDetails(){

	currentSelectedERScreen = "";
	fadeOut('ERNavDetails');
	fadeIn('ERNavWrapper');
    getElementStyleObject("erRefresh").display = "block";

}



function adjustFontforiPhonePhonegap()
{
	
}


function hrNameClicked()
{
	//$("#getTextInput").fadeIn(100);
	getElementStyleObject("getTextInput").display = "block";
	
	getElementById('fnameTextField').value = PV.myFirstName;
	getElementById('lnameTextField').value = PV.myLastName;
		
	getElementObject("fnameTextField").focus();	

	//setTimeout('handleInputFocus();', 1000);	
	return;
}


function updateNameDisplay()
{	
	var temp1 = getLabelObject("hrName"); 
	var temp2 = getLabelObject("namePref");
	
	
	if (PV.myFirstName === "" && PV.myLastName === "")
	{
		temp1.innerHTML = "Add your name";
		temp2.innerHTML = "Add your name"; 	
	}
	
	else{
		temp1.innerHTML = PV.myLastName + ", " + PV.myFirstName;
		temp2.innerHTML = PV.myLastName + ", " + PV.myFirstName;
	}

	return;
}

function doneWithTextInput()
{
	getElementStyleObject("getTextInput").display = "none";

	PV.myFirstName = getElementObject('fnameTextField').value;
	PV.myLastName = getElementObject('lnameTextField').value;

	updateNameDisplay();


	getElementObject("fnameTextField").blur();
	getElementObject("lnameTextField").blur();
	
	PersistantValue.set("myFirstName", PV.myFirstName);
	PersistantValue.set("myLastName", PV.myLastName);
	
	PersistantValue.set("nameSet", "yes");       
    PV.nameSet = "yes";
    
    setReqFields(false);

	
	return;
}

function cancelTextInput()
{
	getElementStyleObject("getTextInput").display = "none";

	getElementObject("fnameTextField").blur();
	getElementObject("lnameTextField").blur();
	
	return;
}

function addCustomInput()
{
    var cl = listFilter.currentList;
    getElementStyleObject("addCustomInput").display = "block";
    listFilter.hide();
    
    if(cl === "medProbs"){
        getElementObject("customTextLabel").innerHTML = "medical problems";
        getElementObject("customTextField").value = PV.customDiseases;
    }
    if(cl === "meds"){
        getElementObject("customTextLabel").innerHTML = "medications";
        getElementObject("customTextField").value = PV.customMeds;
    }
    if(cl === "medAlgs"){
        getElementObject("customTextLabel").innerHTML = "medication allergies";
        getElementObject("customTextField").value = PV.customMedAlgs;
    }
    if(cl === "surgs"){
        getElementObject("customTextLabel").innerHTML = "surgery/procedures";
        getElementObject("customTextField").value = PV.customSurgs;
    }
    if(cl === "ins"){
        getElementObject("customTextLabel").innerHTML = "insurance";
        getElementObject("customTextField").value = PV.customIns;
    }
    
    getElementObject("addCustomInput").focus();

    return;
}

function doneWithCustomInput()
{
	cancelCustomInput();
    var cl = listFilter.currentList;
	var temp;
    
    if(cl === "medProbs"){
        temp = getElementObject("customTextField").value;
        PV.customDiseases = temp;
        PersistantValue.set("customDiseases", temp);
    }
    if(cl === "meds"){
        temp = getElementObject("customTextField").value;
        PV.customMeds = temp;
        PersistantValue.set("customMeds", temp);
    }
    if(cl === "medAlgs"){
        temp = getElementObject("customTextField").value;
        PV.customMedAlgs = temp;
        PersistantValue.set("customMedAlgs", temp);
    }
    if(cl === "surgs"){
        temp = getElementObject("customTextField").value;
        PV.customSurgs = temp;
        PersistantValue.set("customSurgs", temp);
    }
    if(cl === "ins"){
        temp = getElementObject("customTextField").value;
        PV.customIns = temp;
        PersistantValue.set("customIns", temp);
    }
    
	return;
}

function cancelCustomInput()
{
	getElementObject("addCustomInput").blur();
	getElementStyleObject("addCustomInput").display = "none";
    
    listFilter.show();
	return;
}


function doneWithDateInput()
{
	getElementStyleObject("getDateInput").display = "none"
	getElementObject("theDate").blur();
	
	PersistantValue.set("dobSet", "yes");       
    PV.dobSet = "yes";

	setDateinHR();
    setReqFields(false);
}

function cancelDateInput()
{
	getElementStyleObject("getDateInput").display = "none"
	getElementObject("theDate").blur();
}

function getMonthString(month){
	var ret = "";
	
	switch(month)
	{
	case 1:
		ret = "January";
	  	break;
	case 2:
		ret = "February";
	  	break;
	case 3:
		ret = "March";
		break;
	case 4:
		ret = "April";
	  	break;
	case 5:
		ret = "May";
	 	break;
	case 6:
		ret = "June";
	  	break;
	case 7:
		ret = "July";
	 	break;
	case 8:
		ret = "August";
  		break;
	case 9:
		ret = "September";
	  	break;
	case 10:
		ret = "October";
	  	break;
	case 11:
		ret = "November";
	  	break;
	case 12:
		ret = "December";
	  	break;
	
	default:
		ret = "** error **";
	}
	
	return ret;
}

function getDateSt(date)
{
	var temp = CSVToArray(date, "-")[0];
	var outS;
	outS = getMonthString(parseInt(temp[1])) + " " + parseInt(temp[2]).toString() + ", " + temp[0];
	return outS;
}

function setDateinHR()
{
	var outS = getElementObject("theDate").value;	
	if (outS === ""){
		getLabelObject('hrDOB').innerHTML = "Add date-of-birth";		
		getLabelObject('hrDOB1').innerHTML = "Add date-of-birth";		
	}
	else{
		getLabelObject('hrDOB').innerHTML = getDateSt(outS);
		getLabelObject('hrDOB1').innerHTML = getDateSt(outS);
	}
	
	PersistantValue.set("dob", outS);
	PV.dob = outS;
                     

	return;
}
      
                   
function populateMedProbs()
{
	CSVToArray(loadFile("./docs/MedProbs/medProb.txt", populateMedProbs_Aux));
}

function populateMedProbs_Aux(allData)
{
	var allRows;
	var i;
	allRows = CSVToArray(allData, ":");
	
	for (i=0; i < allRows.length; i++){
		allDiseasesEN[i] = allRows[i][0];
		allDiseasesES[i] = allRows[i][1];  
	}
	               
	var allDiseases;
	          
	if (currentLang === "English"){
		allDiseases = allDiseasesEN;
	}
	else{
		allDiseases = allDiseasesES;
	}
	
	var Parent = document.getElementById("medProbList");

	for (i=0; i < allDiseases.length; i++){		
		var NewLI = document.createElement("LI");
		NewLI.innerHTML = 	"<p>" + allDiseases[i] + "</p>";
		
		NewLI.id = "medProbs_" + i;
		
		Parent.appendChild(NewLI); 
		
		if (PV.myDiseases.indexOf(","+i+",") >= 0){ 
			FNS.applyStyle.selected("medProbs_" + i);
		}
	}
	
	for (i=0; i < allDiseases.length; i++){
		FNS.addButtonElement("medProbs_" + i, aMedProblemClicked);
	}
	
	mpiScroll.refresh(); 
	updateMPs();   
	
}

function populateMeds()
{
	var Parent = document.getElementById("medicationList");
	var count = 0;
    var i;

	for (i=0; i < medicationKeys.length; i++){
		for (var j=0; j < medication[medicationKeys[i]].length; j++){		
			var NewLI = document.createElement("LI");
			NewLI.innerHTML = 	"<p>" + medication[medicationKeys[i]][j] + "(" + medicationKeys[i] + ")</p>";
		
			NewLI.id = "meds_" + count;
			Parent.appendChild(NewLI);
			
			if (PV.myMeds.indexOf(","+count+",") >= 0){
				FNS.applyStyle.selected("meds_" + count);
			}
			
			count = count + 1;
		}
	}
	
	for (i=0; i < count; i++){
		FNS.addButtonElement("meds_" + i, aMedClicked);
	}
	
	medsiScroll.refresh(); 
	
}

function populateMedAlgs()
{
	var Parent = document.getElementById("medAlgsList");
	var count = 0;
    var i;

	for (i=0; i < medicationKeys.length; i++){
		for (var j=0; j < medication[medicationKeys[i]].length; j++){		
			var NewLI = document.createElement("LI");
			NewLI.innerHTML = 	"<p>" + medication[medicationKeys[i]][j] + "(" + medicationKeys[i] + ")</p>";
				
			NewLI.id = "medAlgs_" + count;
		
			Parent.appendChild(NewLI); 
			
			if (PV.myAllergies.indexOf(","+count+",") >= 0){
				FNS.applyStyle.selected("medAlgs_" + count);
			}
			
			count = count + 1;
		}
	}
	
	for (i=0; i < count; i++){
		FNS.addButtonElement("medAlgs_" + i, aMedAlgClicked);
	}
	
	medsiScroll.refresh(); 
	
}

    
function populateSurg()
{
	CSVToArray(loadFile("./docs/Surgeries/surg.txt", populateSurg_Aux));
}

function populateSurg_Aux(allData)
{
	var allRows;
	var i;
	allRows = CSVToArray(allData, ":");
	
	for (i=0; i < allRows.length; i++){
		allSurgsEN[i] = allRows[i][0];
		allSurgsES[i] = allRows[i][1];  
	}
	
	var Parent = document.getElementById("surgsList");
	var count = 0;
	var temp;
	var i;
	var allSurgs;
	    
	if (currentLang === "English"){
		allSurgs = allSurgsEN;
	}
	else{
		allSurgs = allSurgsES;
	}
	
	for (i=0; i < allSurgs.length; i++){
			temp = "<h1> ";
		
			var NewLI = document.createElement("LI");
                                                    
			NewLI.innerHTML = 	"<p>" + allSurgs[i] + "</p>";
		
			NewLI.id = "surg_" + i;
			NewLI.className = "surgsLI";
		
			Parent.appendChild(NewLI);    
			if (PV.mySurgeries.indexOf(","+i+",") >= 0){
				FNS.applyStyle.selected("surg_" + i);
			}
			
			count = count + 1;
	}
	
	for (i=0; i < allSurgs.length; i++){
		FNS.addButtonElement("surg_" + i, aSurgClicked);
	}
	
	surgiScroll.refresh(); 
	updateSurgs(); 	
}

   
function populateLangList()
{
	CSVToArray(loadFile("./docs/language/enes.txt", populateLangList_Aux));
}

function populateLangList_Aux(allData)
{
	var allRows;
	var i;
	allRows = CSVToArray(allData, ":");
	
	for (i=0; i < allRows.length; i++){
		langID[i] = allRows[i][0];
		langEN[i] = tokenize(allRows[i][1]);  
		langES[i] = allRows[i][2];
	}

}


function populateHealthInfoList()
{
	CSVToArray(loadFile("./docs/HEALTHINFO/mapping.txt", populateHealthInfoList_Aux));
}

function populateHealthInfoList_Aux(allData)
{                    
	var allRows;
	var i;
	allRows = CSVToArray(allData, ":");
	
	for (i=0; i < allRows.length; i++){
		allHealthIssues[i] = allRows[i][0];
		allHealthIssuesDirDict[i] = tokenize(allRows[i][1]);  
		allHealthIssuesES[i] = allRows[i][2];
	}

	var Parent = document.getElementById("healthInfoList");

    var temp;
    if(currentLang === "English"){
        temp = allHealthIssues;
    }
    else{
        temp = allHealthIssuesES;
    }
    
	for (i=0; i < temp.length; i++){
		var NewLI = document.createElement("LI");
		NewLI.innerHTML = 	"<p> "+ temp[i] + "</p>";
		NewLI.id = "healthInfo_" + i;
		Parent.appendChild(NewLI);
	}
	
	
	for (i=0; i < allRows.length; i++){
		FNS.addButtonElement("healthInfo_" + i, aHealthInfoClicked);
	}                         
	

	healthInfoiScroll.refresh();
	healthInfoiScroll.scrollTo(0,0,0);
    messageHandler.msgSemaphore = true;
    //listFilter.azClicked();
}


function populateAfterERList()
{
	CSVToArray(loadFile("./docs/Places/mapping.txt", populateAfterERList_Aux));
}

function populateAfterERList_Aux(allData)
{             
	
	var allRows;
	var i;
	allRows = CSVToArray(allData, ":");
	
	for (i=0; i < allRows.length; i++){
		allAfterER[allAfterER.length] = allRows[i][0];
		allAfterERDirDict[i] = tokenize(allRows[i][1]);
		allAfterERTags[i] = allRows[i][2]; 
	}

	var Parent = document.getElementById("pharmacyList");

	for (i=0; i < allAfterER.length; i++){		
		var NewLI = document.createElement("LI");

		NewLI.innerHTML = 	"<p> "+ allAfterER[i] + "</p> <h2> "+ allAfterERTags[i] + "</h2>";
		
		NewLI.id = "afterER_" + i;
				
		Parent.appendChild(NewLI);
	}
	
	
	for (i=0; i < allRows.length; i++){
		FNS.addButtonElement("afterER_" + i, aAfterERClicked);
	}

	fuciScroll.refresh();
	fuciScroll.scrollTo(0,0,0); 
    messageHandler.msgSemaphore = true;
    listFilter.azClicked();
	                       
}


function populateIns()
{
	CSVToArray(loadFile("./docs/Insurance/mapping.txt", populateIns_Aux));
}

function populateIns_Aux(allData)
{
	var allRows;
	var i;
	allRows = CSVToArray(allData, ":");
	
	for (i=0; i < allRows.length; i++){
		allInsurance[i] = allRows[i];
	}

	var Parent = document.getElementById("insList");

	for (i=0; i < allInsurance.length; i++){
		
		var NewLI = document.createElement("LI");  
		    		

		NewLI.innerHTML = 	"<p>" + allInsurance[i][0] + "</p>";
		
		NewLI.id = "ins_" + i;
				
		Parent.appendChild(NewLI);  
		
		if (PV.myInsurances.indexOf(","+i+",") >= 0){
			FNS.applyStyle.selected("ins_" + i);
   		}
	}
	
	
	for (i=0; i < allRows.length; i++){
		FNS.addButtonElement("ins_" + i, aInsClicked);
	}

	insiScroll.refresh();
	insiScroll.scrollTo(0,0,0);  
	
	updateIns()
                      
}


function aHealthInfoClicked(event)
{             
	listFilter.hide();
	var hino; 
	
	if(FNS.device.touchSupport){
		hino = parseInt(FNS.lastTouchEnd.substring(11));  
	}
	else{
		hino = parseInt(event.target.id.substring(11));  		
	}                
	
	showiFrameDiv("./docs/HEALTHINFO/" + allHealthIssuesDirDict[hino] + "/index.html");
}


function aAfterERClicked(event)
{
	var hino;
	
	if(FNS.device.touchSupport){
		hino = parseInt(FNS.lastTouchEnd.substring(8));  
	}
	else{
		hino = parseInt(event.target.id.substring(8));  		
	}

	showiFrameDiv("./docs/Places/" + allAfterERDirDict[hino] + "/index.html");
	
	return;
}


function isInMyNots(probNo){
	var flag = PV.myNots.indexOf("," + probNo.toString() + ",");
	if (flag >= 0){
		return true;
	}
	
	return false;
}

function addToMyNots(probNo, rand){	
	FNS.applyStyle.readMail("not_" + probNo + "_" + rand);
	PV.myNots = PV.myNots + probNo.toString() + ",";
	PersistantValue.set("myNots", PV.myNots);
	
}


function removeFromMyNots(probNo, rand)
{		
	FNS.applyStyle.unreadMail("not_" + probNo + "_" + rand);
	PV.myNots = PV.myNots.substring(0,PV.myNots.indexOf(","+ probNo.toString() +",")) + PV.myNots.substring(PV.myNots.indexOf(","+ probNo.toString() +",") + probNo.toString().length + 1);
	PersistantValue.set("myNots", PV.myNots);
}






function isInMyDiseases(probNo){
	var flag = PV.myDiseases.indexOf("," + probNo.toString() + ",");
	if (flag >= 0){
		return true;
	}
	
	return false;
}

function addToMyDiseases(probNo){	
	FNS.applyStyle.selected("medProbs_" + probNo);
	PV.myDiseases = PV.myDiseases + probNo.toString() + ",";
	PersistantValue.set("myDiseases", PV.myDiseases);
	
}


function removeFromMyDiseases(probNo)
{		
	FNS.applyStyle.unselected("medProbs_" + probNo);
	PV.myDiseases = PV.myDiseases.substring(0,PV.myDiseases.indexOf(","+ probNo.toString() +",")) + PV.myDiseases.substring(PV.myDiseases.indexOf(","+ probNo.toString() +",") + probNo.toString().length + 1);
	PersistantValue.set("myDiseases", PV.myDiseases);
}
   


function isInMyMeds(probNo){
	var flag = PV.myMeds.indexOf("," + probNo.toString() + ",");
	if (flag >= 0){
		return true;
	}
	
	return false;
}

function addToMyMeds(probNo){
	FNS.applyStyle.selected("meds_" + probNo);	
	PV.myMeds = PV.myMeds + probNo.toString() + ",";
	PersistantValue.set("myMeds", PV.myMeds);
}


function removeFromMyMeds(probNo)
{		
	FNS.applyStyle.unselected("meds_" + probNo);
	
	PV.myMeds = PV.myMeds.substring(0,PV.myMeds.indexOf(","+ probNo.toString() +",")) + PV.myMeds.substring(PV.myMeds.indexOf(","+ probNo.toString() +",") + probNo.toString().length + 1);
	
	PersistantValue.set("myMeds", PV.myMeds);
}
  


function isInMyMedAlgs(probNo){
	var flag = PV.myAllergies.indexOf("," + probNo.toString() + ",");
	if (flag >= 0){
		return true;
	}
	
	return false;
}

function addToMyMedAlgs(probNo){
	FNS.applyStyle.selected("medAlgs_" + probNo);
	PV.myAllergies = PV.myAllergies + probNo.toString() + ",";
	PersistantValue.set("myAllergies", PV.myAllergies);
}


function removeFromMyMedAlgs(probNo)
{		
	FNS.applyStyle.unselected("medAlgs_" + probNo);
	PV.myAllergies = PV.myAllergies.substring(0,PV.myAllergies.indexOf(","+ probNo.toString() +",")) + PV.myAllergies.substring(PV.myAllergies.indexOf(","+ probNo.toString() +",") + probNo.toString().length + 1);
	PersistantValue.set("myAllergies", PV.myAllergies);
}
   

function isInMySurgs(probNo)
{
	var flag = PV.mySurgeries.indexOf("," + probNo.toString() + ",");
	if (flag >= 0){
		return true;
	}
	return false;
}

function addToMySurgs(probNo)
{
	FNS.applyStyle.selected("surg_" + probNo);
	PV.mySurgeries = PV.mySurgeries + probNo.toString() + ",";
	PersistantValue.set("mySurgeries", PV.mySurgeries);
}


function removeFromMySurgs(probNo)
{		
	FNS.applyStyle.unselected("surg_" + probNo);
	PV.mySurgeries = PV.mySurgeries.substring(0,PV.mySurgeries.indexOf(","+ probNo.toString() +",")) + PV.mySurgeries.substring(PV.mySurgeries.indexOf(","+ probNo.toString() +",") + probNo.toString().length + 1);
	PersistantValue.set("mySurgeries", PV.mySurgeries);
}
     

function isInMyIns(probNo){
	var flag = PV.myInsurances.indexOf("," + probNo.toString() + ",");
	if (flag >= 0){
		return true;
	}
	
	return false;
}

function addToMyIns(probNo)
{
	FNS.applyStyle.selected("ins_" + probNo);
	PV.myInsurances = PV.myInsurances + probNo.toString() + ",";
	PersistantValue.set("myInsurances", PV.myInsurances);
}


function removeFromMyIns(probNo)
{		
	FNS.applyStyle.unselected("ins_" + probNo);
	PV.myInsurances = PV.myInsurances.substring(0,PV.myInsurances.indexOf(","+ probNo.toString() +",")) + PV.myInsurances.substring(PV.myInsurances.indexOf(","+ probNo.toString() +",") + probNo.toString().length + 1);
	PersistantValue.set("myInsurances", PV.myInsurances);
}


function updateMPs()
{	                  	
	var temp1 = getLabelObject("hrDiseases");
	var count = countCommas(PV.customDiseases);
	   

	if (PV.myDiseases === "," && PV.customDiseases.length == 0)
	{
		temp1.innerHTML = "Add your medical problems";		
		return;
	}
	
	var outS = "";   
	  
	var allDiseases;
	
	if (currentLang === "English"){
		allDiseases = allDiseasesEN;
	}
	else{
		allDiseases = allDiseasesES;
	}
	
	for (var i=0; i < allDiseases.length; i++){		
		if (PV.myDiseases.indexOf(","+i+",") >= 0){
			outS = outS + allDiseases[i] + ", ";
			count = count + 1;
		}
	}
	 
    
    if(PV.customDiseases.length > 0){
        outS =   "<h1>Medical Problems (" + count + ") </h1> " + outS + " " + PV.customDiseases;
    }
    else{
        outS =   "<h1>Medical Problems (" + count + ") </h1>" + outS.substring(0,outS.length-2);	
	}
    
	temp1.innerHTML = outS;	
	
	         
	hriScroll.refresh();
	
	return outS;
}
 
function updateSurgs()
{	        	
	var temp1 = getLabelObject("hrSurgs");
	var count = countCommas(PV.customSurgs);

	if (PV.mySurgeries === "," && PV.customSurgs.length == 0)
	{
		temp1.innerHTML = "Add your surgery/procedures";		
		return;
	}
	
	var outS = "";   
	  
	var allDiseases;
	
	if (currentLang === "English"){
		allDiseases = allSurgsEN;
	}
	else{
		allDiseases = allSurgsES;
	}
	
	for (var i=0; i < allDiseases.length; i++){		
		if (PV.mySurgeries.indexOf(","+i+",") >= 0){
			outS = outS + allDiseases[i] + ", ";
			count = count + 1;
		}
	}
	 
    if(PV.customSurgs.length > 0){
        outS =   "<h1>Surgery/Procedures (" + count + ") </h1> " + outS + " " + PV.customSurgs;
    }
    else{
        outS =   "<h1> Surgery/Procedures (" + count + ") </h1> " + outS.substring(0,outS.length-2);
    }
    
    temp1.innerHTML = outS;

	   
	hriScroll.refresh();
	   	
	return outS;
}

function updateIns()
{	       	
	var temp1 = getLabelObject("hrIns");
	var count = countCommas(PV.customIns);
	
	if (PV.myInsurances === "," && PV.customIns.length == 0)
	{
		temp1.innerHTML = "Add insurance info";		
		return;
	}         
	
	
	var outS = "";   
	  
	var allDiseases = allInsurance;
	
	for (var i=0; i < allDiseases.length; i++){		
		if (PV.myInsurances.indexOf(","+i+",") >= 0){
			outS = outS + allDiseases[i][0] + ", ";
			count = count + 1;
		}
	}
	    
    
    if(PV.customIns.length > 0){
        outS =   "<h1>Insurance (" + count + ") </h1> " + outS + " " + PV.customIns;
    }
    else{
        outS =   "<h1> Insurance (" + count + ") </h1> " + outS.substring(0,outS.length-2);	
    }
    
    temp1.innerHTML = outS;
              
	hriScroll.refresh();
	            	
	return outS;
}

function updateMeds()
{	        
	var temp1 = getLabelObject("hrMeds");
	var count = 0;
	var count1 = countCommas(PV.customMeds);
	    
	
	if (PV.myMeds === "," && PV.customMeds.length == 0)
	{
		temp1.innerHTML = "Add your medications";		
		return;
	}
	
	var outS = "";   
	  	         
	var i, j;
	
	for (i=0; i < medicationKeys.length; i++){
		for (j=0; j < medication[medicationKeys[i]].length; j++){ 
			if (PV.myMeds.indexOf(","+count+",") >= 0){
				outS = outS + medication[medicationKeys[i]][j] + " (" + medicationKeys[i] + "), ";
				count1 = count1 + 1;
			}                       
			count = count + 1;
   		}
	}
	
    if(PV.customMeds.length > 0){
        outS =   "<h1>Medications (" + count1 + ") </h1> " + outS + " " + PV.customMeds;
    }
    else{
        outS =   "<h1>Medications (" + count1 + ") </h1> " + outS.substring(0,outS.length-2);  
    }
        temp1.innerHTML = outS;

	        
	hriScroll.refresh();
	             	
	return outS;
}
    
function updateMedAlgs()
{	             	
	var temp1 = getLabelObject("hrAllergies");
	var count = 0;
	var count1 = countCommas(PV.customMedAlgs);
	           
	
	
	if (PV.myAllergies === "," && PV.customMedAlgs.length == 0)
	{
		temp1.innerHTML = "Add your medication allergies";		
		return;
	}
	
	var outS = "";   
	  	         
	var i, j;
	
	for (i=0; i < medicationKeys.length; i++){
		for (j=0; j < medication[medicationKeys[i]].length; j++){ 
			if (PV.myAllergies.indexOf(","+count+",") >= 0){
				outS = outS + medication[medicationKeys[i]][j] + " (" + medicationKeys[i] + "), ";
				count1 = count1 + 1;
			}                       
			count = count + 1;
   		}
	}
	
    if(PV.customMedAlgs.length > 0){
        outS =   "<h1>Medication Allergies (" + count1 + ") </h1> " + outS + " " + PV.customMedAlgs;	
        
    }
    else{
        outS =   "<h1>Medication Allergies (" + count1 + ") </h1> " + outS.substring(0,outS.length-2);	
    }
    
    temp1.innerHTML = outS;

	hriScroll.refresh();
	  	
	return outS;
}
  



function storeUpdatedName()
{
	//PersistantValue.set("dayPercent", "0");       
    //PV.myName = ;
	
}


function aMedProblemClicked(event)
{
	var hino;
	
	if(FNS.device.touchSupport){
		hino = parseInt(FNS.lastTouchEnd.substring(9));  
	}
	else{
		hino = parseInt(event.target.id.substring(9));  		
	}

	toggleMedProb(hino);
}


function aMedClicked()
{
	var hino;
	
	if(FNS.device.touchSupport){
		hino = parseInt(FNS.lastTouchEnd.substring(5));  
	}
	else{
		hino = parseInt(event.target.id.substring(5));  		
	}
             
	toggleMed(hino);
	
}
    
function aMedAlgClicked()
{
	var hino;
	
	if(FNS.device.touchSupport){
		hino = parseInt(FNS.lastTouchEnd.substring(8));  
	}
	else{
		hino = parseInt(event.target.id.substring(8));  		
	}
             
	toggleMedAlg(hino);
}

function aSurgClicked()
{
	var hino;
	
	if(FNS.device.touchSupport){
		hino = parseInt(FNS.lastTouchEnd.substring(5));  
	}
	else{
		hino = parseInt(event.target.id.substring(5));  		
	}
             
	toggleSurg(hino);
}

function aInsClicked()
{
	var hino;
	
	if(FNS.device.touchSupport){
		hino = parseInt(FNS.lastTouchEnd.substring(4));  
	}
	else{
		hino = parseInt(event.target.id.substring(4));  		
	}
             
	toggleIns(hino);
}

function toggleNot(event)
{
    var hino;
    var rand;

	
	if(FNS.device.touchSupport){
        hino = getStringuptoUS(FNS.lastTouchEnd.substring(4)); 
        rand = getStringuptoUS(FNS.lastTouchEnd.substring(5 + hino.length));	     
	}
	else{
        hino = getStringuptoUS(event.target.id.substring(4));  
		rand = getStringuptoUS(event.target.id.substring(5 + hino.length));	    
	}
    
    var probNo = hino;
    
	if (PV.myNots === ","){
		addToMyNots(probNo, rand);
	}
	else if (isInMyNots(probNo)){
		removeFromMyNots(probNo, rand);
	}
	else{
		addToMyNots(probNo, rand);
	}
	
	return;
}  


function toggleMedProb(probNo)
{
	if (PV.myDiseases === ","){
		addToMyDiseases(probNo);
	}
	else if (isInMyDiseases(probNo)){
		removeFromMyDiseases(probNo);
	}
	else{
		addToMyDiseases(probNo);
	}
	
	
	return;
}    

function toggleMed(probNo)
{                  
	
	if (PV.myMeds === ","){
		addToMyMeds(probNo);
	}
	else if (isInMyMeds(probNo)){
		removeFromMyMeds(probNo);
	}
	else{
		addToMyMeds(probNo);
	}
	
	
	return;
}
  
function toggleMedAlg(probNo)
{                  
	
	if (PV.myAllergies === ","){
		addToMyMedAlgs(probNo);
	}
	else if (isInMyMedAlgs(probNo)){
		removeFromMyMedAlgs(probNo);
	}
	else{
		addToMyMedAlgs(probNo);
	}
	
	
	return;
}
   

function toggleSurg(probNo)
{                  

	if (PV.mySurgeries === ","){
		addToMySurgs(probNo);
	}
	else if (isInMySurgs(probNo)){
		removeFromMySurgs(probNo);
	}
	else{
		addToMySurgs(probNo);
	}


	return;
}
  

function toggleIns(probNo)
{                  
    
	if (PV.myInsurances === ","){
		addToMyIns(probNo);
	}
	else if (isInMyIns(probNo)){
		removeFromMyIns(probNo);
	}
	else{
		addToMyIns(probNo);
	}


	return;
}


function showiFrameDiv(url)
{                  
	FNS.goCenter("iFrameScreen");
	
	getElementObject("iFrameTitle").innerHTML = "loading ..."
		
	$('#iFrameHTML').load(url, function() {
	  showiFrameDiv_aux();
	});	   
	
	if (listFilter.currentList === "healthInfo" || listFilter.currentList === "afterER"){ 
		listFilter.hide();                        
	}
	
		
}

function showiFrameDiv_aux()
{      
	if (currentLang === "English"){
		$("#iFrameScreen .es").hide(0);
		$("#iFrameScreen .en").show(0);
	}
	else{
        $("#iFrameScreen .en").hide(0);
		$("#iFrameScreen .es").show(0);
	}                         
	
	iFrameHTMLiScroll.scrollTo(0,0,0);	
	iFrameHTMLiScroll.refresh();
	
	getElementObject("iFrameTitle").innerHTML = ""

}

function hideiFrameDiv()
{     
	if (listFilter.currentList === "healthInfo" || listFilter.currentList === "afterER"){ 
		listFilter.show();                        
	}
	
	FNS.goDown("iFrameScreen");
	setTimeout("hideiFrameDiv_Aux()", 700);
}

function hideiFrameDiv_Aux()
{
	$("#iFrameHTML").load("docs/blank.html");
}

function alphabetSelectedMedProbs(letter)
{	
	var allDli;
	
	if (currentLang === "English"){
		allDli = allDiseasesEN;
	}
	else{
		allDli = allDiseasesES;
	}
	      
	var count = 0;
	
	for (var i=0; i < allDli.length; i++){
		var desc = allDli[i].toLowerCase().indexOf(letter.toLowerCase());
		if (desc == 0){
			getElementStyleObject("medProbs_" + i).display = "block";
			count = count + 1;
		}
		else{
			getElementStyleObject("medProbs_" + i).display = "none";
		}
	}
	
	if (count == 0){
		showNoResultsMessage(letter);
	}	
		
	mpiScroll.refresh(); 
	mpiScroll.scrollTo(0,0,0);
	
}



function alphabetSelectedHI(letter)
{
	
	var allDli;
	     
	if (currentLang === "English"){
		allDli = allHealthIssues; 
	}
	else{
		allDli = allHealthIssuesES; 
	}
	
	var count = 0;
	
	for (var i=0; i < allDli.length; i++){
		var desc = allDli[i].toLowerCase().indexOf(letter.toLowerCase());
		if (desc == 0){
			getElementStyleObject("healthInfo_" + i).display = "block";
			count = count + 1;
		}
		else{
			getElementStyleObject("healthInfo_" + i).display = "none";
		}
	}
	
	if (count == 0){
		showNoResultsMessage(letter);
	}	
	
	healthInfoiScroll.refresh();
	healthInfoiScroll.scrollTo(0,0,0);
}   


function searchHI(letter)
{	
	var allDli;
	if (currentLang === "English"){
		allDli = allHealthIssues; 
	}
	else{
		allDli = allHealthIssuesES; 
	}
	 
	
	var count = 0;
	
	for (var i=0; i < allDli.length; i++){
		var desc = allDli[i].toLowerCase().indexOf(letter.toLowerCase());
		if (desc >= 0){
			getElementStyleObject("healthInfo_" + i).display = "block";
			count = count + 1;
		}
		else{
			getElementStyleObject("healthInfo_" + i).display = "none";
		}
	}
	
	if (count == 0){
		showNoResultsMessage(letter);
	}	
	
	healthInfoiScroll.refresh();
	healthInfoiScroll.scrollTo(0,0,0);   
	
	
}
 

function alphabetSelectedAER(letter)
{
	
	var allDli = allAfterER;
	
	var count = 0;
	
	for (var i=0; i < allDli.length; i++){
		var desc = allDli[i].toLowerCase().indexOf(letter.toLowerCase());
		if (desc == 0){
			getElementStyleObject("afterER_" + i).display = "block";
			count = count + 1;
		}
		else{
			getElementStyleObject("afterER_" + i).display = "none";
		}
	}
	
	if (count == 0){
		showNoResultsMessage(letter);
	}	
	
	fuciScroll.refresh(); 
	fuciScroll.scrollTo(0,0,0);
	
}    

function searchMedProbs(letter)
{		
	var allDli;
	if (currentLang === "English"){
		allDli = allDiseasesEN; 
	}
	else{
		allDli = allDiseasesES; 
	}
	
	
	var count = 0;
	
	for (var i=0; i < allDli.length; i++){
		var desc = allDli[i].toLowerCase().indexOf(letter.toLowerCase());
		if (desc >= 0){
			getElementStyleObject("medProbs_" + i).display = "block";
			count = count + 1;
		}
		else{
			getElementStyleObject("medProbs_" + i).display = "none";
		}
	}
	
	if (count == 0){
		showNoResultsMessage(letter);
	}	
		
	mpiScroll.refresh(); 
	mpiScroll.scrollTo(0,0,0);
	
}
 

      

function alphaMeds(letter)
{		
    
	var count = 0;
    var i, j;

	      
	for (i=0; i < medicationKeys.length; i++){
		for (j=0; j < medication[medicationKeys[i]].length; j++){
			var desc = medication[medicationKeys[i]][j].toLowerCase().indexOf(letter.toLowerCase());
			if (desc == 0){
				getElementStyleObject("meds_" + count).display = "block";
			}
			else{
				getElementStyleObject("meds_" + count).display = "none"; 
			}    
			count = count + 1;
		}   
	}
	
	if (count == 0){
		showNoResultsMessage(letter);
	}	
		
	medsiScroll.refresh(); 
	medsiScroll.scrollTo(0,0,0);
	
}
    

function searchMeds(letter)
{		
    
	var count = 0;
    var i, j;

	      
	for (i=0; i < medicationKeys.length; i++){
		for (j=0; j < medication[medicationKeys[i]].length; j++){
			var desc = medication[medicationKeys[i]][j].toLowerCase().indexOf(letter.toLowerCase());
			if (desc >= 0){
				getElementStyleObject("meds_" + count).display = "block";
			}
			else{
				getElementStyleObject("meds_" + count).display = "none"; 
			}    
			count = count + 1;
		}   
	}
	
	if (count == 0){
		showNoResultsMessage(letter);
	}	
		
	medsiScroll.refresh(); 
	medsiScroll.scrollTo(0,0,0);
	
}
        

function alphaMedAlgs(letter)
{		
    
	var count = 0;
    var i, j;

	      
	for (i=0; i < medicationKeys.length; i++){
		for (j=0; j < medication[medicationKeys[i]].length; j++){
			var desc = medication[medicationKeys[i]][j].toLowerCase().indexOf(letter.toLowerCase());
			if (desc == 0){
				getElementStyleObject("medAlgs_" + count).display = "block";
			}
			else{
				getElementStyleObject("medAlgs_" + count).display = "none"; 
			}    
			count = count + 1;
		}   
	}
	
	if (count == 0){
		showNoResultsMessage(letter);
	}	
		
	medAlgsiScroll.refresh(); 
	medAlgsiScroll.scrollTo(0,0,0);
	
}
    

function searchMedAlgs(letter)
{		
    
	var count = 0;
    var i, j;

	      
	for (i=0; i < medicationKeys.length; i++){
		for (j=0; j < medication[medicationKeys[i]].length; j++){
			var desc = medication[medicationKeys[i]][j].toLowerCase().indexOf(letter.toLowerCase());
			if (desc >= 0){
				getElementStyleObject("medAlgs_" + count).display = "block";
			}
			else{
				getElementStyleObject("medAlgs_" + count).display = "none"; 
			}    
			count = count + 1;
		}   
	}
	
	if (count == 0){
		showNoResultsMessage(letter);
	}	
		
	medAlgsiScroll.refresh(); 
	medAlgsiScroll.scrollTo(0,0,0);
	
}
       
function alphaSurgs(letter)
{		
	var allDli;
	if (currentLang === "English"){
		allDli = allSurgsEN; 
	}
	else{
		allDli = allSurgsES; 
	}
	
	
	var count = 0;
	
	for (var i=0; i < allDli.length; i++){
		var desc = allDli[i].toLowerCase().indexOf(letter.toLowerCase());
		if (desc == 0){
			getElementStyleObject("surg_" + i).display = "block";
			count = count + 1;
		}
		else{
			getElementStyleObject("surg_" + i).display = "none";
		}
	}
	
	if (count == 0){
		showNoResultsMessage(letter);
	}	
		
	surgiScroll.refresh(); 
	surgiScroll.scrollTo(0,0,0);
	
}

function searchSurgs(letter)
{		
	var allDli;
	if (currentLang === "English"){
		allDli = allSurgsEN; 
	}
	else{
		allDli = allSurgsES; 
	}
	
	
	var count = 0;
	
	for (var i=0; i < allDli.length; i++){
		var desc = allDli[i].toLowerCase().indexOf(letter.toLowerCase());
		if (desc >= 0){
			getElementStyleObject("surg_" + i).display = "block";
			count = count + 1;
		}
		else{
			getElementStyleObject("surg_" + i).display = "none";
		}
	}
	
	if (count == 0){
		showNoResultsMessage(letter);
	}	
		
	surgiScroll.refresh(); 
	surgiScroll.scrollTo(0,0,0);
	
}
             

function alphaIns(letter)
{		
	var allDli = allInsurance;

	var count = 0;

	for (var i=0; i < allDli.length; i++){
		var desc = allDli[i][0].toLowerCase().indexOf(letter.toLowerCase());
		if (desc == 0){
			getElementStyleObject("ins_" + i).display = "block";
			count = count + 1;
		}
		else{
			getElementStyleObject("ins_" + i).display = "none";
		}
	}         
	
	if(letter === "A"){
		getElementStyleObject("ins_0").display = "block";
		getElementStyleObject("ins_1").display = "block";
		
	}

	if (count == 0){
		showNoResultsMessage(letter);
	}	

	insiScroll.refresh(); 
	insiScroll.scrollTo(0,0,0);

}

function searchIns(letter)
{		
	var allDli = allInsurance;


	var count = 0;

	for (var i=0; i < allDli.length; i++){
		var desc = allDli[i][0].toLowerCase().indexOf(letter.toLowerCase());
		if (desc >= 0){
			getElementStyleObject("ins_" + i).display = "block";
			count = count + 1;
		}
		else{
			getElementStyleObject("ins_" + i).display = "none";
		}
	}

	if (count == 0){
		showNoResultsMessage(letter);
	}	

	insiScroll.refresh(); 
	insiScroll.scrollTo(0,0,0);

}  
	
	
function searchAER(letter)
{	
	var allDli = allAfterER;
	
	var count = 0;
	
	for (var i=0; i < allDli.length; i++){
		var desc = allDli[i].toLowerCase().indexOf(letter.toLowerCase());
		if (desc >= 0){
			getElementStyleObject("afterER_" + i).display = "block";
			count = count + 1;
		}
		else{
			getElementStyleObject("afterER_" + i).display = "none";
		}
	}
	
	if (count == 0){
		showNoResultsMessage(letter);
	}	
	
	fuciScroll.refresh(); 
	fuciScroll.scrollTo(0,0,0); 
   	
}


function showNoResultsMessage(letter)
{
	messageHandler.flashNewMessage("no results", "")
}

function showNoResultsMessageHI(letter)
{
	messageHandler.flashNewMessage("There are no results for '"  + letter, "")
}

function goToTopMPList()
{
	mpiScroll.scrollTo(0,0,500);
	return;
}

function goToTopiFrameHTML()
{
	iFrameHTMLiScroll.scrollTo(0,0,500);
	return;
}


function goToTopHRScreen()
{
	hriScroll.scrollTo(0,0,500);
	goToTopiFrameHTML();
	return;
}


function goToTopAboutScreen()
{
	aboutHTMLiScroll.scrollTo(0,0,500);
	return;
}

function goToTopHIScreen()
{
	healthInfoiScroll.scrollTo(0,0,500);
}

function handleInputFocus()
{
	document.body.scrollTop = 0; 
	document.body.scrollLeft = 0; 
	FNS.goDown("confMessage");
}

function doneSetPassword()
{
	
	var pass1 = getElementObject("passwd1").value;
	var pass2 = getElementObject("passwd2").value;
	
	if (pass1 != pass2){
		messageHandler.flashNewMessage("Passwords do not match.", "password has to be 3 characters or longer and entered correctly each time");
		return;
	}
	
	if(pass1.length < 3){
		messageHandler.flashNewMessage("Password too short.", "password has to be 3 characters or longer");
		return;		
	}
	
	getElementStyleObject("setPassword").display = "none";
	
	getElementObject("passwd1").blur();
	getElementObject("passwd2").blur();


	PersistantValue.set("passwd", hex_md5(pass1));
	
	PersistantValue.set("passwdSet", "yes");       
    PV.passwdSet = "yes";

	getLabelObject("passwdPref").innerHTML = "Change password";

    setReqFields(false);
    
	return;
}

function cancelSetPassword()
{
	getElementStyleObject("setPassword").display = "none";
	getElementObject("passwd1").blur();
	getElementObject("passwd2").blur();
	return;
}


function setPasswordClicked_Main()
{	
	if (PV.passwdSet === "yes"){
		getElementStyleObject("passwdWrapper").display = "block"; 
		getElementObject("currentPasswd").focus();
		return;
	}
	setPasswordClicked();
	
	return;
}

function setPasswordClicked()
{	
	getElementStyleObject("setPassword").display = "block";
	return;
}
       
function refreshPatientID()
{
	patientID = hex_md5(PV.myFirstName + PV.myLastName + PV.dob) + PV.myRandom;
}                
  
function confirmSessionClose()
{
	FNS.goCenter("AgreementScreen1");
}   

function startSessionEULA() 
{
    if(inSession){
		confirmSessionClose();
	}
    else{
        FNS.goCenter("AgreementScreen");
    }
    
    if (currentLang === "English"){
        getElementStyleObject("agen").display = "block";
        getElementStyleObject("ages").display = "none";
	}
	else{
        getElementStyleObject("agen").display = "none";
        getElementStyleObject("ages").display = "block";	
    }
    
    return;

}
   
var inSession = false;


function endSession()
{
       
	inSession = false;
	FNS.goDown("AgreementScreen1");
	
	getLabelObject("sessionPref").innerHTML = "Not at the ER";
	FNS.applyStyle.unselected("sessionPref");
	
    
	FNS.applyStyle.unselected("welcomeListItem");
	FNS.applyStyle.unselected("intakeListItem");
	FNS.applyStyle.unselected("quickLookListItem");
	FNS.applyStyle.unselected("evaluationListItem");
	FNS.applyStyle.unselected("continuingCareListItem");
	FNS.applyStyle.unselected("observationDischargeListItem");
	

	
	refreshPatientID();
	
    var url = 'https://www.iercares.com/service/service.php?function=delete-session&parameters={"userId":"' + patientID +  '",' + '"superAuthToken":"3qwdEPv1Itxvx6Db6OWFFcAC1gvrR7Oq"' + '}';

	// console.log(url);



	//var callback = function(s){alert(s)};
	var callback = function(){};
    messageHandler.flashNewMessage("Session ended", "");
	loadFile(url, callback); 	
}


function startSession()
{
       
	FNS.goDown("AgreementScreen");
	
	getLabelObject("sessionPref").innerHTML = "Checked in";	
	FNS.applyStyle.selected("sessionPref");
	
	refreshPatientID();
	
	var url = 'https://www.iercares.com/service/service.php?function=generate-session&parameters={"userId":"' + patientID + '", "dob":"' + PV.dob + '","myFirstName":"' + PV.myFirstName + '","myLastName":"' + PV.myLastName + '","myDiseases":"' + getDiseasesString() + '","myMeds":"' + getMedsString() +  '","myAllergies":"' + getMedAlgsString() + '","mySurgeries":"' + getSurgString() + '","myInsurances":"' + getInsString() + '","passwdSet":"' + PV.passwdSet + '","lang":"' + currentLang + '","passwdSet":"' + PV.passwdSet + '","nameSet":"' + PV.nameSet + '","passwd":"' + PV.passwd + '",' + '"superAuthToken":"3qwdEPv1Itxvx6Db6OWFFcAC1gvrR7Oq"' + ',"provider":"' + '"}';

	// console.log(url);



	var callback = startSessionCallback;
	loadFile(url, callback);
}


function startSessionCallback(status)
{
    if(status === "1"){
        messageHandler.flashNewMessage("Session successfully created!", "");
        inSession = true;
    }
    else{
        inSession = false;
        messageHandler.flashNewMessage("Unable to create session", "try again in a few minutes");
    }
    
}
                    

function getDiseasesString()
{
	return updateMPs();
} 
 

function getMedsString()
{
	return updateMeds();
}


function getMedAlgsString()
{
	return updateMedAlgs();
}


function getSurgString()
{
	return updateSurgs();
}
  

function getInsString()
{
	return updateIns();
}

function loadERStatus()
{
    activityIndicator.show();
    
    var url = 'https://www.iercares.com/service/service.php?function=get-navigation&parameters={"userId":"' + patientID +  '",' + '"superAuthToken":"3qwdEPv1Itxvx6Db6OWFFcAC1gvrR7Oq"' + '}';

    // console.log(url);
	var callback = loadERStatus_Aux;
	loadFile(url, callback);
}

function loadERStatus_Aux(status)
{
    data5 = status;    
    activityIndicator.hide();

	if(status.length < 6){
		updateERStatus(ERStatusDefault);
		copyERStatusFromString(ERStatusDefault);
	}
    else{
        copyERStatusFromString(status.substring(1));
    }
    
    ERNaviScroll.refresh();

}

function copyERStatusFromString(source){
	ERStatus = [];
	for(var i = 0; i < source.length; i++){
		ERStatus[i] = source[i];
	}

	
	
	FNS.applyStyle.unselected("welcomeListItem");
	FNS.applyStyle.unselected("intakeListItem");
	FNS.applyStyle.unselected("quickLookListItem");
	FNS.applyStyle.unselected("evaluationListItem");
	FNS.applyStyle.unselected("continuingCareListItem");
	FNS.applyStyle.unselected("observationDischargeListItem");
	
	if (ERStatus[0] == "1"){
		FNS.applyStyle.selected("welcomeListItem");
	}
	
	if (ERStatus[1] == "1"){
		FNS.applyStyle.selected("intakeListItem");
	}
	
	if (ERStatus[2] == "1"){
		FNS.applyStyle.selected("quickLookListItem");
	}
	
	if (ERStatus[3] == "1"){
		FNS.applyStyle.selected("evaluationListItem");
	}
	
	if (ERStatus[4] == "1"){
		FNS.applyStyle.selected("continuingCareListItem");
	}
	
	if (ERStatus[5] == "1"){
		FNS.applyStyle.selected("observationDischargeListItem");
	}
	


	return;
}


function updateERStatus(status)
{
    var url = 'https://www.iercares.com/service/service.php?function=get-navigation&parameters={"userId":"' + patientID +  '",' + '"superAuthToken":"3qwdEPv1Itxvx6Db6OWFFcAC1gvrR7Oq"' + '}';

	var url = 'https://www.iercares.com/service/service.php?function=update-navigation&parameters={"userId":"' + patientID + '", "navigator":"' + status +  '","superAuthToken":"3qwdEPv1Itxvx6Db6OWFFcAC1gvrR7Oq"' + '"}'
    
    // console.log(url);
	
	var callback = function(){};
	loadFile(url, callback);
}



var totalNots = 0;

function populateNots()
{
    activityIndicator.show();
    getElementObject('notificationsList').innerHTML = "";

    var url = 'https://www.iercares.com/service/service.php?function=get-notifications&parameters={"userId":"' + patientID +  '",' + '"superAuthToken":"3qwdEPv1Itxvx6Db6OWFFcAC1gvrR7Oq"' + '}';
	var callback = populateNots_Aux;
    // console.log(url);
    
	loadFile(url, callback);
}


function populateNots_Aux(allData)
{
	var i;
    var tp = [];
	var allNots = JSON.parse(allData);
    
    data5 = allNots;
 	
	var Parent = document.getElementById("notificationsList");
    	
	for (i=0; i < allNots.length; i++){
        
		
		if (allNots[i]['user_name'] != patientID){
			continue;
		}
		
		var NewLI = document.createElement("LI");
        

		NewLI.innerHTML = 	"<p> "+ allNots[i].message + "</p>"
							+ 	"<h2> By "+ allNots[i].sent_by + ", at " + allNots[i].create_date + "</h2>"

		
        var temp = "not_" + allNots[i].id + "_" + randomsalt; 					
        NewLI.id = temp;    

        tp[tp.length] = NewLI.id;
		randomsalt++;

		Parent.appendChild(NewLI);
        
	}
	
    var count = 0;

    for (i=0; i < tp.length; i++){
		FNS.addButtonElement(tp[i], toggleNot);
        console.log(getStringuptoUS(tp[i].substring(4)));
        if(isInMyNots(getStringuptoUS(tp[i].substring(4)))){ 
			FNS.applyStyle.readMail(tp[i]);
		}
        else{
            FNS.applyStyle.unreadMail(tp[i]);
        }
    }   
    
    totalNots = tp.length;
    count = totalNots - (countCommas(PV.myNots) - 1);


	notificationsiScroll.refresh();
	notificationsiScroll.scrollTo(0,0,0);

    if(count > 0){
        getElementObject('systemNotN').innerHTML = count.toString();
        getElementStyleObject('systemNotN').display = "block";
    }
    else{
        getElementObject('systemNotN').innerHTML = "";
        getElementStyleObject('systemNotN').display = "none";
    }
    activityIndicator.hide();

}


 
function tagSelected(tag)
{ 
	var allDli = allAfterERTags;
	
	if(tag === "All"){
		for (var i=0; i < allDli.length; i++){
			getElementStyleObject("afterER_" + i).display = "block";
		}         
		return;
	}   
	
	
	var count = 0;
	
	for (var i=0; i < allDli.length; i++){
		var desc = allDli[i].indexOf(tag);
		if (desc >= 0){
			getElementStyleObject("afterER_" + i).display = "block";
			count = count + 1;
		}
		else{
			getElementStyleObject("afterER_" + i).display = "none";
		}
	}
	
	if (count == 0){
		showNoResultsMessage(tag);
	}	
	
	fuciScroll.refresh();
	fuciScroll.scrollTo(0,0,0);
}


function getiScrollStateSpec()
{
    
    var iScrollList = ['settingsList', 'pharmacyList', 'notificationsList', 'healthRecordsList', 'healthInfoList', 'ERNavList', 'medProbList', 'medicationList', 'medAlgsList', 'surgsList', 'insList'];
    var retS = "";
    for(var i = 0; i < iScrollList.length; i++){
        retS = retS + getElementStyleObject(iScrollList[i]).webkitTransform; 
    }
    return retS;
}