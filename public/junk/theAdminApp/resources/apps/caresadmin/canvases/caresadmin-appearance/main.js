var caresadminappearanceinit = function()
{
  getElementObject("appearanceInputBox").innerHTML = '<p class="text"> Header Background</p> <input autocapitalize="off" autocorrect="off" class="inputDI" id="appHBGRed" onkeypress="appChanged()" onchange="appPreviewUpdate()"> <input autocapitalize="off" autocorrect="off" class="inputDI" id="appHBGGreen" onkeypress="appChanged()" onchange="appPreviewUpdate()"> <input autocapitalize="off" autocorrect="off" class="inputDI" id="appHBGBlue" onkeypress="appChanged()" onchange="appPreviewUpdate()"><p class="text"> Header Text Color</p> <input autocapitalize="off" autocorrect="off" class="inputDI" id="appHCRed" onkeypress="appChanged()" onchange="appPreviewUpdate()"> <input autocapitalize="off" autocorrect="off" class="inputDI" id="appHCGreen" onkeypress="appChanged()" onchange="appPreviewUpdate()"> <input autocapitalize="off" autocorrect="off" class="inputDI" id="appHCBlue" onkeypress="appChanged()" onchange="appPreviewUpdate()"><p class="text"> Main Screen Background </p> <input autocapitalize="off" autocorrect="off" class="inputDI" id="appMainRed" onkeypress="appChanged()" onchange="appPreviewUpdate()"> <input autocapitalize="off" autocorrect="off" class="inputDI" id="appMainGreen" onkeypress="appChanged()" onchange="appPreviewUpdate()"> <input autocapitalize="off" autocorrect="off" class="inputDI" id="appMainBlue" onkeypress="appChanged()" onchange="appPreviewUpdate()"> <p class="text"> Upload/Change App Icon </p><input type="file" id="appIconUpload">';

}
function appearanceActive()
{
  getAppearanceInfo();
}

function getAppearanceInfo()
{
  var url = "../";
  url = url + "getLocationAppInfo";
  url = url + "?loc=" + Location;
  var callback = getAppearanceInfo_CB;
  loadFile(url, callback);
}

var data68;

function getAppearanceInfo_CB(data)
{
  var hInfo = JSON.parse(decodeURIComponent(data));
  data68 = hInfo;
  var hIKeys = Object.keys(hInfo);
  var temp;
  temp = "name";
  if(hIKeys.contains(temp) < 0){
    getElementObject("headerTitlePreview2").innerHTML = "";
  }
  else{
    getElementObject("headerTitlePreview2").innerHTML = decodeURIComponent(hInfo[temp]);
  }
  
  temp = "color";
  if(hIKeys.contains(temp) < 0){
    temp = ["0", "0", "0", "100", "100", "100", "255", "255", "255"];
  }
  else{
    temp = JSON.parse(hInfo[temp]);
  }

  for(var i=0; i<appNumbers.length; i++){
    getElementObject(appNumbers[i]).value = temp[i];
  }
  appPreviewUpdate();
}

function saveAppearanceClicked()
{
  var t = appPreviewUpdate();
  if(t!=null){
    saveAppearanceInfo();
  }
}
function saveAppearanceInfo()
{
  var url = "../";
  url = url + "editLocation";
  url = url + "?loc=" + Location;
  url = url + "&color=" + encodeURIComponent(JSON.stringify(appPreviewUpdate()));
  console.log(url);
  var callback = function(){}
  loadFile(url, callback);
}

function appearanceDeActive()
{
  for(var i=0; i< appNumbers.length; i++){
    getElementObject(appNumbers[i]).value = "";
  }
}

function appChanged()
{
  dataEdited = true;
  unsavedDataPlugin.show();
}

function isNormalInteger(str) {
      var n = ~~Number(str);
      return String(n) === str && n >= 0 && n <=255;
}

var appNumbers = ["appMainRed","appMainGreen","appMainBlue","appHBGRed","appHBGGreen","appHBGBlue","appHCRed", "appHCGreen","appHCBlue"];

function appPreviewUpdate()
{
  var temp;
  var valid = [true,true,true];
  var redColor = "rgb(209, 72, 54)";
  var colors = new Array();
  for(var i=0; i< appNumbers.length; i++){
    temp = getElementObject(appNumbers[i]).value;
    if(isNormalInteger(temp)){
      getElementStyleObject(appNumbers[i]).backgroundColor = "";
      colors[colors.length] = temp;
    }
    else{
      valid[parseInt(i/3)] = false;
      getElementStyleObject(appNumbers[i]).backgroundColor = redColor;
    }
  }
  if(valid[0]){
    getElementStyleObject("appPreview").backgroundColor = "rgb(" + colors[0] + "," + colors[1] + "," + colors[2] + ")";
  }
  else{
    getElementStyleObject("appPreview").backgroundColor = "";
  }
  if(valid[1]){
    getElementStyleObject("headerPreview").backgroundColor = "rgb(" + colors[3] + "," + colors[4] + "," + colors[5] + ")";
  }
  else{
    getElementStyleObject("headerPreview").backgroundColor = "";
  }
  if(valid[2]){
    getElementStyleObject("headerPreview").color = "rgb(" + colors[6] + "," + colors[7] + "," + colors[8] + ")";
  }
  else{
    getElementStyleObject("headerPreview").color = "";
  }
  if(valid[0] && valid[1] && valid[2]){
    return colors;
  }
  else{
    return null;
  }
}


