var caresadminlogininit = function()
{
  var temp;
  temp = PersistantValue.get("loginLoc");
  if(temp == null){
    PersistantValue.set("loginLoc", "ALL");
  }

  getLocations();
  FNS.addTapElement("logInButton", loginClicked);
  FNS.addTapElement("logOutButton", logoutClicked);
}

var loginLocationID = "";
var userID;
var Location;

function loginClicked()
{
  userID = getElementObject("ERCusername" + loginLocationID).value;
  var passwd = hex_md5(getElementObject("ERCpassword" + loginLocationID).value);
  Location = getElementObject("ll" + loginLocationID).value;

  var url = "../";
  url = url + "loginAdmin";
  url = url + "?userID=" + userID;
  url = url + "&password=" + passwd;
  url = url + "&loc=" + Location;
  var callback = login_CB;
  loadFile(url, callback);
}

function login_CB(data)
{
  data56 = data;
  if(data.length < 13 || data.substring(0,13) != "Authenticated"){
    messageHandler.flashNewMessage("Login Failed!","Please check your login name and password");
    return;
  }
  activityIndicator.show();
  setTimeout("activityIndicator.hide()", 700);
  $(".nonLogin").fadeIn();
  getElementObject("loginInputBox").innerHTML = "";
  getElementObject("loginInputTitle").innerHTML = "You are now logged in as " + userID + " at " + Location + ".";
  getElementStyleObject("logOutButton").display = "block";
  getElementStyleObject("logInButton").display = "none";
  PersistantValue.set("loginLoc", Location);

}

function logoutClicked()
{
  var url = "../";
  url = url + "logout";
  var callback = logout_CB;
  loadFile(url, callback);
}

function logout_CB(data)
{
  $(".nonLogin").fadeOut();
  getLocations();
  getElementObject("loginInputTitle").innerHTML = "You will need ER Cares Login to customize ER Cares for your location.";
  getElementStyleObject("logOutButton").display = "none";
  getElementStyleObject("logInButton").display = "block";

}

function getLocations()
{
  var url = "../";
  url = url + "getLocations";
  var callback = getLocations_CB;
  loadFile(url, callback);
}

var data56;
function getLocations_CB(data)
{
  var locations = JSON.parse(data);
  data56 = locations;
  var temp = new Date().getTime();
  loginLocationID = temp;
  
  var outS = '<p class="text"> User Name</p> <input autocapitalize="off" autocorrect="off" class="input" id="ERCusername' + temp + '"> <p class="text"> Password </p> <input autocapitalize="off" autocorrect="off" type="password" class="input" id="ERCpassword' + temp + '">' + "<select class='selectInput' id='ll" + temp + "'>";
  for(var i = 0; i < locations.length; i++){
    if(Object.keys(locations[i]).contains("name") == -1){
      outS = outS + "<option value='" + locations[i].loc + "'>" + locations[i].loc + "</option>";
    }
    else{
      outS = outS + "<option value='" + locations[i].loc + "'>" + locations[i].name + "</option>";
    }
  }
  outS = outS + "</select>";
  getElementObject("loginInputBox").innerHTML = outS;
  $(".nonLogin").fadeOut();
    
  setTimeout("getElementObject('ll" + temp + "').value = " + '"' + PersistantValue.get("loginLoc") + '"', 1000);
}
