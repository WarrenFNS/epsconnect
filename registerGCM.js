var url = require('url'),
	querystring = require('querystring'),
	auth = require('../utils/authenticate.js');
	
function init(app, db)	{

	app.get('/registerGCM', function(req, res){ 
	if(req.method == 'GET'){
		var incoming = url.parse(req.url).query;
		var info = querystring.parse(incoming);
		db.collection('GCMStuff').findOne({userID:info.userID}, function(err, result) {
			if (err) throw err;
			if(result) { 
				if(result.regId.indexOf(info.regId) != -1){
					console.log("regId already exists for user");
				} else {
					db.collection('GCMStuff').update({userID:req.query.userID}, {'$addToSet':{regId:req.query.regId}}, function(err) {
					    if (err) throw err;
					    else{
							res.writeHead(200, "OK", {'Content-Type': 'text/html'});
							res.write("1")
							res.end();
						}
					});
				}
			}

			else{
				var regId = new Array();
				regIds[0] = info.regId;
				info.regId = regIds;
				console.log(info.regId);
				db.collection('GCMStuff').insert(info, function(err, result) {
					if (err) throw err;
					if (result) {							
						res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
						res.write("1");
						res.end();
					}
				});
			}
		});


	} else {
		console.log("[405] " + req.method + " to " + req.url);
		res.writeHead(405, "Method not supported", {'Content-Type': 'text/html'});
		res.end('<html><head><title>405 - Method not supported </title></head><body><h1>Method not suported.</h1></body></html>');
	}
	
	console.log("regId:" + req.query.regId + "\n userId:" + req.query.userId);
				
	});
}


exports.init = init;