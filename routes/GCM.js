function init(app, db)	{
var gcm = require('node-gcm'),
	url = require('url'),
	querystring = require('querystring'),
	auth = require('../utils/authenticate.js');

	app.get('/sendMessage', function(req, res){ 
		var message = new gcm.Message();		
		var sender = new gcm.Sender('AIzaSyB95UNqRNKa2MxPPw5l9pXvDV8ZeWrnv5c');
		var registrationIDs;
		var title = 'ERCares';
		var messageString = req.query.message;
		message.addData('title', title);
		message.addData('message', messageString);
		message.addData('msgcnt','1');
		message.collapseKey = 'demo';
		message.delayWhileIdle = true;
		message.timeToLive = 3;
		
		//adding regIDs for userID
		db.collection('GCMStuff').findOne({userID:req.query.userID}, function(err, result) {
			if (err){
				throw err;
				res.send("0");
			} 
			else{
					console.log("result");
					registrationIDs = result.regID;
				}
				
				/*
				 ** Parameters: message-literal, registrationIDs-array, No. of retries, callback-function
				 */

				sender.send(message, registrationIDs, 4, function (result) {
				    console.log(result);
					res.send("1");
				});
				
		});

		
	});
	
	app.get('/registerGCM', function(req, res){ 
		if(req.method == 'GET'){
			var incoming = url.parse(req.url).query;
			var info = querystring.parse(incoming);
			db.collection('GCMStuff').findOne({userID:info.userID}, function(err, result) {
				if (err) throw err;
				if(result) { 
					if(result.regID.indexOf(info.regID) != -1){
						console.log("regID already exists for user");
					} else {
						console.log(req.query.userID);
						db.collection('GCMStuff').update({userID:req.query.userID}, {'$addToSet':{regID:req.query.regID}}, function(err) {
					    	if (err) throw err;
					    		else{
									res.writeHead(200, "OK", {'Content-Type': 'text/html'});
									res.write("1")
									res.end();
								}
						});
					}
				}

				else{
					var regIDs = new Array();
					regIDs[0] = info.regID;
					info.regID = regIDs;
					console.log(info.regID);
					db.collection('GCMStuff').insert(info, function(err, result) {
						if (err) throw err;
						if (result) {							
							res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
							res.write("1");
							res.end();
						}
					});
				}
			});


		} else {
			console.log("[405] " + req.method + " to " + req.url);
			res.writeHead(405, "Method not supported", {'Content-Type': 'text/html'});
			res.end('<html><head><title>405 - Method not supported </title></head><body><h1>Method not suported.</h1></body></html>');
		}
	
		console.log("regID:" + req.query.regID + "\n userID:" + req.query.userID);
				
	});
	
	app.get('/showGCM', function(req, res){
		db.collection('GCMStuff').find().toArray(function(err, result) {
			if (err) throw err;
			else{
				res.writeHead(200, {"Content-Type": "text/plain"});
				for (i = 0; i< result.length; i++){
					var object = result[i];
					var output = '';
					for (property in object) {
					  	output += property + ': ' + object[property] + ' ';
					}
					output+='\n';
					res.write(output);
				}
				res.end();
			}
		});
	});	
	
	app.get('/clearGCM', function(req, res){
		db.collection('GCMStuff').remove( function(err, result) {
			if (err){
				throw err;
				res.send("0");
			} 
			else{
					res.send("1");
				}
			});
		});

}


exports.init = init;