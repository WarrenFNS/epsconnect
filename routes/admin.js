var 
	approvedUsers = ['superUser', 'admin'],
	userType = 'admin',
	users = require('../utils/user_stuff.js');
	
function init(app, db){
	
	app.get('/addAdmin', function (req, res){
		users.add(req, res, userType, approvedUsers, db);
	});
	
	app.get('/getAdmin', function(req, res){
		users.get(req, res, userType, approvedUsers, db);
	});
	
	app.get('/getAdmins', function(req, res){
		users.getAll(req, res, userType, approvedUsers, db);
	});

	app.get('/deleteAdmin', function (req, res){
		users.destroy(req, res, userType, approvedUsers, db);
	});
	
	app.get('/changeAdminPassword', function (req, res){
		users.changePassword(req, res, userType, approvedUsers, db);
	});

  app.get('/editAdmin', function(req, res){
    users.edit(req, reses, userType, approvedUsers, db);
    });
}
		

exports.init = init;
