var auth = require('../utils/authenticate.js');
var async = require('../node_modules/async')
var S3_KEY = 'AKIAIVWEGU4M57C3W75A';
var S3_SECRET = '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';
var baseS3URL = "https://s3.amazonaws.com/";
var S3_BUCKET ="epsconnect";
db = require('../node_modules/mongoskin').db('mongodb://fhsofaadajdp:skfbskjgowhfwb37632b2kbr@dharma.mongohq.com:10000/EPSConnectApp');
var knox = require('knox').createClient({
        key: S3_KEY,
        secret: S3_SECRET,
        bucket: S3_BUCKET
});

exports.upload = function(req, res){
        var requiredAcessPriv = req.body.accessPrivileges;
        if(auth.fileAccess(req.session.accessPrivileges,requiredAcessPriv, req.session.userType)){
		/*
                // Set up variables for S3
                var intName = req.body.fileInput;
                var filename = req.files.uploaded.name;
                var fileType = req.files.uploaded.type;
                var tmpPath = req.files.uploaded.path;
                var folder1 = req.body.category;
                var locations = req.body.locations;
                var locationsArray = locations.split(",");
                console.log(locationsArray);
                for (i=0; i<locationsArray.length;i++){
                        var s3Path = '/' + folder1 +'/'+ locationsArray[i] +'/' + filename;
                        knox.putFile( tmpPath, s3Path,{ 'x-amz-acl': 'public-read', 'Content-Type': fileType }, function(err, resp) {
                        if (200 == resp.statusCode) { 
                                        console.log('Uploaded to amazon S3'); 
                                        res.end("success");
                                }else { 
                                        console.log('Failed to upload file to Amazon S3'); 
                                        res.end("failure");
                                }
                        });
                }
			*/
                var filename = req.files.uploaded.name;
                var fileType = req.files.uploaded.type;
                var tmpPath = req.files.uploaded.path;
				var s3Path = '/' + filename;
                knox.putFile( tmpPath, s3Path,{ 'x-amz-acl': 'public-read', 'Content-Type': fileType }, function(err, resp) {
                    if (200 == resp.statusCode) { 
                        console.log('Uploaded to amazon S3'); 
                                        res.end("success");
                    }else { 
                        console.log('Failed to upload file to Amazon S3'); 
                        res.end("failure");
                    }
				});
				
				db.collection('files').insert({filename: filename, url: baseS3URL + S3_BUCKET + '/' + filename, permissions:requiredAcessPriv, category:req.body.category, name:req.body.name, description:req.body.description} , function(err, result) {
					if (result) {
						console.log(result);							
						res.send('1');
					}
				});
        } else {
                res.send("You do not have the proper privileges.")
        }
}

exports.get = function(req, res){
	var links =[];
    if(req.method == 'GET'){
		var approvedUsers = ['user'];
		auth.restrict(req, res, db, approvedUsers, function(ret){	
			if(ret == true){
				db.collection('files').find().toArray(function(err, result){
					if (result){
						var count = 0;

						async.whilst(
						    function () { 
						    	return count < result.length;
						    },
						    function (callback) {
								var accessPrivs = result[count].permissions; 
								for(i=0; i< accessPrivs.length; i++)
								{
									if(accessPrivs[i] == 1 && req.session.accessPrivileges[i] == 1){
										console.log("!!MATCH!!");
										links.push(result[count]);
										break;
									}
	
								}
						        count++;
						        callback();
						    },
						    function (err) {
						    	var retString = JSON.stringify(links);
								res.writeHead(200,{
									'Content-Length': retString.length,
									'Content-Type': 'text/plain',
									'Pragma': 'no-cache',
									'Cache-Control': 'no-cache',
									'Expires': '-1' });
							res.write(retString);
							res.end();
						    }
						);						
					}
				});
			}else {
				res.send('You do not have the proper permissions');
			}
		});
	}else {
			console.log('[405] ' + req.method + ' to ' + req.url);
			res.writeHead(405, 'Method not supported', {'Content-Type': 'text/html'});
			res.end('<html><head><title>405 - Method not supported </title></head><body><h1>Method not suported.</h1></body></html>');
	}	
}

exports.wipe = function(req, res){
	db.collection('files').remove({}, function(err, result) {
		if (result){
			res.send('1');
		}
	});
}