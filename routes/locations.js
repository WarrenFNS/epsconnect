var url = require("url"),
	querystring = require("querystring"),
	auth = require('../utils/authenticate.js');
	
function init(app, db){
	
	app.get('/addLocation', function(req, res){
		if(req.method == 'GET'){
			var approvedUsers = ['superUser', 'admins'];
			auth.restrict(req, res, db, approvedUsers, function(ret){	
				if(ret==true){
					var incoming = url.parse(req.url).query;
					var info = querystring.parse(incoming);
			
					db.collection('locations').findOne({loc:info.loc}, function(err, result) {
				    	if (err) throw err;
				    	if(result) { 
							res.writeHead(200, "OK", {'Content-Type': 'text/html'});
							res.write("0")
							res.end();
						}

						else{
							db.collection('locations').insert(info, function(err, result) {
								if (err) throw err;
								if (result) {
									console.log('Added!');							
									res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
									res.write("1");
									res.end();
								}
							});
						}
					});
				}else {
					res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
					res.write("Improper credentials");
					res.end();
				}
			})

		 } else {
			console.log("[405] " + req.method + " to " + req.url);
			res.writeHead(405, "Method not supported", {'Content-Type': 'text/html'});
			res.end('<html><head><title>405 - Method not supported </title></head><body><h1>Method not suported.</h1></body></html>');
		}
	});
	
	app.get('/editLocation', function(req, res){
		if(req.method == 'GET'){
			var approvedUsers = ['superUser','admin'];

			auth.restrict(req, res, db, approvedUsers, function(ret){	
				if(ret==true){		
					var incoming = url.parse(req.url).query;
					var info = querystring.parse(incoming);

					db.collection('locations').findOne({loc:info.loc,}, function(err, result) {
			    		if (err) throw err;
						else{
							info["_id"] = result._id;
              var temp = Object.keys(info);
              var key;
							for(var t = 0; t <  temp.length; t++){
							//	if (info[key] != result[key])
                  key = temp[t];
                  result[key] = info[key];
								
							}
							db.collection('locations').save(result, function(err) {
								if (err) {
									throw err;
									res.send("0");

								} else {				
									console.log('Updated!');
									res.send("1");						
								}		
							});	
						}
					});
							
					
				} else {
				res.send('You do not have the proper permissions');
				res.end();
				}
			})
		
		}else {
			console.log('[405] ' + req.method + ' to ' + req.url);
			res.writeHead(405, 'Method not supported', {'Content-Type': 'text/html'});
			res.end('<html><head><title>405 - Method not supported </title></head><body><h1>Method not suported.</h1></body></html>');
		}
	});
	
	app.get('/getLocations', function(req, res){

		db.collection('locations').find({}, {loc:1, name:1}).toArray(function(err, result) {
			if (!err){
			    res.writeHead(200, {"Content-Type": "text/plain"});
          res.write(JSON.stringify(result));
			    res.end();
			}
		});
	});

  app.get('/getLocationInfo', function(req, res){
    var incoming = url.parse(req.url).query;
    var info = querystring.parse(incoming);

		db.collection('locations').findOne({loc:info.loc}, function(err, result) {
			if (!err){
			    res.writeHead(200, {"Content-Type": "text/plain"});
          res.write(JSON.stringify(result));
			    res.end();
			}
		});
	});
		
  app.get('/getLocationAppInfo', function(req, res){
    var incoming = url.parse(req.url).query;
    var info = querystring.parse(incoming);

		db.collection('locations').findOne({loc:info.loc}, {color:1, name:1}, function(err, result) {
			if (!err){
			    res.writeHead(200, {"Content-Type": "text/plain"});
          res.write(JSON.stringify(result));
			    res.end();
			}
		});
	});
		
	app.get('/deleteLocation', function (req, res){
		if(req.method == 'GET'){
			if(req.method == 'GET'){
				var approvedUsers = ['superUser', 'admin'];
				auth.restrict(req, res, db, approvedUsers, function(ret){	
					if(ret==true){
						var incoming = url.parse(req.url).query;
						var info = querystring.parse(incoming);
						db.collection('locations').remove({loc:info.loc}, function(err, result) {
		          			if (err) throw err;
							else{
								res.writeHead(200, "OK", {'Content-Type': 'text/html'});
								res.write("1");
								res.end();
							}
						});
					}else {
						res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
						res.write("Improper credentials");
						res.end();
					}
				})

			} else {
				console.log("[405] " + req.method + " to " + req.url);
				res.writeHead(405, "Method not supported", {'Content-Type': 'text/html'});
				res.end('<html><head><title>405 - Method not supported </title></head><body><h1>Method not suported.</h1></body></html>');
				}
			}
	});
		
	
}
exports.init = init;
		
