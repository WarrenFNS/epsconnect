//var db = require('../node_modules/mongoskin').db('localhost:27017/test');
var db = require('../node_modules/mongoskin').db('mongodb://fhsofaadajdp:skfbskjgowhfwb37632b2kbr@dharma.mongohq.com:10000/EPSConnectApp');
exports.admin= function(req, res){
	var auth = require('../utils/authenticate.js');
	auth.authenticate(req.query.username, req.query.password, 'admins', db, function(err, result){
	    if (result) {
			console.log("there is an admin");

	      	// Regenerate session when signing in
	      	// to prevent fixation 
	      		req.session.regenerate(function(){
	        		// Store the admin's primary key 
	        		// in the session store to be retrieved,
	        		// or in this case the entire admin object

					console.log("admin confirmed");
					req.session.username = result.username;
					req.session.collection =  "admins";
					req.session.password = result.password;
					req.session.userType = 'admin';
					req.session.accessPrivileges = '11111111';
					res.send('Authenticated as ' + result.username);

	      		});

	    } else {
			console.log("didn't log in successfully")
	      	res.send('Authentication failed, please check your '
	        + ' adminID and password.');
	    }
	});
}

exports.user= function(req, res){
	var auth = require('../utils/authenticate.js');
	auth.authenticate(req.query.username, req.query.password, 'users', db, function(err, result){
	    if (result) {
			console.log("there is a user");

	      	// Regenerate session when signing in
	      	// to prevent fixation 
	      		req.session.regenerate(function(){
	        		// Store the user's primary key 
	        		// in the session store to be retrieved,
	        		// or in this case the entire user object

					console.log("user confirmed");
					req.session.username = result.username;
					req.session.collection =  "users";
					req.session.password = result.password;
					req.session.userType = 'user';
					req.session.accessPrivileges = result.accessPrivileges;
					res.send('Authenticated as ' + result.username);

	      		});

	    } else {
			console.log("didn't log in successfully")
	      	res.send('Authentication failed, please check your '
	        + ' userID and password.');
	    }
	});
};

exports.logout= function(req, res){
	// destroy the user's session to log them out
	// will be re-created next request
	req.session.destroy(function(){
		res.send("You have been logged out");
	});
} 