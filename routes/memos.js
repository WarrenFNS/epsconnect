var auth = require('../utils/authenticate.js');
var async = require('../node_modules/async');
//var db = require('../node_modules/mongoskin').db('localhost:27017/test');
var db = require('../node_modules/mongoskin').db('mongodb://fhsofaadajdp:skfbskjgowhfwb37632b2kbr@dharma.mongohq.com:10000/EPSConnectApp');

exports.post = function(req, res){
	if(req.method == 'GET'){
		var approvedUsers = ['admin'];
		auth.restrict(req, res, db, approvedUsers, function(ret){	
			if(ret == true){
				//var incoming = url.parse(req.url).query;
				var info = req.query;
				db.collection('memos').insert(info, function(err, result) {
					if (result) {
						console.log('Added!');							
						res.send('1');
					}
				});
			} else {
				res.send('You do not have the proper permissions');
			}
		});
	}else {
		console.log('[405] ' + req.method + ' to ' + req.url);
		res.writeHead(405, 'Method not supported', {'Content-Type': 'text/html'});
		res.end('<html><head><title>405 - Method not supported </title></head><body><h1>Method not suported.</h1></body></html>');
	}
}

exports.getAll = function(req, res){
	if(req.method == 'GET'){
		var approvedUsers = ['admin'];
		auth.restrict(req, res, db, approvedUsers, function(ret){	
			if(ret == true){
				db.collection('memos').find().toArray(function(err, result) {
					if (result){
						var retString = JSON.stringify(result);
						res.writeHead(200,{
							'Content-Length': retString.length,
							'Content-Type': 'text/plain',
							'Pragma': 'no-cache',
							'Cache-Control': 'no-cache',
							'Expires': '-1' });
						res.write(retString);
						res.end();
					}
				});
			}else {
				res.send('You do not have the proper permissions');
			}
		});
	}else {
			console.log('[405] ' + req.method + ' to ' + req.url);
			res.writeHead(405, 'Method not supported', {'Content-Type': 'text/html'});
			res.end('<html><head><title>405 - Method not supported </title></head><body><h1>Method not suported.</h1></body></html>');
	}	
}

exports.getUserMemos = function(req, res){
	if(req.method == 'GET'){
		var userMemos=[];
		var approvedUsers = ['user'];
		auth.restrict(req, res, db, approvedUsers, function(ret){	
			if(ret == true){
				db.collection('memos').find().toArray(function(err, result){
					if (result){
						var count = 0;

						async.whilst(
						    function () { 
						    	return count < result.length;
						    },
						    function (callback) {
								console.log("Memo" + count);
								var accessPrivs = result[count].accessPrivileges; 
								for(i=0; i< accessPrivs.length; i++)
								{
									if(accessPrivs[i] == 1 && req.session.accessPrivileges[i] == 1){
										console.log("!!MATCH!!");
										userMemos.push(result[count]);
										break;
									}
	
								}
						        count++;
						        callback();
						    },
						    function (err) {
						    	var retString = JSON.stringify(userMemos);
								res.writeHead(200,{
									'Content-Length': retString.length,
									'Content-Type': 'text/plain',
									'Pragma': 'no-cache',
									'Cache-Control': 'no-cache',
									'Expires': '-1' });
							res.write(retString);
							res.end();
						    }
						);						
					}
				});
			}else {
				res.send('You do not have the proper permissions');
			}
		});
	}else {
			console.log('[405] ' + req.method + ' to ' + req.url);
			res.writeHead(405, 'Method not supported', {'Content-Type': 'text/html'});
			res.end('<html><head><title>405 - Method not supported </title></head><body><h1>Method not suported.</h1></body></html>');
	}	
}

exports.wipe = function(req, res){
	db.collection('memos').remove({}, function(err, result) {
		if (result){
			res.send('1');
		}
	});
}
