var url = require("url"),
	querystring = require("querystring"),
	auth = require('../utils/authenticate.js');

function init(app, db){
	app.get('/addNotification', function (req, res){
		
		var incoming = url.parse(req.url).query;
		var info = querystring.parse(incoming);
	
		db.collection('notifications').insert(info, function(err, result) {
			if (err) throw err;
			if (result) {
				console.log('Added!');							
				res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
				res.write("1");
				res.end();
				}
		});
	});

	app.get('/showNotifications', function (req, res){
		
		var incoming = url.parse(req.url).query;
		var info = querystring.parse(incoming);
		
		db.collection('notifications').find({patient:info.patient}).toArray(function(err, result) {
		    if (err) throw err;
			else{
		    	res.writeHead(200, {"Content-Type": "text/plain"});
				for (i = 0; i< result.length; i++){
					var object = result[i];
					var output = '';
					for (property in object) {
			  			output += property + ': ' + object[property] + ' ';
					}
					output+='\n';
					res.write(output);
				}
		    	res.end();
			}
		});
	});

 	app.get('/clearNotifications', function (req, res){
		db.collection('notifications').remove(function(err, result) {
		  if (!err) {
		  	console.log('CLEAR');
		  	res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
		  	res.write("CLEARED");
		  	res.end();
			}
		});	
	});
}

exports.init = init;