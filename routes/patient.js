var 
	approvedUsers = ['provider'],
	userType = 'patient',
	users = require('../utils/user_stuff.js');
	
function init(app, db){
	
	app.get('/addPatient', function (req, res){
	if(req.method == 'GET'){
				var info = req.query;
				var collection = 'patients'

				info.loc = [];

    db.collection(collection).findOne({userID:info.userID}, function(err, result) {
						if(result) { 
							res.send('0');
						} else {
							info.userType = 'patient';
							db.collection(collection).insert(info, function(err, result) {
								if (result) {
									console.log('Added!');							
									res.send('1');
								}
							});
						}
					});
    }
  });
	
	app.get('/getPatient', function(req, res){
		users.get(req, res, userType, approvedUsers, db);
	});
	
	app.get('/getPatients', function(req, res){
		users.getAll(req, res, userType, approvedUsers, db);
	});

	app.get('/deletePatient', function (req, res){
		users.destroy(req, res, userType, approvedUsers, db);
	});
	

	app.get('/getPatientLocations', function(req, res){
		db.collection('patients').findOne({userID:req.query.userID}, function(err, result) {
		    if (err) throw err;
			var output = '';
			
			res.writeHead(200, {"Content-Type": "text/plain"});
			for (i = 0; i< result.loc.length; i++){
				output += result.loc[i];
				output += '\n';
			}
			
			res.write(output);
	    	res.end();
		});
	});
	
	app.get('/addPatientLocation', function(req, res){
		db.collection('patients').update({userID:req.query.userID}, {'$addToSet':{loc:req.query.loc}}, function(err) {
		    if (err) throw err;
		    else{
				res.writeHead(200, "OK", {'Content-Type': 'text/html'});
				res.write("1")
				res.end();
			}
		});
	});
	
}




exports.init = init;
