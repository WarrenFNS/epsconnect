var url = require("url"),
	querystring = require("querystring"),
	auth = require('../utils/authenticate.js');
	
function init(app, db){
	
	app.get('/addPatient', function(req, res){
		console.log("new session to be added to DB");

		if(req.method == 'GET'){
			var incoming = url.parse(req.url).query;
			var PatientInfo = querystring.parse(incoming);
			db.collection('patients').findOne({username:PatientInfo.username}, function(err, result) {
				    if (err) throw err;
				    if(result) { 

							res.writeHead(200, "OK", {'Content-Type': 'text/html'});
							res.write("0")
							res.end();
						}

					else{
						db.collection('patients').insert(PatientInfo, function(err, result) {
							if (err) throw err;
							if (result) {
								console.log('Added!');							
								res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
								res.write("1");
								res.end();
							}
							});
					}
				});

		 } else {
			console.log("[405] " + req.method + " to " + req.url);
			res.writeHead(405, "Method not supported", {'Content-Type': 'text/html'});
			res.end('<html><head><title>405 - Method not supported </title></head><body><h1>Method not suported.</h1></body></html>');
		}
		
	});
	
	app.get('/editPatient' , function (req, res){
			if(req.method == 'GET'){
				var incoming = url.parse(req.url).query;
				var sessionInfo = querystring.parse(incoming);
				db.collection('patients').update({username:sessionInfo.username}, {$set:sessionInfo}, function(err, result) {
					    if (err) throw err;
								res.writeHead(200, "OK", {'Content-Type': 'text/html'});
								res.write("1");
								res.end();

					});

			 } else {
				console.log("[405] " + req.method + " to " + req.url);
				res.writeHead(405, "Method not supported", {'Content-Type': 'text/html'});
				res.end('<html><head><title>405 - Method not supported </title></head><body><h1>Method not suported.</h1></body></html>');
			}



	})
	app.get('/showPatients', function(req, res){
		db.collection('patients').find().toArray(function(err, result) {
		    if (err) throw err;
			else{
		    	res.writeHead(200, {"Content-Type": "text/plain"});
				for (i = 0; i< result.length; i++){
					var object = result[i];
					var output = '';
					for (property in object) {
			  			output += property + ': ' + object[property] + ' ';
					}
					output+='\n';
					res.write(output);
				}
		    	res.end();
			}
		});
	});
	
	app.get('/deletePatient', function (req, res){

			console.log("new session to be added to DB");

			if(req.method == 'GET'){
				var incoming = url.parse(req.url).query;
				var sessionInfo = querystring.parse(incoming);
					db.collection('patients').remove({username:sessionInfo.username}, function(err, result) {
	          if (err) throw err;
						else{
								res.writeHead(200, "OK", {'Content-Type': 'text/html'});
								res.write("1");
								res.end();
						}
					});

			 } else {
				console.log("[405] " + req.method + " to " + req.url);
				res.writeHead(405, "Method not supported", {'Content-Type': 'text/html'});
				res.end('<html><head><title>405 - Method not supported </title></head><body><h1>Method not suported.</h1></body></html>');
			}
	});
}
		




exports.init = init;