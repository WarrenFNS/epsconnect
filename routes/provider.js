var 
	approvedUsers = ['admin'],
	userType = 'provider',
	users = require('../utils/user_stuff.js');
	
function init(app, db){
	
	app.get('/addProvider', function (req, res){
		users.add(req, res, userType, approvedUsers, db);
	});
	
	app.get('/getProvider', function(req, res){
		users.get(req, res, userType, approvedUsers, db);
	});
	
	app.get('/getProviders', function(req, res){
		users.getAll(req, res, userType, approvedUsers, db);
	});

	app.get('/deleteProvider', function (req, res){
		users.destroy(req, res, userType, approvedUsers, db);
	});
	
	app.get('/changeProviderPassword', function (req, res){
		users.changePassword(req, res, userType, ['admin','provider'], db);
	});

  app.get('/editProvider', function(req, res){
    users.edit(req, res, userType, approvedUsers, db);
    });
}

exports.init = init;
