var auth = require('../utils/authenticate.js');
	
function init(app, db){
	
	app.get('/addRow', function(req, res){
		if(req.method == 'GET'){
			var approvedUsers = ['superUser', 'admin'];

			auth.restrict(req, res, db, approvedUsers, function(ret){	
				if(ret == true){
					var info = req.query;
					var collection = info.category;

					if (collection == "ERNav" || "HealthInfo" || "AfterER" || "HealthRecords" || "CannedNotifications" || "CannedReminders"){
						db.collection(collection).findOne({rowID:info.rowID, loc:info.loc}, function(err, result) {
					   	if(result) { 
								res.send('0');
							}

							else{
								db.collection(collection).insert(info, function(err, result) {
									if (err) throw err;
									if (result) {
										console.log('Row added!');							
										res.send('1');
									}
								});
							}
						});
					} else {
						res.send('0');
					}
				} else {
					res.send('You do not have the proper permissions');
				}
			});
		}else {
			console.log('[405] ' + req.method + ' to ' + req.url);
			res.writeHead(405, 'Method not supported', {'Content-Type': 'text/html'});
			res.end('<html><head><title>405 - Method not supported </title></head><body><h1>Method not suported.</h1></body></html>');
		}
	});

	app.get('/deleteRow', function(req, res){
		if(req.method == 'GET'){
			var approvedUsers = ['superUser', 'admin'];

			auth.restrict(req, res, db, approvedUsers, function(ret){	
				if(ret==true){
					var info = req.query;
					var collection = info.category;
					if (collection == "ERNav" || "HealthInfo" || "AfterER" || "HealthRecords" || "CannedNotifications" || "CannedReminders"){
						db.collection(collection).remove({rowID:info.rowID, loc:info.loc}, function(err, result) {
				    		if (err){
                  res.send('0');
                }
							else{
								res.send('1');
							}
						});
					} else {
						res.send('0');
					}
				}else {
					res.send('improper credentails');
				}
			});
	
		} else {
			console.log('[405] ' + req.method + ' to ' + req.url);
			res.writeHead(405, 'Method not supported', {'Content-Type': 'text/html'});
			res.end('<html><head><title>405 - Method not supported </title></head><body><h1>Method not suported.</h1></body></html>');
		}
	});		

	app.get('/editRow', function(req, res){
		if(req.method == 'GET'){
			var approvedUsers = ['superUser', 'admin'];

			auth.restrict(req, res, db, approvedUsers, function(ret){	
				if(ret==true){
					var info = req.query;
					var collection = info.category;
					if (collection == "ERNav" || "HealthInfo" || "AfterER" || "HealthRecords" || "CannedNotifications" || "CannedReminders"){
				    	db.collection(collection).findOne({rowID:info.rowID, loc:info.loc}, function(err, result) {
			    			if (result){
              					var temp = Object.keys(info);
              					var key;
								for(var t = 0; t <  temp.length; t++){
                  					key = temp[t];
                  					result[key] = info[key];
								
								}
								db.collection(collection).save(result, function(err) {
									if (err) {
										res.send("0");
									} else {				
										res.send("1");						
									}		
								});	
							}
						});
					} else{
						res.send('not a valid collection');
					}
				} else {
					res.send('improper credentails');
				}
			});
		} else {
			console.log('[405] ' + req.method + ' to ' + req.url);
			res.writeHead(405, 'Method not supported', {'Content-Type': 'text/html'});
			res.end('<html><head><title>405 - Method not supported </title></head><body><h1>Method not suported.</h1></body></html>');
		}			
	});

	app.get('/getRowInfo', function(req, res){
		if(req.method == 'GET'){
			var approvedUsers = ['superUser', 'admin'];

			auth.restrict(req, res, db, approvedUsers, function(ret){	
				if(ret==true){
					var info = req.query;
					var collection = info.category;
					if (collection == "ERNav" || "HealthInfo" || "AfterER" || "HealthRecords" || "CannedNotifications" || "CannedReminders"){
						db.collection(collection).findOne({rowID:info.rowID, loc:info.loc}, function(err, result) {
							if(result!=null){
                console.log(result);
								res.send(JSON.stringify(result));
							} else {
								res.send('0');
							}
						});
					} else {
						res.send('not a valid collection');
					}
				}else {
					res.send('improper credentails');
				}
			});
		} else {
			console.log('[405] ' + req.method + ' to ' + req.url);
			res.writeHead(405, 'Method not supported', {'Content-Type': 'text/html'});
			res.end('<html><head><title>405 - Method not supported </title></head><body><h1>Method not suported.</h1></body></html>');
		}
	});	

	app.get('/getRows', function(req, res){
		if(req.method == 'GET'){
			var approvedUsers = ['superUser', 'admin', 'provider'];

			auth.restrict(req, res, db, approvedUsers, function(ret){	
				if(ret==true){
					var info = req.query;
					var collection = info.category;
					if (collection == "ERNav" || "HealthInfo" || "AfterER" || "HealthRecords" || "CannedNotifications" || "CannedReminders"){
						db.collection(collection).find({category:info.category, loc:info.loc}).toArray(function(err, result) {
							if(result!=null){
                console.log(result);
								res.send(JSON.stringify(result));
							} else {
								res.send('');
							}
						});
					} else {
						res.send('not a valid collection');
					}
				}else {
					res.send('improper credentails');
				}
			});
		} else {
			console.log('[405] ' + req.method + ' to ' + req.url);
			res.writeHead(405, 'Method not supported', {'Content-Type': 'text/html'});
			res.end('<html><head><title>405 - Method not supported </title></head><body><h1>Method not suported.</h1></body></html>');
		}
	});	
}

exports.init = init;
