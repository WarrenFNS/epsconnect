var url = require("url"),
	auth = require('../utils/authenticate.js'),
	querystring = require("querystring");


function init(app, db){
	app.get('/generateSession', function (req, res){
		
		console.log("new session to be added to DB");

		if(req.method == 'GET'){
			var approvedUsers = ['provider'];

			auth.restrict(req, res, db, approvedUsers, function(ret){	
				if(ret == true){
					var incoming = url.parse(req.url).query;
					var info = querystring.parse(incoming);
			
					db.collection(info.loc).findOne({sessionID:info.sessionID, loc:info.loc}, function(err, result) {
				    	if (err) throw err;
				    	if(result) { 

							res.writeHead(200, "OK", {'Content-Type': 'text/html'});
							res.write("<script> alert('That sessionID is already in use');</script>")
							res.end();
						}

						else{
							db.collection(info.loc).insert(info, function(err, result) {
								if (err) throw err;
								if (result) {
									console.log('Added!');							
									res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
									var output = '';
									for (property in info) {
						  				output += property + ': ' + info[property] + ' ';
									}
									output+='\n';
									res.write(output);
									res.end();
								}
							});

						}
					});

				} else {
					res.send('You do not have the proper permissions');
				}
			})
		} else {
			console.log('[405] ' + req.method + ' to ' + req.url);
			res.writeHead(405, 'Method not supported', {'Content-Type': 'text/html'});
			res.end('<html><head><title>405 - Method not supported </title></head><body><h1>Method not suported.</h1></body></html>');
		}
	});
	
	app.get('/getSessionInfo', function (req, res){
		
		if(req.method == 'GET'){
			var approvedUsers = ['provider'];

			auth.restrict(req, res, db, approvedUsers, function(ret){	
				if(ret == true){
					var incoming = url.parse(req.url).query;
					var info = querystring.parse(incoming);
			
					db.collection(info.loc).findOne({sessionID:info.sessionID, loc:info.loc}, function(err, result) {
				    	if (err) throw err;
				    	if(result) {
							res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
                			var output = JSON.stringify(result);
                			res.write(output);
                			res.end();
						}

						else{
							res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
							res.write("0");
							res.end();
          				}
					});

				} else {
					res.send('You do not have the proper permissions');
				}
			})
		} else {
			console.log('[405] ' + req.method + ' to ' + req.url);
			res.writeHead(405, 'Method not supported', {'Content-Type': 'text/html'});
			res.end('<html><head><title>405 - Method not supported </title></head><body><h1>Method not suported.</h1></body></html>');
		}
	});

	app.get('/getSessions', function (req, res){
		if(req.method == 'GET'){
			var approvedUsers = ['provider'];

			auth.restrict(req, res, db, approvedUsers, function(ret){	
				if(ret == true){
					var incoming = url.parse(req.url).query;
					var info = querystring.parse(incoming);
		
					db.collection(info.loc).find({loc:info.loc}).toArray(function(err, result) {
		    			if (err) throw err;
						else{
		    				res.writeHead(200, {"Content-Type": "text/plain"});
							for (i = 0; i< result.length; i++){
								var object = result[i];
								var output = '';
								for (property in object) {
			  						output += property + ': ' + object[property] + ' ';
								}
								output+='\n';
								res.write(output);
							}
		    				res.end();
						}
					});
				} else {
					res.send('You do not have the proper permissions');
				}
			})
		} else {
			console.log('[405] ' + req.method + ' to ' + req.url);
			res.writeHead(405, 'Method not supported', {'Content-Type': 'text/html'});
			res.end('<html><head><title>405 - Method not supported </title></head><body><h1>Method not suported.</h1></body></html>');
		}
	});

/*	app.get('/cleardb', function (req, res){
		db.collection(info.loc).remove(function(err, result) {
		    if (!err) {
			console.log('CLEAR');
			res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
			res.write("CLEARED");
			res.end();
		}
	});	
	})*/
	
	app.get('/editSession', function (req, res){
			if(req.method == 'GET'){
				var approvedUsers = ['provider'];

				auth.restrict(req, res, db, approvedUsers, function(ret){	
					if(ret==true){		
						var incoming = url.parse(req.url).query;
						var info = querystring.parse(incoming);

						db.collection(info.loc).findOne({loc:info.loc,}, function(err, result) {
				    		if (err) throw err;
							else{
								info["_id"] = result._id;
								for(key in result){
									if (info[key] != result[key]) info[key] = result[key];

								}
								db.collection(info.loc).save(info, function(err) {
									if (err) {
										throw err;
										res.send("0");

									} else {				
										console.log('Updated!');
										res.send("1");						
									}		
								});	
							}
						});


					} else {
					res.send('You do not have the proper permissions');
					res.end();
					}
				})

			}else {
				console.log('[405] ' + req.method + ' to ' + req.url);
				res.writeHead(405, 'Method not supported', {'Content-Type': 'text/html'});
				res.end('<html><head><title>405 - Method not supported </title></head><body><h1>Method not suported.</h1></body></html>');
			}
		});

	app.get('/isSession', function (req, res){
		if(req.method == 'GET'){
			var approvedUsers = ['provider'];

			auth.restrict(req, res, db, approvedUsers, function(ret){	
				if(ret==true){		
					var incoming = url.parse(req.url).query;
					var info = querystring.parse(incoming);
				
					db.collection(info.loc).findOne({sessionID:info.sessionID}, function(err, result) {
					    if (err) throw err;
					    if(result) {
								res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
								res.write("1");
								res.end();
							}

						else{
								res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
								res.write("0");
								res.end();
	          				}
					});

				} else {
				res.send('You do not have the proper permissions');
				res.end();
				}
			})

		}else {
			console.log('[405] ' + req.method + ' to ' + req.url);
			res.writeHead(405, 'Method not supported', {'Content-Type': 'text/html'});
			res.end('<html><head><title>405 - Method not supported </title></head><body><h1>Method not suported.</h1></body></html>');
		}
	});

	app.get('/deleteSession', function (req, res){

			if(req.method == 'GET'){
				var approvedUsers = ['provider'];

				auth.restrict(req, res, db, approvedUsers, function(ret){	
					if(ret==true){

						var incoming = url.parse(req.url).query;
						var info = querystring.parse(incoming);

						db.collection(info.loc).remove({sessionID:info.sessionID}, function(err, result) {
					    	if (err) throw err;
							else{
								res.send('1');
							}
						});
					}else {
						res.send('improper credentails');
					}
				})

			} else {
				console.log('[405] ' + req.method + ' to ' + req.url);
				res.writeHead(405, 'Method not supported', {'Content-Type': 'text/html'});
				res.end('<html><head><title>405 - Method not supported </title></head><body><h1>Method not suported.</h1></body></html>');
			}
		});
}

exports.init = init;