var auth = require('../utils/authenticate.js');
//var db = require('../node_modules/mongoskin').db('localhost:27017/test');
var db = require('../node_modules/mongoskin').db('mongodb://fhsofaadajdp:skfbskjgowhfwb37632b2kbr@dharma.mongohq.com:10000/EPSConnectApp');

exports.post = function(req, res){
	if(req.method == 'GET'){
		var approvedUsers = ['user'];
		auth.restrict(req, res, db, approvedUsers, function(ret){	
			if(ret == true){
				var info = req.query;
					db.collection('timesheets').insert(info, function(err, result) {
						if (result) {
							console.log('Added!');							
							res.send('1');
						}
					});
			} else {
					res.send('You do not have the proper permissions');
			}
		});
	}else {
		console.log('[405] ' + req.method + ' to ' + req.url);
		res.writeHead(405, 'Method not supported', {'Content-Type': 'text/html'});
		res.end('<html><head><title>405 - Method not supported </title></head><body><h1>Method not suported.</h1></body></html>');
	}
}



exports.getCurrentPayPeriod = function(req, res){
	if(req.method == 'GET'){
		var approvedUsers = ['admin', 'user'];
		var info = req.query;
		auth.restrict(req, res, db, approvedUsers, function(ret){	
			if(ret == true){
				if(info.sheetType == "monthly"){
					db.collection('payPeriods').find({sheetType:"monthly"}).sort({_id:-1}).limit(1).toArray(function(err, result) {
						if (result){
							var retString = JSON.stringify(result[0]);
							res.writeHead(200,{
								'Content-Length': retString.length,
								'Content-Type': 'text/plain',
								'Pragma': 'no-cache',
								'Cache-Control': 'no-cache',
								'Expires': '-1' });
							res.write(retString);
							res.end();
						}
					});

				}else if (info.sheetType = "bi-monthly"){
					db.collection('payPeriods').find({sheetType:"bi-monthly"}).sort({_id:-1}).limit(1).toArray(function(err, result) {
						if (result){
							var retString = JSON.stringify(result[0]);
							res.writeHead(200,{
								'Content-Length': retString.length,
								'Content-Type': 'text/plain',
								'Pragma': 'no-cache',
								'Cache-Control': 'no-cache',
								'Expires': '-1' });
							res.write(retString);
							res.end();
						}
					});

				}
			}else {
				res.send('You do not have the proper permissions');
			}
		});
	}else {
			console.log('[405] ' + req.method + ' to ' + req.url);
			res.writeHead(405, 'Method not supported', {'Content-Type': 'text/html'});
			res.end('<html><head><title>405 - Method not supported </title></head><body><h1>Method not suported.</h1></body></html>');
	}
}

exports.getAllPayPeriods = function(req, res){
	if(req.method == 'GET'){
		var approvedUsers = ['admin'];
		auth.restrict(req, res, db, approvedUsers, function(ret){	
			if(ret == true){
				db.collection('payPeriods').find().toArray(function(err, result) {
					if (result){
						var retString = JSON.stringify(result);
						res.writeHead(200,{
							'Content-Length': retString.length,
							'Content-Type': 'text/plain',
							'Pragma': 'no-cache',
							'Cache-Control': 'no-cache',
							'Expires': '-1' });
						res.write(retString);
						res.end();

					}
				});
			}else {
				res.send('You do not have the proper permissions');
			}
		});
	}else {
			console.log('[405] ' + req.method + ' to ' + req.url);
			res.writeHead(405, 'Method not supported', {'Content-Type': 'text/html'});
			res.end('<html><head><title>405 - Method not supported </title></head><body><h1>Method not suported.</h1></body></html>');
	}
}
	
exports.searchTimesheetsByName = function(req, res){
	if(req.method == 'GET'){
		var approvedUsers = ['admin'];
		var info = req.query;
		auth.restrict(req, res, db, approvedUsers, function(ret){	
			if(ret == true){
				db.collection('timesheets').find({username: info.username}).toArray(function(err, result) {
					if (result){
						console.log("USERNAME: "+ info.username);
						console.log(result);
						var retString = JSON.stringify(result);
						res.writeHead(200,{
							'Content-Length': retString.length,
							'Content-Type': 'text/plain',
							'Pragma': 'no-cache',
							'Cache-Control': 'no-cache',
							'Expires': '-1' });
						res.write(retString);
						res.end();

					}
				});
			}else {
				res.send('You do not have the proper permissions');
			}
		});
	}else {
			console.log('[405] ' + req.method + ' to ' + req.url);
			res.writeHead(405, 'Method not supported', {'Content-Type': 'text/html'});
			res.end('<html><head><title>405 - Method not supported </title></head><body><h1>Method not suported.</h1></body></html>');
	}
}

exports.searchTimesheetsByPayPeriod = function(req, res){
	if(req.method == 'GET'){
		var approvedUsers = ['admin'];
		var info = req.query;
		auth.restrict(req, res, db, approvedUsers, function(ret){	
			if(ret == true){
					console.log(info.timesheet);
					console.log(info.sheetType);
					db.collection('timesheets').find({timesheet: info.timesheet}).toArray(function(err, result) {
						if (result){
							console.log(result);
							var retString = JSON.stringify(result);
							res.writeHead(200,{
								'Content-Length': retString.length,
								'Content-Type': 'text/plain',
								'Pragma': 'no-cache',
								'Cache-Control': 'no-cache',
								'Expires': '-1' });
							res.write(retString);
							res.end();
						}
					});
			}else {
				res.send('You do not have the proper permissions');
			}
		});
	}else {
			console.log('[405] ' + req.method + ' to ' + req.url);
			res.writeHead(405, 'Method not supported', {'Content-Type': 'text/html'});
			res.end('<html><head><title>405 - Method not supported </title></head><body><h1>Method not suported.</h1></body></html>');
	}
}

exports.wipe = function(req, res){
	db.collection('timesheets').remove({}, function(err, result) {
		if (result){
			res.send('1');
		}
	});
}

exports.payperiodWipe = function(req, res){
	db.collection('payPeriods').remove({}, function(err, result) {
		if (result){
			res.send('1');
		}
	});
}