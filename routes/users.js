var auth = require('../utils/authenticate.js');
//var db = require('../node_modules/mongoskin').db('localhost:27017/test');
var db = require('../node_modules/mongoskin').db('mongodb://fhsofaadajdp:skfbskjgowhfwb37632b2kbr@dharma.mongohq.com:10000/EPSConnectApp');
exports.create = function(req, res){
	if(req.method == 'GET'){
		var approvedUsers = ['admin'];
		auth.restrict(req, res, db, approvedUsers, function(ret){	
			if(ret == true){
				var info = req.query;
				db.collection('users').findOne({username: req.session.username}, function(err, result) {
					if(result) { 
						res.send('0');
					} else{
						db.collection('users').insert(info, function(err, result) {
							if (result) {
								console.log('Added!');							
								res.send('1');
							}
						});
					}
				});
			} else {
					res.send('You do not have the proper permissions');
				}
		});
	}else {
		console.log('[405] ' + req.method + ' to ' + req.url);
		res.writeHead(405, 'Method not supported', {'Content-Type': 'text/html'});
		res.end('<html><head><title>405 - Method not supported </title></head><body><h1>Method not suported.</h1></body></html>');
	}
}

exports.getAll = function(req, res){
	if(req.method == 'GET'){
		var approvedUsers = ['admin'];
		auth.restrict(req, res, db, approvedUsers, function(ret){	
			if(ret == true){
				db.collection('users').find().sort({lastName:1}).toArray(function(err, result) {
					if(result){
						var retString = JSON.stringify(result);
						res.writeHead(200,{
							'Content-Length': retString.length,
							'Content-Type': 'text/plain',
							'Pragma': 'no-cache',
							'Cache-Control': 'no-cache',
							'Expires': '-1' });
						res.write(retString);
						res.end();

					}
				});
			}else {
				res.send('You do not have the proper permissions');
			}
		});
	}else {
			console.log('[405] ' + req.method + ' to ' + req.url);
			res.writeHead(405, 'Method not supported', {'Content-Type': 'text/html'});
			res.end('<html><head><title>405 - Method not supported </title></head><body><h1>Method not suported.</h1></body></html>');
	}
}

exports.getCurrentUser = function(req, res){
	if(req.method == 'GET'){
		var approvedUsers = ['user'];
		auth.restrict(req, res, db, approvedUsers, function(ret){	
			if(ret == true){
				var info = req.query;
				db.collection('users').findOne({username: req.session.username}, function(err, result) {
					if(result){
						var retString = JSON.stringify(result);
						res.writeHead(200,{
							'Content-Length': retString.length,
							'Content-Type': 'text/plain',
							'Pragma': 'no-cache',
							'Cache-Control': 'no-cache',
							'Expires': '-1' });
						res.write(retString);
						res.end();

					}
				});
			}else {
				res.send('You do not have the proper permissions');
			}
		});
	}else {
			console.log('[405] ' + req.method + ' to ' + req.url);
			res.writeHead(405, 'Method not supported', {'Content-Type': 'text/html'});
			res.end('<html><head><title>405 - Method not supported </title></head><body><h1>Method not suported.</h1></body></html>');
	}	
}

exports.resetPassword = function(req, res){
	if(req.method == 'GET'){
		var approvedUsers = ['admin','user'];
		auth.restrict(req, res, db, approvedUsers, function(ret){	
			if(ret==true){
				var info = req.query;
				db.collection('users').update({username:info.username}, {'$set':{password:info.password}}, function(err, result) {
					if(result){				
						res.send('1');
					}									
				});

		}else {
			res.send('You do not have the proper permissions');
		}
	});
	
	} else {
		console.log('[405] ' + req.method + ' to ' + req.url);
		res.writeHead(405, 'Method not supported', {'Content-Type': 'text/html'});
		res.end('<html><head><title>405 - Method not supported </title></head><body><h1>Method not suported.</h1></body></html>');
	}
}

exports.delete = function(req, res){
	if(req.method == 'GET'){
		var approvedUsers = ['admin'];
		auth.restrict(req, res, db, approvedUsers, function(ret){	
			if(ret == true){
				var info = req.query;
				db.collection('users').findOne({username: info.username}, function(err, result) {
					if(result){
						db.collection('users').remove({username:result.username}, function(err2, result2) {
							if(result2){
								res.send('1');
							}
						});

					}
				});
			}else {
				res.send('You do not have the proper permissions');
			}
		});
	}else {
			console.log('[405] ' + req.method + ' to ' + req.url);
			res.writeHead(405, 'Method not supported', {'Content-Type': 'text/html'});
			res.end('<html><head><title>405 - Method not supported </title></head><body><h1>Method not suported.</h1></body></html>');
	}	
}

exports.wipe = function(req, res){
	db.collection('users').remove({}, function(err, result) {
		if(result){
			res.send('1');
		}
	});
}
