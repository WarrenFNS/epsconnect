exports.authenticate = function(name, pass, collection, db, fn) {
	console.log('userID: '+ name +' password: '+ pass + " collection: " + collection +' accepted to authenticate');

	db.collection(collection).findOne({username:name, password:pass}, function(err, user) {		
		if(user) {
			return fn(null, user);
		}else{
			console.log("no user found");
		}
	});
}


exports.restrict = function(req, res, db, approvedUsers, fn) {
	var ret;
	if(req.session.collection != null){
		db.collection(req.session.collection).findOne({username:req.session.username}, function(err, result) {
			if(result) { 
				if (result.username == req.session.username && result.password == req.session.password && approvedUsers.indexOf(req.session.userType) != -1 ) {
					ret = true;
						
				}else {
					ret = false;
				}
			
				return fn(ret);
		
			} else{
				res.send("The user does not exist");
			}
		});
	}else{
		res.send("You are not currently logged in. Please log in and try again.")
	}
}

exports.memoAccess = function(accessPrivileges, requiredPrivileges, userType){
	if(userType == 'admin');
		return true;
	for(i=0;i<requiredPrivileges.length;i++)
	{	
		if ( accessPrivileges.charAt(i) == 1 && requiredPrivileges.charAt(i) == 1)	
			return true;
	}
	return false;
}

exports.fileAccess = function(accessPrivileges, requiredPrivileges, userType) {
	console.log("ACCESS PRIV:" + accessPrivileges+"\n");
	console.log("REQ PRIV:" + requiredPrivileges+"\n");
	console.log("USER TYPE:" + userType+"\n");
	if ( accessPrivileges.charAt(requiredPrivileges) == 1 || userType == 'admin')	
		return true;
	else
		return false;
}

