auth = require('./authenticate.js');

function addUser(req, res, userType, approvedUsers, db){
	if(req.method == 'GET'){
		auth.restrict(req, res, db, approvedUsers, function(ret){	
			if(ret == true){

				var info = req.query;
				var collection = info.loc;

				//special initialization for patients
				if(userType == 'patient'){
					var locationArray = new Array();
						locationArray[0] = info.loc;
						info.loc = locationArray;
						collection = 'patient';
				}

				if(req.session.loc == info.loc || req.session.userType == "superUser"){
					db.collection(collection).findOne({userID:info.userID}, function(err, result) {
						if(result) { 
							res.send('0');
						} else {
							info.userType = userType;	
							db.collection(collection).insert(info, function(err, result) {
								if (result) {
									console.log('Added!');							
									res.send('1');
								}
							});
						}
					});
				}

			} else {
				res.send('You do not have the proper permissions');
			}
		});
	} else {
		console.log('[405] ' + req.method + ' to ' + req.url);
		res.writeHead(405, 'Method not supported', {'Content-Type': 'text/html'});
		res.end('<html><head><title>405 - Method not supported </title></head><body><h1>Method not suported.</h1></body></html>');
	}
}

function getUser(req, res, userType, approvedUsers, db){
	if(req.method == 'GET'){
		auth.restrict(req, res, db, approvedUsers, function(ret){	
			if(ret==true){		
				var info = req.query;
				var collection = info.loc;

				//special initialization for patients
				if(userType == 'patient'){
					collection = 'patient';
				}

				if(req.session.loc == info.loc || req.session.userType == "superUser"){
					db.collection(collection).findOne({userID: info.userID}, function(err, result) {
						if(result) {
							res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
	                		var output = JSON.stringify(result);
	                		res.write(output);
	                		res.end();
						}
					});
				}
			}else {
			res.send('You do not have the proper permissions');
			res.end();
			}
		});
		
	}else {
		console.log('[405] ' + req.method + ' to ' + req.url);
		res.writeHead(405, 'Method not supported', {'Content-Type': 'text/html'});
		res.end('<html><head><title>405 - Method not supported </title></head><body><h1>Method not suported.</h1></body></html>');
	}
}

function getAllUsers(req, res, userType, approvedUsers, db){
	if(req.method == 'GET'){
		auth.restrict(req, res, db, approvedUsers, function(ret){	
			if(ret==true){		
				var info = req.query;
				var collection = info.loc;

				//special initialization for patients
				if(userType == 'patient'){
					collection = 'patient';
				}

				if(req.session.loc == info.loc || req.session.userType == "superUser"){
					db.collection(collection).find({userType: userType}).toArray(function(err, result) {
				    	if (result){
				    		res.writeHead(200, {'Content-Type': 'text/plain'});
							  res.write(JSON.stringify(result));
				    		res.end();
						}
					});
				}
			}else {
			res.send('You do not have the proper permissions');
			res.end();
			}
		});
		
	}else {
		console.log('[405] ' + req.method + ' to ' + req.url);
		res.writeHead(405, 'Method not supported', {'Content-Type': 'text/html'});
		res.end('<html><head><title>405 - Method not supported </title></head><body><h1>Method not suported.</h1></body></html>');
	}
}

function deleteUser(req, res, userType, approvedUsers, db){
	if(req.method == 'GET'){

		auth.restrict(req, res, db, approvedUsers, function(ret){	
			if(ret==true){

				var info = req.query;
				var collection = info.loc;

				//special initialization for patients
				if(userType == 'patient'){
					collection = 'patient';
				}
				if(req.session.loc == info.loc || req.session.userType == "superUser"){
					db.collection(collection).remove({userID:info.userID, userType: userType}, function(err, result) {
						if(result){
							res.send('1');
						}
						else{
							res.send('0');
						}
					});
				}
			}else {
				res.send('improper credentails');
			}
		})
	
	} else {
		console.log('[405] ' + req.method + ' to ' + req.url);
		res.writeHead(405, 'Method not supported', {'Content-Type': 'text/html'});
		res.end('<html><head><title>405 - Method not supported </title></head><body><h1>Method not suported.</h1></body></html>');
	}
}

function changeUserPassword(req, res, userType, approvedUsers, db){
	if(req.method == 'GET'){
		auth.restrict(req, res, db, approvedUsers, function(ret){
			console.log(req.session);	
			if(ret==true){
				var info = req.query;

				if(req.session.loc == info.loc || req.session.userType == "superUser"){	
					db.collection(info.loc).update({userID:info.userID, userType: userType, password:info.oldPassword}, {'$set':{password:info.newPassword}}, function(err, result) {
						if (result) {
							res.send('1');

						} else {				
							res.send('0');							
						}		
					});
				}

			}else {
				res.send('improper credentails');
			}
		});
	
	} else {
		console.log('[405] ' + req.method + ' to ' + req.url);
		res.writeHead(405, 'Method not supported', {'Content-Type': 'text/html'});
		res.end('<html><head><title>405 - Method not supported </title></head><body><h1>Method not suported.</h1></body></html>');
	}
}

function editUser(req, res, userType, approvedUsers, db){
    if(req.method == 'GET'){
    	auth.restrict(req, res, db, approvedUsers, function(ret){       
            if(ret==true){          
                var info = req.query;
				var collection = info.loc;

                if(req.session.loc == info.loc || req.session.userType == "superUser"){
					db.collection(collection).findOne({loc:info.loc, userType: userType, userID:info.userID}, function(err, result) {
	                    if (result){
	                    	console.log("HERE'S THE CULPRIT:" + result);
	                        info["_id"] = result._id;
	              			var temp = Object.keys(info);
	              			var key;
	                        for(var t = 0; t <  temp.length; t++){
	                  			key = temp[t];
	                  			result[key] = info[key];                                  
	                        }
	                        db.collection(collection).save(result, function(err) {
	                        	if (result){                             
	                            	console.log('Updated!');
	                            	res.send("1");                                          
	                        	}               
	                        });     
	                    }
	                });
                }                                                      
            } else{
                res.send('You do not have the proper permissions');
                res.end();
            }
    	});
                
    } else{
        console.log('[405] ' + req.method + ' to ' + req.url);
        res.writeHead(405, 'Method not supported', {'Content-Type': 'text/html'});
        res.end('<html><head><title>405 - Method not supported </title></head><body><h1>Method not suported.</h1></body></html>');
    }
}

exports.add = addUser;
exports.get = getUser;
exports.getAll = getAllUsers;
exports.destroy = deleteUser;
exports.changePassword = changeUserPassword;
exports.edit = editUser;
